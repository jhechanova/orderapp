package com.example.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class WIFIConnectionHelper {
	
	public static boolean isConnected = false;
		
	public WIFIConnectionHelper() {}
	
	/*
     * Wifi Connection service
     */
    
    private void DisplayWifiState(Context myContext)
    {      	
    	@SuppressWarnings("static-access")
		ConnectivityManager myConnManager = (ConnectivityManager) myContext.getSystemService(myContext.CONNECTIVITY_SERVICE);
	    NetworkInfo myNetworkInfo = myConnManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);	    
	    
	    if (myNetworkInfo.isConnected())
	    {
	    	Toast.makeText(myContext,"WIFI Available.",Toast.LENGTH_LONG).show();
	    	if (!isConnected)
	    	{
		    	isConnected = true;
		    	//new SyncData().execute("getItems");
		    	//new SyncData().execute("getPaymentTerms");
		    	//new SyncData().execute("getCustomers");
		    	
		    	//new SyncData().execute("getSalesMan");
		    	//new SyncData().execute("getConsignments");
	    	}
	    }
	    else
	    {
	    	Toast.makeText(myContext,"No WIFI Available.",Toast.LENGTH_LONG).show();
	    	isConnected = false;
	    }
    }
           
	public BroadcastReceiver myWifiReceiver = new BroadcastReceiver()
    {
    	@Override
    	public void onReceive(Context context, Intent arg1)
    	{
    		@SuppressWarnings("deprecation")
			NetworkInfo networkInfo = (NetworkInfo) arg1.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
    		if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
    		{
    			DisplayWifiState(context);
    		}
    	}
    };
}
