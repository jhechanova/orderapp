package com.example.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.cete.dynamicpdf.Document;
import com.cete.dynamicpdf.Font;
import com.cete.dynamicpdf.Page;
import com.cete.dynamicpdf.PageOrientation;
import com.cete.dynamicpdf.PageSize;
import com.cete.dynamicpdf.RgbColor;
import com.cete.dynamicpdf.pageelements.Cell;
import com.cete.dynamicpdf.pageelements.CellAlign;
import com.cete.dynamicpdf.pageelements.CellVAlign;
import com.cete.dynamicpdf.pageelements.Row;
import com.cete.dynamicpdf.pageelements.Table;

@SuppressWarnings("deprecation")
public class PDFCreateHelper {
	
	private Document objDocument = null;
	private Page objPage = null;
	private ArrayList<String[]> data = null;
	
	private int stopIndx1 = 0;
	private int stopIndx2 = 0;
	private int stopIndx3 = 0;
	private int stopIndx4 = 0;
	private int stopIndx5 = 0;
	private int stopIndx6 = 0;
	
	private boolean prodWritten = false;
	
	public void createPDF(String fileName, Context context, ArrayList<String[]> d)
	{		
		String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/OrderApp/reports/" + fileName;
		data = d;
		
		// Create a document and set it's properties
        objDocument = new Document();
        objDocument.setAuthor("SAJI Corporation");
        
        // Create a page to add to the document
        objPage = new Page(PageSize.LETTER, PageOrientation.PORTRAIT, 30.0f);
        
        if (fileName.toLowerCase().contains("cashsales"))
        {
        	objDocument.setTitle("Cash Sales Summary");
        	boolean isEnd = generateCashSalesPDF(objDocument);
        	while (!isEnd)
        	{
        		isEnd = generateCashSalesPDF(objDocument);
        	}
        }
        else if (fileName.toLowerCase().contains("creditsales"))
        {
        	objDocument.setTitle("Credit Sales Summary");
        	boolean isEnd = generateCreditSalesPDF(objDocument);
        	while (!isEnd)
        	{
        		isEnd = generateCreditSalesPDF(objDocument);
        	}
        }
        else if (fileName.toLowerCase().contains("productsold"))
        {
        	objDocument.setTitle("Product Sold Summary");
        	boolean isEnd = generateProductSoldPDF(objDocument);
        	while (!isEnd)
        	{
        		isEnd = generateProductSoldPDF(objDocument);
        	}
        }
        else if (fileName.toLowerCase().contains("cashcollection"))
        {
        	objDocument.setTitle("Cash Collection Summary");
        	boolean isEnd = generateCashCollectionPDF(objDocument);
        	while (!isEnd)
        	{
        		isEnd = generateCashCollectionPDF(objDocument);
        	}
        }
        else if (fileName.toLowerCase().contains("productremainingquantity"))
        {
        	objDocument.setTitle("Product Remaining Quantity");
        	boolean isEnd = generateProductInventory(objDocument);
        	while (!isEnd)
        	{
        		isEnd = generateProductInventory(objDocument);
        	}
        }
        
        try {
            // Outputs the document to file
            objDocument.draw(path);
            //Toast.makeText(context, "File has been written to :" + path, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Error, unable to write to file\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
	}
	
	private boolean generateCashSalesPDF(Document _document)
	{
		boolean endLine = true;
		
		Page _page = new Page(PageSize.LETTER, PageOrientation.PORTRAIT, 30.0f);
		
		// Create table
        Table table2 = new Table(0, 0, 650, 700);
        
        // Add columns to the table
        table2.getColumns().add(100);
        table2.getColumns().add(250);
        table2.getColumns().add(200);
         
        // Add rows to the table and add cells to the rows
        Row row1 = table2.getRows().add(40, Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray());
        
        row1.setAlign(CellAlign.CENTER);
        row1.setVAlign(CellVAlign.CENTER);
        row1.getCellList().add("Receipt No.");
        row1.getCellList().add("Customer Name");
        row1.getCellList().add("Payment Method");
        
        double totalAmount = 0.0;
        for (int i = stopIndx3; i < data.size(); i++)
        {
	        if (table2.getVisibleHeight() >= 670)
	        {
	        	endLine = false;
	        	stopIndx3 = i;
	        	break;
	        }
	        
	        Row row2 = table2.getRows().add(30);
	        row2.setVAlign(CellVAlign.TOP);
	        row2.getCellList().add(data.get(i)[0]);
	        row2.getCellList().add(data.get(i)[1]);
	        
	        String pmethod = data.get(i)[3] + " [ Amount : " + data.get(i)[2] + " ]";
	        if (data.get(i)[3].equals("Check"))
	        	pmethod += "\nCheck No : " + data.get(i)[4] + "\nCheck Date : " + data.get(i)[5] +	"\nBank : " + data.get(i)[6];
	        			
	        row2.getCellList().add( pmethod);
	        
	        totalAmount += Double.valueOf(data.get(i)[2].replace(",", ""));
        }
        
        Row row3 = table2.getRows().add(30);
        Cell cell2 = row3.getCellList().add("Total", Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 2);
        cell2.setAlign(CellAlign.RIGHT);
        cell2.setVAlign(CellVAlign.CENTER);
        Cell cell3 = row3.getCellList().add(String.valueOf(new DecimalFormat("#,###,###.##").format(totalAmount)), Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 2);
        cell3.setAlign(CellAlign.RIGHT);
        cell3.setVAlign(CellVAlign.CENTER);
                         
        // Add the table to the page
        _page.getElements().add(table2);
        
        _document.getPages().add(_page);
        
        return endLine;
	}
	
	private boolean generateCreditSalesPDF(Document _document)
	{
		int yOffset = 0;
		boolean endLine = true;
		
		Page _page = new Page(PageSize.LETTER, PageOrientation.PORTRAIT, 30.0f);
		
		// Create table
        Table table2 = new Table(0, 0, 600, 700);
        
        // Add columns to the table
        table2.getColumns().add(100);
        table2.getColumns().add(100);
        table2.getColumns().add(200);
        table2.getColumns().add(100);
         
        // Add rows to the table and add cells to the rows
        Row row1 = table2.getRows().add(40, Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray());
        
        row1.setAlign(CellAlign.CENTER);
        row1.setVAlign(CellVAlign.CENTER);
        row1.getCellList().add("Receipt No.");
        row1.getCellList().add("Due Date");
        row1.getCellList().add("Customer Name");
        row1.getCellList().add("Amount");        
         
        yOffset = 40;
        
        double totalAmount = 0.0;
        for (int i = stopIndx4; i < data.size(); i++)
        {
        	yOffset += 30;
	        
	        if (yOffset >= 670)
	        {
	        	endLine = false;
	        	stopIndx4 = i;
	        	break;
	        }
	        
	        Row row2 = table2.getRows().add(30);
	        row2.setVAlign(CellVAlign.BOTTOM);
	        row2.getCellList().add(data.get(i)[0]);
	        row2.getCellList().add(data.get(i)[1]);
	        row2.getCellList().add(data.get(i)[2]);
	        Cell cell1 = row2.getCellList().add(data.get(i)[3]);
	        cell1.setAlign(CellAlign.RIGHT);
	        
	        totalAmount += Double.valueOf(data.get(i)[3].replace(",", ""));
        }
        
        Row row3 = table2.getRows().add(30);
        Cell cell2 = row3.getCellList().add("Total", Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 3);
        cell2.setAlign(CellAlign.RIGHT);
        cell2.setVAlign(CellVAlign.CENTER);
        Cell cell3 = row3.getCellList().add(String.valueOf(new DecimalFormat("#,###,###.##").format(totalAmount)), Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 2);
        cell3.setAlign(CellAlign.RIGHT);
        cell3.setVAlign(CellVAlign.CENTER);
 
        // Add the table to the page
        _page.getElements().add(table2);
        
        _document.getPages().add(_page);
        
        return endLine;
	}
	
	private boolean generateProductSoldPDF(Document _document)
	{
		int yOffset = 0;
		boolean endLine = true;
		
		Page _page = new Page(PageSize.LETTER, PageOrientation.PORTRAIT, 30.0f);
		
		// Create table
        Table table2 = new Table(0, 0, 600, 700);
        
        // Add columns to the table
        table2.getColumns().add(50);
        table2.getColumns().add(100);
        table2.getColumns().add(100);
        table2.getColumns().add(100);
        table2.getColumns().add(100);
        table2.getColumns().add(100);
         
        // Add rows to the table and add cells to the rows
        Row row1 = table2.getRows().add(40, Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray());
        
        row1.setAlign(CellAlign.CENTER);
        row1.setVAlign(CellVAlign.CENTER);
        Cell cell1 = row1.getCellList().add("Product Name");
        cell1.setColumnSpan(5);
        row1.getCellList().add("Total");
        
        yOffset = 40;
        
        for (int i = stopIndx1; i < data.size(); i++)
        {
        	if (stopIndx2 == 0 && !prodWritten)
        	{
	        	// draw product name and total
	        	Row row2 = table2.getRows().add(30, Font.getHelveticaBold(), 16, RgbColor.getBlack(), null);
		        row2.setVAlign(CellVAlign.BOTTOM);
		        Cell cell2 = row2.getCellList().add(data.get(i)[0]);
		        cell2.setColumnSpan(5);
		        Cell cell3 = row2.getCellList().add(data.get(i)[1]);
		        cell3.setAlign(CellAlign.RIGHT);
		        
		        prodWritten = true;
		        
		        yOffset += 30;
	            
		        if (yOffset >= 700)
		        {
		        	endLine = false;
		        	stopIndx1 = i - 1;
		        	break;
		        }
        	}
	        
	        // Add rows to the table and add cells to the rows
            Row row4 = table2.getRows().add(40, Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray());
            
            row4.setAlign(CellAlign.CENTER);
            row4.setVAlign(CellVAlign.CENTER);
            row4.getCellList().add("");
            row4.getCellList().add("DR No");
            row4.getCellList().add("Qty");
            row4.getCellList().add("Selling Price");
            row4.getCellList().add("Discount");
            row4.getCellList().add("Sub Total");
            
            yOffset += 40;
            
	        if (yOffset >= 700)
	        {
	        	endLine = false;
	        	stopIndx1 = i -1;
	        	break;
	        }
            
	        String[] drs = data.get(i)[2].split(",");
	        String[] qtys = data.get(i)[3].split(",");
	        String[] sprices = data.get(i)[4].split(",");
	        String[] discs = data.get(i)[5].split(",");
	        String[] stotals = data.get(i)[6].split(",");
	        
	        for (int o = stopIndx2; o < drs.length; o++)
	        {	            
		        Row row3 = table2.getRows().add(30);
		        row3.setVAlign(CellVAlign.BOTTOM);
		        row3.getCellList().add("");
		        row3.getCellList().add(drs[o]);
		        Cell cell4 = row3.getCellList().add(qtys[o]);
		        cell4.setAlign(CellAlign.RIGHT);
		        Cell cell5 = row3.getCellList().add(sprices[o]);
		        cell5.setAlign(CellAlign.RIGHT);
		        Cell cell6 = row3.getCellList().add(discs[o]);
		        cell6.setAlign(CellAlign.RIGHT);
		        Cell cell7 = row3.getCellList().add(stotals[o]);
		        cell7.setAlign(CellAlign.RIGHT);
		        
		        yOffset += 30;
		        
		        if (yOffset >= 700)
		        {
		        	endLine = false;
		        	stopIndx2 = o;
		        	break;
		        }
	        }
	        
	        if (stopIndx2 > 0 && endLine)
	        {
	        	stopIndx2 = 0;
	        }
	        
        	prodWritten = false;
        }
 
        // Add the table to the page
        _page.getElements().add(table2);
        
        _document.getPages().add(_page);
        
        return endLine;
	}
	
	private boolean generateCashCollectionPDF(Document _document)
	{
		boolean endLine = true;
		int yOffset = 0;
		
		Page _page = new Page(PageSize.LETTER, PageOrientation.PORTRAIT, 30.0f);
		
		// Create table
        Table table2 = new Table(0, 0, 600, 700);
        
        // Add columns to the table
        table2.getColumns().add(100);
        table2.getColumns().add(300);
        table2.getColumns().add(100);
         
        // Add rows to the table and add cells to the rows
        Row row1 = table2.getRows().add(40, Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray());
        
        row1.setAlign(CellAlign.CENTER);
        row1.setVAlign(CellVAlign.CENTER);
        row1.getCellList().add("Collection No.");
        row1.getCellList().add("Customer Name");
        row1.getCellList().add("Amount");        
         
        yOffset = 40;
        
        double totalAmount = 0.0;
        for (int i = stopIndx5; i < data.size(); i++)
        {
        	yOffset += 30;
	        
	        if (yOffset >= 670)
	        {
	        	endLine = false;
	        	stopIndx5 = i;
	        	break;
	        }
	        
	        Row row2 = table2.getRows().add(30);
	        row2.setVAlign(CellVAlign.BOTTOM);
	        row2.getCellList().add(data.get(i)[0]);
	        row2.getCellList().add(data.get(i)[1]);
	        Cell cell1 = row2.getCellList().add(data.get(i)[2]);
	        cell1.setAlign(CellAlign.RIGHT);
	        
	        totalAmount += Double.valueOf(data.get(i)[2].replace(",", ""));
        }
        
        Row row3 = table2.getRows().add(30);
        Cell cell2 = row3.getCellList().add("Total", Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 2);
        cell2.setAlign(CellAlign.RIGHT);
        cell2.setVAlign(CellVAlign.CENTER);
        Cell cell3 = row3.getCellList().add(String.valueOf(new DecimalFormat("#,###,###.##").format(totalAmount)), Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 2);
        cell3.setAlign(CellAlign.RIGHT);
        cell3.setVAlign(CellVAlign.CENTER);
 
        // Add the table to the page
        _page.getElements().add(table2);
        
        _document.getPages().add(_page);
        
        return endLine;
	}
	
	private boolean generateProductInventory(Document _document)
	{
		boolean endLine = true;
	
		Page _page = new Page(PageSize.LETTER, PageOrientation.PORTRAIT, 30.0f);
		
		// Create table
        Table table2 = new Table(0, 0, 600, 700);
        
        // Add columns to the table
        table2.getColumns().add(400);
        table2.getColumns().add(100);
         
        // Add rows to the table and add cells to the rows
        Row row1 = table2.getRows().add(40, Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray());
        
        row1.setAlign(CellAlign.CENTER);
        row1.setVAlign(CellVAlign.CENTER);
        row1.getCellList().add("Product Description");
        row1.getCellList().add("Quantity");        
        	
        double totalQuantity = 0.0;
        for (int i = stopIndx6; i < data.size(); i++)
        {        
	        if (table2.getVisibleHeight() >= 670)
	        {
	        	endLine = false;
	        	stopIndx6 = i;
	        	break;
	        }
	        
	        Row row2 = table2.getRows().add(30);
	        row2.setVAlign(CellVAlign.BOTTOM);
	        row2.getCellList().add(data.get(i)[0]);
	        Cell cell1 = row2.getCellList().add(data.get(i)[1]);
	        cell1.setAlign(CellAlign.RIGHT);
	        
	        totalQuantity += Double.valueOf(data.get(i)[1].replace(",", ""));
        }
        
        Row row3 = table2.getRows().add(30);
        Cell cell2 = row3.getCellList().add("Total", Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 1);
        cell2.setAlign(CellAlign.RIGHT);
        cell2.setVAlign(CellVAlign.CENTER);
        Cell cell3 = row3.getCellList().add(String.valueOf(new DecimalFormat("#,###,###.##").format(totalQuantity)), Font.getHelveticaBold(), 16, RgbColor.getBlack(), RgbColor.getGray(), 2);
        cell3.setAlign(CellAlign.RIGHT);
        cell3.setVAlign(CellVAlign.CENTER);
 
        // Add the table to the page
        _page.getElements().add(table2);
        
        _document.getPages().add(_page);
        
        return endLine;
	}
}
