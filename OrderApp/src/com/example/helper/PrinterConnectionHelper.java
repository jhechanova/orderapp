package com.example.helper;

import java.util.ArrayList;
import java.util.Set;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bixolon.printer.BixolonPrinter;
import com.example.classobj.CustomerProfile;
import com.example.classobj.MenuDetails;
import com.example.classobj.OrderHeader;
import com.example.classobj.SalesmanProfile;
import com.example.list.view.helper.MenuListAdapterHelper;
import com.example.list.view.helper.PrintersListAdapterHelper;
import com.example.list.view.helper.SalesmansListAdapterHelper;
import com.example.misc.AlertDialogManager;
import com.example.orderapp.MainActivity;
import com.example.orderapp.MainActivity.SyncData;
import com.example.orderapp.MainActivity.UploadCustomersToServer;
import com.example.orderapp.MainActivity.UploadOrdersToServer;
import com.example.orderapp.R;

public class PrinterConnectionHelper {
	// printer variables
	
	// SPP-R200II characters per line : 42
	// SPP-R300 characters per line : 64
	public static int cpl = 0;
	
	public boolean mIsConnected;
	
	private BixolonPrinter mBixolonPrinter;
	private String mConnectedDeviceName = null;
	private String mConnectedDeviceAddress = null;
	
	public int acenter = BixolonPrinter.ALIGNMENT_CENTER;
	public int aleft = BixolonPrinter.ALIGNMENT_LEFT;
	public int aright = BixolonPrinter.ALIGNMENT_RIGHT;
	
	public int fontC = BixolonPrinter.TEXT_ATTRIBUTE_FONT_C;
	public int bold = BixolonPrinter.TEXT_ATTRIBUTE_EMPHASIZED;
	
	public int sizeh1 = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
	
	private Context _context;
	
	public PrinterConnectionHelper(Context context) 
	{
		_context = context;
		
		mBixolonPrinter = new BixolonPrinter(context, mHandler, null);
	    mBixolonPrinter.findBluetoothPrinters();
	}
	
	public void printText(String text, int align, int attrib, int size)
	{
		mBixolonPrinter.printText(text, align, attrib, size, false);
	}
	
	public void printNextLine()
	{
		printText("\n", acenter, fontC,	sizeh1);
	}
	
	public void printDivider()
	{
		String dashes = "";
		for(int i=0; i<cpl; i++)
			dashes += "-";
		printText(dashes + "\n", acenter, fontC, sizeh1);
	}
		
	public void printHeader()
	{
		printText(getResources().getString(R.string.company_name) + "\n", acenter, fontC | bold, sizeh1);
		printText(getResources().getString(R.string.company_address1) + "\n", acenter, fontC, sizeh1);
		printText(getResources().getString(R.string.company_address2) + "\n", acenter, fontC, sizeh1);
		printText(getResources().getString(R.string.company_vat_tin) + "\n", acenter, fontC, sizeh1);
		mBixolonPrinter.lineFeed(1, false);
	}
	
	public void printLineFeed(int feed, boolean state)
	{
		mBixolonPrinter.lineFeed(feed,  state);
	}
	
	public void disconnectPrinter()
	{
		Toast.makeText(_context, "Disconnecting to " + mConnectedDeviceName, Toast.LENGTH_LONG).show();
		
		mBixolonPrinter.disconnect();
	}
	
	public void connectPrinter()
	{
		mBixolonPrinter.connect(mConnectedDeviceAddress);
	}
	
	private final Handler mHandler = new Handler(new Handler.Callback() {
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
				case BixolonPrinter.MESSAGE_STATE_CHANGE:
					switch (msg.arg1) {
						case BixolonPrinter.STATE_CONNECTED:
							Toast.makeText(_context, "Connected to " + mConnectedDeviceName + " ( " + mConnectedDeviceAddress + " )", Toast.LENGTH_LONG).show();
							mIsConnected = true;
							
							break;
							
						case BixolonPrinter.STATE_CONNECTING:
							Toast.makeText(_context, "Connecting to the printer", Toast.LENGTH_SHORT).show();
							
							break;
		
						case BixolonPrinter.STATE_NONE:
							Toast.makeText(_context, "Not connected to the printer", Toast.LENGTH_SHORT).show();
							mIsConnected = false;
							
							break;
					}
					return true;
					
				case BixolonPrinter.MESSAGE_PRINT_COMPLETE:
					Toast.makeText(_context, "Complete to print", Toast.LENGTH_SHORT).show();					
					break;
					
				case BixolonPrinter.MESSAGE_BLUETOOTH_DEVICE_SET:
					Set<BluetoothDevice> bluetoothDeviceSet = (Set<BluetoothDevice>) msg.obj;
					final ArrayList<BluetoothDevice> _printersList = new ArrayList<BluetoothDevice>();
					for (BluetoothDevice device : bluetoothDeviceSet) {
						_printersList.add(device);
						/**if (device.getName().equals("SPP-R200II")) { 
							// TODO: Connect printer
							mConnectedDeviceAddress = device.getAddress();
							break;
						}**/
					}
					
					final Dialog printerDialog = new Dialog(_context);
					printerDialog.setTitle("Printers");
					printerDialog.setContentView(R.layout.printer_list);
					ListView _listView = (ListView) printerDialog.findViewById(R.id.printer_list_view);
					
					PrintersListAdapterHelper _printersAdapterHelper = new PrintersListAdapterHelper((FragmentActivity)_context, _printersList);
					_listView.setAdapter(_printersAdapterHelper);

					printerDialog.setCancelable(false);
					printerDialog.show();
					
					_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> adapter, View view, int position, long arg)   {
						    // TODO Auto-generated method stub
							
							if ( _printersList.get(position).getName().equals("SPP-R200II"))
								cpl = 42;
							else if ( _printersList.get(position).getName().equals("SPP-R300"))
								cpl = 64;
							
							mConnectedDeviceAddress = _printersList.get(position).getAddress();
							
							printerDialog.dismiss();
						}
					});
					
					return true;
					
				case BixolonPrinter.MESSAGE_DEVICE_NAME:
					mConnectedDeviceName = msg.getData().getString(BixolonPrinter.KEY_STRING_DEVICE_NAME);
					return true;
			}
			return false;
		}
	});
	
	public Context getContext()
	{
		return _context;
	}
	
	public Resources getResources()
	{
		return _context.getResources();
	}
}
