package com.example.helper;

import java.io.File;

import android.os.Environment;
import android.util.Log;

public class DirectoryHelper {
	
	private static final String TAG = "OrderApp";
	
	public DirectoryHelper()
	{
		
	}
	
	public void CreateDirectory(String dirName)
	{
		Log.d(TAG, "Create Directory : "+Environment.getExternalStorageDirectory().getPath() + dirName );
 	    
		File mediaDir = new File( Environment.getExternalStorageDirectory().getPath() + dirName);
		
		if (!mediaDir.exists())
		{
			mediaDir.mkdir();
			mediaDir.setExecutable(true, false);
			mediaDir.setReadable(true, false);
			mediaDir.setWritable(true, false);
		}
	}
	
	public void CleanDirectory(String dirName)
	{
		Log.d(TAG, "Create Directory : "+Environment.getExternalStorageDirectory().getPath() + dirName );
 	    
		File mediaDir = new File( Environment.getExternalStorageDirectory().getPath() + dirName);
		
		if (mediaDir.exists())
		{
	        String[] children = mediaDir.list();
	        for (int i = 0; i < children.length; i++) {
	            new File(mediaDir, children[i]).delete();
	        }
	    }
	}
}
