package com.example.helper;

import java.io.File;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.LruCache;
import android.widget.ImageView;

public class ManageBitmapHelper {

	private LruCache<String, Bitmap> mMemoryCache;
	private Context _context;
	
	public ManageBitmapHelper(Context context)
	{
		_context = context;
		
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 8;
		
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize)
		{
			@Override
			protected int sizeOf(String key, Bitmap bitmap)
			{
				return bitmap.getByteCount() / 1024;
			}
		};
	}
	
	private void addBitmapToMemoryCache(String key, Bitmap bitmap)
	{
		if (getBitmapFromMemCache(key) == null)
		{
			mMemoryCache.put(key, bitmap);
		}
	}
	
	private Bitmap getBitmapFromMemCache(String key)
	{
		return mMemoryCache.get(key);
	}
	
	public void loadBitmap(int resId, ImageView imageView, String imageBitmap)
	{
		final String key = String.valueOf(resId);
		final Bitmap bitmap = getBitmapFromMemCache(key);
				
		if (bitmap != null)
		{
			imageView.setImageBitmap(bitmap);
		}
		else
		{
			new AddBitmapTask(imageView, imageBitmap).execute(resId);
		}
	}
	
	private Bitmap decodeSampledBitmapFromResource(Resources res, String imageStr, int reqWidth, int reqHeight)
	{

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(imageStr, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeFile(imageStr, options);
	}
	
	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
	{
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	
	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	
	    return inSampleSize;
	}
	
	class AddBitmapTask extends AsyncTask<Integer, Void, Bitmap> {
		private ImageView _imageView;
		private String _imageBitmap;
		
		public AddBitmapTask(ImageView imageView, String imageBitmap) {
			// TODO Auto-generated constructor stub
			_imageView = imageView;
			_imageBitmap = imageBitmap;
		}

		@Override
		protected void onPreExecute() {}

		protected Bitmap doInBackground(Integer... args) {
			Bitmap bitmap = null;
			
			try
			{
				bitmap = decodeSampledBitmapFromResource(_context.getResources(), _imageBitmap, 100, 100);
				addBitmapToMemoryCache(String.valueOf(args[0]), bitmap);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			
			return bitmap;
		}

		protected void onPostExecute(Bitmap result) {
			_imageView.setImageBitmap(result);
		}
	}
}
