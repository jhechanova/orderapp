package com.example.helper;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.example.orderapp.MainActivity;

public class GeoLocationHelper implements LocationListener {

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		MainActivity._longitude = (int) location.getLongitude();
		MainActivity._latitude = (int) location.getLatitude();
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

}
