package com.example.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;

import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.classobj.Banks;
import com.example.classobj.CustomerProfile;
import com.example.classobj.ItemProfile;
import com.example.classobj.ItemUoms;
import com.example.classobj.PaymentTerms;
import com.example.classobj.TaxTypes;

public class FunctionHelper {	
	public static String dbFormat = "yyyy-MM-dd";
	public static String fldFormat = "EEE, MMM dd, yyyy";
	public static String printFormat = "MM/dd/yyyy";
	
	public static String ipaddr = "";
	//public static String ipaddr = "10.100.60.2";
	public static String url = "";
	public static String urlnologin = "";
	public static String session = "";
	
	public static List<CustomerProfile> customerList = new ArrayList<CustomerProfile>();
	public static List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
	public static List<ItemProfile> itemsList = new ArrayList<ItemProfile>();
	public static List<ItemUoms> uomsList = new ArrayList<ItemUoms>();
	public static List<TaxTypes> taxTypesList = new ArrayList<TaxTypes>();
	public static List<Banks> banksList = new ArrayList<Banks>();
	
	public static final List<NameValuePair> params = new ArrayList<NameValuePair>();
	
	public static String ConvertDate(String fromFormat, String toFormat, String dateStr)
	{
		String returnStr = "";
		try
		{
			SimpleDateFormat format = null;
        
			format = new SimpleDateFormat(fromFormat);
			Date dat = format.parse(dateStr);
        
			format = new SimpleDateFormat(toFormat);				
			returnStr = format.format(dat);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return returnStr;
	}
	
	static public List<String> GetCustomers()
    {
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 0; i < customerList.size(); i++)
    		list.add(customerList.get(i).getCustomerName());
    		
    	return list;
    }
	
	static public int GetCustomersIndx(long id)
	{
		for (int i = 0; i < customerList.size(); i++)
    		if (customerList.get(i).getCustomerId() == id)
    			return i;
    	
    	return -1;
	}
	
	static public int GetCustomersIndx(String name)
	{
		for (int i = 0; i < customerList.size(); i++)
    		if (customerList.get(i).getCustomerName().equals(name))
    			return i;
    	
    	return -1;
	}
	
	static public List<String> GetPaymentTerms()
    {
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 0; i < paymentTermsList.size(); i++)
    		list.add(paymentTermsList.get(i).getName());
    		
    	return list;
    }
	
	static public int GetPaymentTermsIndx(long id)
    {    	
    	for (int i = 0; i < paymentTermsList.size(); i++)
    		if (paymentTermsList.get(i).getId() == id)
    			return i;
    	
    	return -1;
    }
	
	public static List<String> GetItems()
	{
		List<String> list = new ArrayList<String>();
    	
    	for (int i = 0; i < itemsList.size(); i++)
    		list.add(itemsList.get(i).getDescription());
    		
    	return list;
	}
	
	static public int GetItemsIndx(long id)
	{
		for (int i = 0; i < itemsList.size(); i++)
    		if (itemsList.get(i).getItemId() == id)
    			return i;
    	
    	return -1;
	}
	
	static public void SetItemQty(long id, double qty)
	{
		for (int i = 0; i < itemsList.size(); i++)
			if (itemsList.get(i).getItemId() == id)
				itemsList.get(i).setQty(qty);
	}
	
	static public int GetItemsIndx(String name)
	{
		for (int i = 0; i < itemsList.size(); i++)
    		if (itemsList.get(i).getDescription().equals(name))
    			return i;
    	
    	return -1;
	}
	
	static public List<String> GetUOMs()
    {
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 0; i < uomsList.size(); i++)
    		list.add(uomsList.get(i).getName());
    		
    	return list;
    }
	
	static public int GetUOMsIndx(long id)
    {    	
    	for (int i = 0; i < uomsList.size(); i++)
    		if (uomsList.get(i).getId() == id)
    			return i;
    	
    	return -1;
    }
	
	// added sept. 27, 2014
	static public List<String> GetBanks()
    {
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 0; i < banksList.size(); i++)
    		list.add(banksList.get(i).getName());
    		
    	return list;
    }
	
	static public int GetBanksIndx(long id)
    {    	
    	for (int i = 0; i < banksList.size(); i++)
    		if (banksList.get(i).getId() == id)
    			return i;
    	
    	return -1;
    }
	
	static public List<String> GetPaymentMethods()
    {
    	List<String> list = new ArrayList<String>();
    	
    	list.add("Cash");
    	list.add("Check");
    	    		
    	return list;
    }
	
	static public int GetPaymentMethodsIndx(String val)
    {    	
		List<String> list = new ArrayList<String>();
    	
    	list.add("Cash");
    	list.add("Check");

    	for (int i = 0; i < list.size(); i++)
    	{
    		if (list.get(i).equals(val))
    		{
    			return i;
    		}
    	}
    	
    	return -1;
    }
	
	static public List<String> GetTaxTypes()
    {
    	List<String> list = new ArrayList<String>();
    	
    	for (int i = 0; i < taxTypesList.size(); i++)
    		list.add(taxTypesList.get(i).getName());
    		
    	return list;
    }
	
	static public int GetTaxTypesIndx(long id)
    {    	
    	for (int i = 0; i < taxTypesList.size(); i++)
    		if (taxTypesList.get(i).getId() == id)
    			return i;
    	
    	return -1;
    }
	
	public static void getListViewSize(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }

        int totalHeight = 0;
        for (int size = 0; size < myListAdapter.getCount(); size++) {
            totalHeight += 50;
        }

        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight;
        myListView.setLayoutParams(params);
    }
	
	public static String concatWithSpace(String s1, String s2)
	{
		return s1 + String.format("%" + (PrinterConnectionHelper.cpl-(s1.length()+s2.length())) + "s", "") + s2;
	}
	
	public static String indentString(String s1, int indent)
	{
		return String.format("%"+indent+"s%s", "", s1);
	}
	
	public static String lpad(long value, int padding)
	{
		return String.format("%0"+padding+"d", value);
	}
	
	public static String lpad(int value, int padding)
	{
		return String.format("%0"+padding+"d", value);
	}
}
