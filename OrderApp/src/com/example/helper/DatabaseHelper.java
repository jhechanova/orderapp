package com.example.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Environment;

import com.example.classobj.Banks;
import com.example.classobj.CustomerCollection;
import com.example.classobj.CustomerProfile;
import com.example.classobj.ItemProfile;
import com.example.classobj.ItemUoms;
import com.example.classobj.OrderHeader;
import com.example.classobj.PaymentTerms;
import com.example.classobj.SalesmanProfile;
import com.example.classobj.TaxTypes;
import com.example.orderapp.MainActivity;

public class DatabaseHelper extends SQLiteOpenHelper {
		
	private static final String DB_NAME = Environment.getExternalStorageDirectory().getPath() +  "/OrderApp/db/";
	private static final int DB_VERSION = 1;
	
	private ItemProfile myItemProfile;
	
	private Context myContext;
	private String myType;
	
	public long partnerID;
	
	public DatabaseHelper(Context context, String DBtoUse)
	{
		super (context, DB_NAME+DBtoUse+".sql3", null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)	{}
	
	public void createTable()
	{		
		String sqlQuery = "";
		SQLiteDatabase db = this.getWritableDatabase();
		
		// salesmans table
		sqlQuery = "create table if not exists salesmans ( salesman_id integer primary key, salesman_name text, selected integer )";
		db.execSQL(sqlQuery);
		
		// payment terms table
		sqlQuery = "create table if not exists payment_terms ( id integer primary key, name text, days integer, days_allowable integer)";
		db.execSQL(sqlQuery);
		
		// customers table
		sqlQuery = "create table if not exists customers ( customer_id integer primary key, customer_name text, address text,"
				+ " contact_no text, salesman_id integer, prof_image_path text, geo_latitude double,"
				+ " geo_longitude double, route_days integer, comp_image_path text, tin text, term integer, tax_type integer )"; // modified - check
		db.execSQL(sqlQuery);
		
		// items table
		sqlQuery = "create table if not exists items ( item_id integer primary key, sku text,"
				+ " description text, price real, qty real, consignment_no integer, salesman_id integer, uom integer )";
		db.execSQL(sqlQuery);
		
		// order table
		sqlQuery = "create table if not exists order_header ( order_no integer primary key autoincrement, salesman_id integer, customer_id integer,"
				+ " order_date date, order_items_discount text, order_items text, order_items_qty text, order_total_price text, payment_terms integer, "
				+ " status text, payment_method text, amount double, bank integer, check_no text, check_date date )"; // modified sept 27, 2014
		db.execSQL(sqlQuery);
		
		// customers updated table
		sqlQuery = "create table if not exists customers_update ( id integer, salesman_id integer )";
		db.execSQL(sqlQuery);
		
		// collections table
		sqlQuery = "create table if not exists collections ( collection_no integer primary key autoincrement, salesman_id integer, customer_id integer,"
				+ " collection_date date, ref_dates text, ref_numbers text, ref_amounts text, total_collection double, applied_amount string,"
				+ " status text,  payment_method text, amount double, bank integer, check_no text, check_date date )";
		db.execSQL(sqlQuery);
		
		// uom table
		sqlQuery = "create table if not exists uoms (id integer primary key, short text, name text)";
		db.execSQL(sqlQuery);
		
		// tax types table
		sqlQuery = "create table if not exists tax_types (id integer primary key, name text, rate real)";
		db.execSQL(sqlQuery);
		
		// tax types table
		sqlQuery = "create table if not exists banks (id integer primary key, code text, name text)";
		db.execSQL(sqlQuery);
	}
	
	public void dropTable()
	{
		String sqlQuery = "";
		SQLiteDatabase db = this.getWritableDatabase();
		
		// salesmans table
		//sqlQuery = "drop table if exists salesmans";
		//db.execSQL(sqlQuery);
		
		// payment terms table
		//sqlQuery = "drop table if exists payment_terms";
		//db.execSQL(sqlQuery);
		
		// customers table
		sqlQuery = "drop table if exists customers";
		db.execSQL(sqlQuery);
		
		// items table
		sqlQuery = "drop table if exists items";
		db.execSQL(sqlQuery);
		
		// order table
		sqlQuery = "drop table if exists order_header";
		db.execSQL(sqlQuery);
		
		// customers update table
		sqlQuery = "drop table if exists customers_update";
		db.execSQL(sqlQuery);
		
		// collections table
		sqlQuery = "drop table if exists collections";
		db.execSQL(sqlQuery);
		
		createTable();
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
	
	// items corner
	
	public ItemProfile getItem(long itemID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		String sqlUQuery = "select * from items where item_id=" + itemID + " and salesman_id=" + MainActivity.salesmanId;
		
		Cursor cursor = db.rawQuery(sqlUQuery, null);
		
		if (cursor != null)
		{
			if (cursor.moveToFirst())
			{
				ItemProfile itemProfile = new ItemProfile(qIntValue(cursor, "item_id"), qStringValue(cursor, "sku"), 
					qStringValue(cursor, "description"), qDoubleValue(cursor, "price"), qDoubleValue(cursor, "qty"), 
					qIntValue(cursor, "consignment_no"), qIntValue(cursor, "uom"));
			
				return itemProfile;
			}
		}
		
		return null;
	}
	
	public ArrayList<ItemProfile> getItems()
	{
		try {
			String sqlUQuery = "select * from items where salesman_id=" + MainActivity.salesmanId;
			return new GetItemsTask().execute(sqlUQuery).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<ItemProfile> getSelectedItems(String cond)
	{
		try {
			String sqlUQuery = "select * from items where item_id in (" + cond + ") and salesman_id=" + MainActivity.salesmanId;
			return new GetItemsTask().execute(sqlUQuery).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<ItemProfile> getNotZeroQtyItems()
	{
		try {
			String sqlUQuery = "select * from items where qty > 0 and salesman_id=" + MainActivity.salesmanId;
			return new GetItemsTask().execute(sqlUQuery).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	class GetItemsTask extends AsyncTask<String, Void, ArrayList<ItemProfile>>{
		@Override
		protected ArrayList<ItemProfile> doInBackground(String... params) {
			SQLiteDatabase db = getWritableDatabase();
			ArrayList<ItemProfile> itemProfile = new ArrayList<ItemProfile>();
			String sqlUQuery = params[0];
			
			Cursor cursor = db.rawQuery(sqlUQuery, null);
			
			if (cursor.moveToFirst())
			{
				do {
					ItemProfile _itemProfile = new ItemProfile(qIntValue(cursor, "item_id"), qStringValue(cursor, "sku"), 
							qStringValue(cursor, "description"), qDoubleValue(cursor, "price"), qDoubleValue(cursor, "qty"), 
							qIntValue(cursor, "consignment_no"), qIntValue(cursor, "uom"));
					itemProfile.add(_itemProfile);
					
				} while(cursor.moveToNext());
			}
			
			return itemProfile;
		}
	}
	
	public void addNewItem(ItemProfile itemProfile)
	{
		myType = "save";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cValues = new ContentValues();
		cValues.put("item_id", itemProfile.getItemId());
		cValues.put("sku", itemProfile.getSKU());
		cValues.put("description", itemProfile.getDescription());
		cValues.put("price", itemProfile.getPrice());
		cValues.put("qty", itemProfile.getQty());
		cValues.put("consignment_no", itemProfile.getConsignmentNo());
		cValues.put("salesman_id", MainActivity.salesmanId);
		cValues.put("uom", itemProfile.getUOM());
				
		try
		{
			db.insert("items", "nullColumnHack", cValues);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
					
		db.close();
	}
	
	public void updateItemQty(long itemId, double itemQty)
	{
		myType = "update";
		
		SQLiteDatabase db = this.getWritableDatabase();
			
		try
		{
			String sqlQuery = "update items set qty=(qty-" + itemQty + ") where item_id=" + itemId + " and salesman_id=" + MainActivity.salesmanId;
			db.execSQL(sqlQuery);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
					
		db.close();
	}
	
	// end items corner
	
	// customers corner
	
	public CustomerProfile getCustomer(long customerID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		String sqlUQuery = "select * from customers where customer_id=" + customerID + " and salesman_id=" + MainActivity.salesmanId;
		
		Cursor cursor = db.rawQuery(sqlUQuery, null);
		
		if (cursor != null)
		{
			if (cursor.moveToFirst())
			{
				CustomerProfile customerProfile = new CustomerProfile(qIntValue(cursor, "customer_id"), qStringValue(cursor, "customer_name"), 
					qStringValue(cursor, "address"), qStringValue(cursor, "contact_no"), qStringValue(cursor, "prof_image_path"), 
					qDoubleValue(cursor, "geo_latitude"), qDoubleValue(cursor, "geo_longitude"), qIntValue(cursor, "route_days"),
					qStringValue(cursor, "comp_image_path"), qStringValue(cursor, "tin"), qIntValue(cursor, "term"), 
					qIntValue(cursor, "tax_type")); // modified - check
			
				return customerProfile;
			}
		}
		
		return null;
	}
	
	public String getCustomersUpdated()
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		String sqlUQuery = "select group_concat(id) as updated_ids from customers_update where salesman_id=?";
		
		Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(MainActivity.salesmanId)});
		
		if (cursor != null)
		{
			if (cursor.moveToFirst())
			{
				return qStringValue(cursor, "updated_ids");
			}
		}
		
		return null;
	}
	
	public ArrayList<CustomerProfile> getCustomers()
	{
		try {
			return new GetCustomersTask().execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<CustomerProfile> getSelectedCustomers(String cond)
	{
		try {
			return new GetCustomersTask().execute(cond).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	class GetCustomersTask extends AsyncTask<String, Void, ArrayList<CustomerProfile>>{
		@Override
		protected ArrayList<CustomerProfile> doInBackground(String... params) {
			SQLiteDatabase db = getWritableDatabase();
			ArrayList<CustomerProfile> customerProfile = new ArrayList<CustomerProfile>();
			String sqlUQuery;
			
			if (params.length > 0)
			{
				sqlUQuery = "select * from customers where " + params[0] + " and salesman_id=" + MainActivity.salesmanId;
			}
			else
			{
				sqlUQuery = "select * from customers where salesman_id=" + MainActivity.salesmanId;
			}
			
			Cursor cursor = db.rawQuery(sqlUQuery, null);
			
			if (cursor.moveToFirst())
			{
				do {
					CustomerProfile _customerProfile = new CustomerProfile(qIntValue(cursor, "customer_id"), qStringValue(cursor, "customer_name"), 
							qStringValue(cursor, "address"), qStringValue(cursor, "contact_no"), qStringValue(cursor, "prof_image_path"), 
							qDoubleValue(cursor, "geo_latitude"), qDoubleValue(cursor, "geo_longitude"), 
							qIntValue(cursor, "route_days"), qStringValue(cursor, "comp_image_path"), qStringValue(cursor, "tin"),
							qIntValue(cursor, "term"), qIntValue(cursor, "tax_type")); // modified - check
					customerProfile.add(_customerProfile);
					
				} while(cursor.moveToNext());
			}
			
			return customerProfile;
		}
	}
	
	public long addNewCustomer(CustomerProfile customerProfile)
	{
		myType = "save";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cValues = new ContentValues();
		// modified
		if (customerProfile.getCustomerId() > 0)
			cValues.put("customer_id", customerProfile.getCustomerId());
		//end
		cValues.put("customer_name", customerProfile.getCustomerName());
		cValues.put("address", customerProfile.getAddress());
		cValues.put("contact_no", customerProfile.getContactNo());
		cValues.put("salesman_id", MainActivity.salesmanId);
		// added
		cValues.put("prof_image_path", customerProfile.getProfImagePath());
		cValues.put("comp_image_path", customerProfile.getCompImagePath());
		cValues.put("geo_latitude", customerProfile.getGeoLatitude());
		cValues.put("geo_longitude", customerProfile.getGeoLongitude());
		cValues.put("route_days", customerProfile.getRouteDays());
		cValues.put("tin", customerProfile.getTin());
		cValues.put("term", customerProfile.getTerm());
		cValues.put("tax_type", customerProfile.getTaxType());
		// end
				
		long Insert_ID = 0;
		try
		{
			Insert_ID = db.insert("customers", "nullColumnHack", cValues);
			
			// insert to cutomers_update
			if (customerProfile.getCustomerId() == 0 && Insert_ID > -1)
			{
				ContentValues cValuesI = new ContentValues();
				cValuesI.put("id", Insert_ID);
				cValuesI.put("salesman_id", MainActivity.salesmanId);
				db.insert("customers_update", "nullColumnHack", cValuesI);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
					
		db.close();
		
		return Insert_ID;
	}
	
	// added
	public void updateCustomer(CustomerProfile customerProfile)
	{
		myType = "update";
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cValues = new ContentValues();
		cValues.put("customer_name", customerProfile.getCustomerName());
		cValues.put("address", customerProfile.getAddress());
		cValues.put("contact_no", customerProfile.getContactNo());
		cValues.put("prof_image_path", customerProfile.getProfImagePath());
		cValues.put("comp_image_path", customerProfile.getCompImagePath());
		cValues.put("geo_latitude", customerProfile.getGeoLatitude());
		cValues.put("geo_longitude", customerProfile.getGeoLongitude());
		cValues.put("tin", customerProfile.getTin());
		cValues.put("term", customerProfile.getTerm());
		cValues.put("tax_type", customerProfile.getTaxType());
		
		db.update("customers", cValues, "customer_id=? and salesman_id=?", new String[] { String.valueOf(customerProfile.getCustomerId()), String.valueOf(MainActivity.salesmanId) } );
		
		// insert to cutomers_update
		ContentValues cValuesI = new ContentValues();
		cValuesI.put("id", customerProfile.getCustomerId());
		cValuesI.put("salesman_id", MainActivity.salesmanId);
		db.insert("customers_update", "nullColumnHack", cValuesI);
	}
	// end
	
	// end customers corner
	
	// orders corner
	
	public OrderHeader getOrder(long orderID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		String sqlUQuery = "select a.order_no, a.customer_id, a.order_date, a.order_items, c.name as payment_terms_name," +
				" a.payment_method, a.check_no, a.check_date, a.amount, " + // added sept. 27, 2014
				" d.id as bank_id, d.name as bank_name, c.id as payment_terms_id, a.status, a.order_items_discount, " +
				" a.order_items_qty, a.order_total_price, b.customer_name, b.address, b.contact_no" +
				" from order_header a left join customers b on a.customer_id=b.customer_id" +
				" left join payment_terms c on a.payment_terms=c.id" +
				" left join banks d on a.bank=d.id" +
				" where order_no=? and a.salesman_id=?";
		
		Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(orderID), String.valueOf(MainActivity.salesmanId)});
		
		if (cursor != null)
		{
			if (cursor.moveToFirst())
			{
				/**OrderHeader orderHeader = new OrderHeader(qIntValue(cursor, "order_no"), qIntValue(cursor, "customer_id"),
					qStringValue(cursor, "customer_name"), 
					FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
					qStringValue(cursor, "order_date")), qStringValue(cursor, "address"), qStringValue(cursor, "contact_no"),
					qStringValue(cursor, "order_items"), qStringValue(cursor, "order_items_qty"), 
					qDoubleValue(cursor, "order_total_price"), qIntValue(cursor, "payment_terms_id"),
					qStringValue(cursor, "payment_terms_name"), qStringValue(cursor, "status"),
					qStringValue(cursor, "order_items_discount"));**/
				OrderHeader orderHeader = new OrderHeader(qIntValue(cursor, "order_no"), qIntValue(cursor, "customer_id"),
						FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
						qStringValue(cursor, "order_date")), qStringValue(cursor, "order_items"), qStringValue(cursor, "order_items_qty"), 
						qDoubleValue(cursor, "order_total_price"), qIntValue(cursor, "payment_terms_id"),
						qStringValue(cursor, "payment_terms_name"), qStringValue(cursor, "status"),
						qStringValue(cursor, "order_items_discount"), qStringValue(cursor, "payment_method"), 
						qDoubleValue(cursor, "amount"), qStringValue(cursor, "check_no"), 
						FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
						qStringValue(cursor, "check_date")),qIntValue(cursor, "bank_id"), qStringValue(cursor, "bank_name")); // modified sept. 27, 2014
			
				return orderHeader;
			}
		}
		
		return null;
	}
	
	public ArrayList<OrderHeader> getOrders()
	{
		try {
			return new GetOrdersTask().execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	class GetOrdersTask extends AsyncTask<String, Void, ArrayList<OrderHeader>>{
		
		protected ArrayList<OrderHeader> doInBackground(String... params) {
			SQLiteDatabase db = getWritableDatabase();
			ArrayList<OrderHeader> orderHeader = new ArrayList<OrderHeader>();
			String sqlUQuery;
			
		   sqlUQuery = "select a.order_no, a.customer_id, a.order_date, a.order_items, c.name as payment_terms_name," +
				" a.payment_method, a.check_no, a.check_date, a.amount, " + // added sept. 27, 2014
				" d.id as bank_id, d.name as bank_name, c.id as payment_terms_id, a.status, a.order_items_discount, " +
				" a.order_items_qty, a.order_total_price, b.customer_name, b.address, b.contact_no" +
				" from order_header a left join customers b on a.customer_id=b.customer_id" +
				" left join payment_terms c on a.payment_terms=c.id " +
				" left join banks d on a.bank=d.id where a.salesman_id=" + MainActivity.salesmanId;
			
			Cursor cursor = db.rawQuery(sqlUQuery, null);
			
			if (cursor.moveToFirst())
			{
				do {
					/**OrderHeader _orderHeader = new OrderHeader(qIntValue(cursor, "order_no"), qIntValue(cursor, "customer_id"),
						qStringValue(cursor, "customer_name"), 
						FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
						qStringValue(cursor, "order_date")), qStringValue(cursor, "address"), qStringValue(cursor, "contact_no"), 
						qStringValue(cursor, "order_items"), qStringValue(cursor, "order_items_qty"), 
						qDoubleValue(cursor, "order_total_price"), qIntValue(cursor, "payment_terms_id"), 
						qStringValue(cursor, "payment_terms_name"), qStringValue(cursor, "status"),
						qStringValue(cursor, "order_items_discount"));**/
					
					OrderHeader _orderHeader = new OrderHeader(qIntValue(cursor, "order_no"), qIntValue(cursor, "customer_id"),
							FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
							qStringValue(cursor, "order_date")), qStringValue(cursor, "order_items"), qStringValue(cursor, "order_items_qty"), 
							qDoubleValue(cursor, "order_total_price"), qIntValue(cursor, "payment_terms_id"), 
							qStringValue(cursor, "payment_terms_name"), qStringValue(cursor, "status"),
							qStringValue(cursor, "order_items_discount"), qStringValue(cursor, "payment_method"), 
							qDoubleValue(cursor, "amount"), qStringValue(cursor, "check_no"), 
							FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
							qStringValue(cursor, "check_date")),qIntValue(cursor, "bank_id"), qStringValue(cursor, "bank_name"));
					orderHeader.add(_orderHeader);
					
				} while(cursor.moveToNext());
			}
			
			return orderHeader;
		}
	}
	
	public void addNewOrder(OrderHeader orderHeader)
	{
		myType = "save";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cValues = new ContentValues();
		//cValues.put("order_no", orderHeader.getOrderNo());
		cValues.put("customer_id", orderHeader.getCustomerId());
		cValues.put("order_date", FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, orderHeader.getOrderDate()));
		cValues.put("order_items", orderHeader.getOrderItems());
		cValues.put("order_items_qty", orderHeader.getOrderItemsQty());
		cValues.put("order_total_price", orderHeader.getOrderTotalPrice());
		cValues.put("payment_terms", orderHeader.getPaymentTermsId());
		cValues.put("status", orderHeader.getStatus());
		cValues.put("order_items_discount", orderHeader.getOrderItemsDesc());
		
		// added sept. 27, 2014
		cValues.put("payment_method", orderHeader.getPaymentMethod());
		cValues.put("check_date", FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, orderHeader.getCheckDate()));
		cValues.put("check_no", orderHeader.getCheckNo());
		cValues.put("bank", orderHeader.getBankId());
		cValues.put("amount", orderHeader.getAmount());
		
		cValues.put("salesman_id", MainActivity.salesmanId);		
				
		try
		{
			db.insert("order_header", "nullColumnHack", cValues);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
					
		db.close();
	}
	
	public void updateOrder(OrderHeader orderHeader)
	{
		myType = "update";
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cValues = new ContentValues();
		cValues.put("order_date", FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, orderHeader.getOrderDate()));
		cValues.put("customer_id", orderHeader.getCustomerId());
		cValues.put("order_items", orderHeader.getOrderItems());
		cValues.put("order_items_qty", orderHeader.getOrderItemsQty());
		cValues.put("order_total_price", orderHeader.getOrderTotalPrice());
		cValues.put("payment_terms", orderHeader.getPaymentTermsId());
		cValues.put("status", orderHeader.getStatus());
		cValues.put("order_items_discount", orderHeader.getOrderItemsDesc());
		
		// added sept. 27, 2014
		cValues.put("payment_method", orderHeader.getPaymentMethod());
		cValues.put("check_date", FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, orderHeader.getCheckDate()));
		cValues.put("check_no", orderHeader.getCheckNo());
		cValues.put("bank", orderHeader.getBankId());
		cValues.put("amount", orderHeader.getAmount());
		
		db.update("order_header", cValues, "order_no=? and salesman_id=?", new String[] { String.valueOf(orderHeader.getOrderNo()), String.valueOf(MainActivity.salesmanId) } );
	}
	
	/**public void deleteOrder(long orderID)
	{
		myType = "delete";
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.delete("order_header", "order_no=? and salesman_id=?", new String[] { String.valueOf(orderID), String.valueOf(MainActivity.salesmanId) } );
	}**/
	
	// end orders corner
	
	
	// payment terms corner
	
	public PaymentTerms getPaymentTerm(long paymentTermID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		
		String sqlUQuery = "select name, id, days, days_allowable from payment_terms where id=?";
		
		Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(paymentTermID)});
		
		if (cursor != null)
		{
			if (cursor.moveToFirst())
			{
				PaymentTerms paymentTerms = new PaymentTerms(qStringValue(cursor, "name"), qIntValue(cursor, "id"),
						qIntValue(cursor, "days"), qIntValue(cursor, "days_allowable")); 
			
				return paymentTerms;
			}
		}
		
		return null;
	}
	
	public ArrayList<PaymentTerms> getPaymentTerms()
	{
		try {
			return new GetPaymentTermsTask().execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	class GetPaymentTermsTask extends AsyncTask<String, Void, ArrayList<PaymentTerms>>{
		
		protected ArrayList<PaymentTerms> doInBackground(String... params) {
			SQLiteDatabase db = getWritableDatabase();
			ArrayList<PaymentTerms> paymentTerms = new ArrayList<PaymentTerms>();
			String sqlUQuery;
			
			sqlUQuery = "select name, id, days, days_allowable from payment_terms";
			
			Cursor cursor = db.rawQuery(sqlUQuery, null);
			
			if (cursor.moveToFirst())
			{
				do {
					PaymentTerms _paymentTerms = new PaymentTerms(qStringValue(cursor, "name"), qIntValue(cursor, "id"),
							qIntValue(cursor, "days"), qIntValue(cursor, "days_allowable"));
					
					paymentTerms.add(_paymentTerms);
					
				} while(cursor.moveToNext());
			}
			
			return paymentTerms;
		}
	}
	
	public void addNewPaymentTerms(PaymentTerms paymentTerms)
	{
		myType = "save";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cValues = new ContentValues();
		//cValues.put("order_no", orderHeader.getOrderNo());
		cValues.put("id", paymentTerms.getId());
		cValues.put("name", paymentTerms.getName());
		cValues.put("days", paymentTerms.getDays());
		cValues.put("days_allowable", paymentTerms.getDaysAllowable());
				
		try
		{
			db.insert("payment_terms", "nullColumnHack", cValues);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
					
		db.close();
	}
	
	// end payment terms corner
	
	// banks corner - added sept. 27, 2014
	
		public Banks getBank(long bankID)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			
			String sqlUQuery = "select name, id, code from banks where id=?";
			
			Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(bankID)});
			
			if (cursor != null)
			{
				if (cursor.moveToFirst())
				{
					Banks _banks = new Banks(qStringValue(cursor, "name"), qIntValue(cursor, "id"),
							qStringValue(cursor, "code")); 
				
					return _banks;
				}
			}
			
			return null;
		}
		
		public ArrayList<Banks> getBanks()
		{
			try {
				return new GetBanksTask().execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		class GetBanksTask extends AsyncTask<String, Void, ArrayList<Banks>>{
			
			protected ArrayList<Banks> doInBackground(String... params) {
				SQLiteDatabase db = getWritableDatabase();
				ArrayList<Banks> _banks = new ArrayList<Banks>();
				String sqlUQuery;
				
				sqlUQuery = "select name, id, code from banks";
				
				Cursor cursor = db.rawQuery(sqlUQuery, null);
				
				if (cursor.moveToFirst())
				{
					do {
						Banks _bankso = new Banks(qStringValue(cursor, "name"), qIntValue(cursor, "id"),
								qStringValue(cursor, "code"));
						
						_banks.add(_bankso);
						
					} while(cursor.moveToNext());
				}
				
				return _banks;
			}
		}
		
		public void addNewBanks(Banks _banks)
		{
			myType = "save";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues cValues = new ContentValues();
			//cValues.put("order_no", orderHeader.getOrderNo());
			cValues.put("id", _banks.getId());
			cValues.put("name", _banks.getName());
			cValues.put("code", _banks.getCode());
					
			try
			{
				db.insert("banks", "nullColumnHack", cValues);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
						
			db.close();
		}
		
		// end banks corner
	
	// salesmans corner
	
		public SalesmanProfile getSalesman(long salesmanID)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			
			String sqlUQuery = "select salesman_name, salesman_id, selected from salesmans where salesman_id=?";
			
			Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(salesmanID)});
			
			if (cursor != null)
			{
				if (cursor.moveToFirst())
				{
					SalesmanProfile salesmanProfile = new SalesmanProfile(qIntValue(cursor, "salesman_id"), qStringValue(cursor, "salesman_name"), 
							((qIntValue(cursor, "selected") == 0)?false:true)); 
				
					return salesmanProfile;
				}
			}
			
			return null;
		}
		
		public ArrayList<SalesmanProfile> getSalesmans()
		{
			try {
				return new GetSalesmansTask().execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		class GetSalesmansTask extends AsyncTask<String, Void, ArrayList<SalesmanProfile>>{
			
			protected ArrayList<SalesmanProfile> doInBackground(String... params) {
				SQLiteDatabase db = getWritableDatabase();
				ArrayList<SalesmanProfile> salesmansProfile = new ArrayList<SalesmanProfile>();
				String sqlUQuery;
				
				sqlUQuery = "select salesman_name, salesman_id, selected from salesmans";
				
				Cursor cursor = db.rawQuery(sqlUQuery, null);
				
				if (cursor.moveToFirst())
				{
					do {
						SalesmanProfile salesmanProfile = new SalesmanProfile(qIntValue(cursor, "salesman_id"), qStringValue(cursor, "salesman_name"), 
								((qIntValue(cursor, "selected") == 0)?false:true)); 
					
						salesmansProfile.add(salesmanProfile);
						
					} while(cursor.moveToNext());
				}
				
				return salesmansProfile;
			}
		}
		
		public void addNewSalesmans(SalesmanProfile salesmanProfile)
		{
			myType = "save";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues cValues = new ContentValues();

			cValues.put("salesman_id", salesmanProfile.getSalesmanId());
			cValues.put("salesman_name", salesmanProfile.getSalesmanName());
			cValues.put("selected", 0);
					
			try
			{
				db.insert("salesmans", "nullColumnHack", cValues);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
						
			db.close();
		}
		
		public void updateSelectedSalesman(long salesmanID)
		{
			myType = "update";
			
			SQLiteDatabase db = this.getWritableDatabase();
				
			try
			{
				String sqlQuery = "update salesmans set selected=0";
				db.execSQL(sqlQuery);
				
				sqlQuery = "update salesmans set selected=1 where salesman_id=" + salesmanID;
				db.execSQL(sqlQuery);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
						
			db.close();
		}

		public SalesmanProfile getSelectedSalesman()
		{
			SQLiteDatabase db = this.getWritableDatabase();
			
			String sqlUQuery = "select salesman_name, salesman_id, selected from salesmans where selected=1";
			
			Cursor cursor = db.rawQuery(sqlUQuery, null);
			
			if (cursor != null)
			{
				if (cursor.moveToFirst())
				{
					SalesmanProfile salesmanProfile = new SalesmanProfile(qIntValue(cursor, "salesman_id"), qStringValue(cursor, "salesman_name"), true); 
				
					return salesmanProfile;
				}
			}
			
			return null;
		}
		
		// end salesmans corner
		
		// collections corner
		
		public CustomerCollection getCollection(long collectionID)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			
			String sqlUQuery = "select a.collection_no, a.collection_date, a.customer_id, a.ref_dates, a.ref_numbers, a.ref_amounts," +
					" a.payment_method, a.check_no, a.check_date, a.amount, " +
					" c.id as bank_id, c.name as bank_name , " +
					"a.total_collection, a.applied_amount, a.status, b.customer_name" +
					" from collections a left join customers b on a.customer_id=b.customer_id" +
					" left join banks c on a.bank=c.id " +
					" where collection_no=? and a.salesman_id=?";
			
			Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(collectionID), String.valueOf(MainActivity.salesmanId)});
			
			if (cursor != null)
			{
				if (cursor.moveToFirst())
				{
					CustomerCollection collection = new CustomerCollection(qIntValue(cursor, "customer_id"),
							qStringValue(cursor, "ref_dates"), qStringValue(cursor, "ref_amounts"), qStringValue(cursor, "ref_numbers"),
							qDoubleValue(cursor, "total_collection"), qStringValue(cursor, "payment_method"), 
							qDoubleValue(cursor, "amount"), qStringValue(cursor, "check_no"), 
							FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
							qStringValue(cursor, "check_date")),qIntValue(cursor, "bank_id"));
				
					collection.setCustomerName(qStringValue(cursor, "customer_name"));
					collection.setAppliedAmount(qStringValue(cursor, "applied_amount"));
					collection.setCollectionDate(qStringValue(cursor, "collection_date"));
					collection.setCollectionNo(qIntValue(cursor, "collection_no"));
					collection.setStatus(qStringValue(cursor, "status"));
					
					// added sept. 29 , 2014
					collection.setBankName(qStringValue(cursor, "bank_name"));
					
					return collection;
				}
			}
			
			return null;
		}
		
		public ArrayList<CustomerCollection> getCollections()
		{
			try {
				return new GetCollectionsTask().execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		public ArrayList<CustomerCollection> getSelectedCollections(String cond)
		{
			try {
				return new GetCollectionsTask().execute(cond).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}

		class GetCollectionsTask extends AsyncTask<String, Void, ArrayList<CustomerCollection>>{
			
			protected ArrayList<CustomerCollection> doInBackground(String... params) {
				SQLiteDatabase db = getWritableDatabase();
				ArrayList<CustomerCollection> collections = new ArrayList<CustomerCollection>();
				String sqlUQuery;
				
				if (params.length > 0)
				{
					sqlUQuery = "select a.collection_no, a.collection_date, a.customer_id, a.ref_dates, a.ref_numbers, a.ref_amounts," +
							"a.total_collection, a.applied_amount, a.status, b.customer_name," +
							" a.payment_method, a.check_no, a.check_date, a.amount, " +
							" c.id as bank_id, c.name as bank_name  " +
							" from collections a left join customers b on a.customer_id=b.customer_id" +
							" left join banks c on a.bank=c.id " +
							" where " + params[0] + " and a.salesman_id=" + MainActivity.salesmanId; 
				}
				else
				{
					sqlUQuery = "select a.collection_no, a.collection_date, a.customer_id, a.ref_dates, a.ref_numbers, a.ref_amounts," +
							"a.total_collection, a.applied_amount, a.status, b.customer_name," +
							" a.payment_method, a.check_no, a.check_date, a.amount, " +
							" c.id as bank_id, c.name as bank_name  " +
							" from collections a left join customers b on a.customer_id=b.customer_id" +
							" left join banks c on a.bank=c.id " +
							" where a.salesman_id=" + MainActivity.salesmanId;
				}
				
				Cursor cursor = db.rawQuery(sqlUQuery, null);
				
				if (cursor.moveToFirst())
				{
					do {
						CustomerCollection _collections = new CustomerCollection(qIntValue(cursor, "customer_id"), 
								qStringValue(cursor, "ref_dates"), qStringValue(cursor, "ref_amounts"), qStringValue(cursor, "ref_numbers"),
								qDoubleValue(cursor, "total_collection"), qStringValue(cursor, "payment_method"), 
								qDoubleValue(cursor, "amount"), qStringValue(cursor, "check_no"), 
								FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.fldFormat, 
								qStringValue(cursor, "check_date")),qIntValue(cursor, "bank_id"));
					
						_collections.setCustomerName(qStringValue(cursor, "customer_name"));
						_collections.setAppliedAmount(qStringValue(cursor, "applied_amount"));
						_collections.setCollectionDate(qStringValue(cursor, "collection_date"));
						_collections.setCollectionNo(qIntValue(cursor, "collection_no"));
						_collections.setStatus(qStringValue(cursor, "status"));
						
						
						// added sept. 29, 2014
						_collections.setBankName(qStringValue(cursor, "bank_name"));
						
						collections.add(_collections);
						
					} while(cursor.moveToNext());
				}
				
				return collections;
			}
		}
		
		public void addNewCollection(CustomerCollection collection)
		{
			// to do date
			
			myType = "save";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues cValues = new ContentValues();
			cValues.put("customer_id", collection.getCustomerId());
			cValues.put("collection_date", collection.getCollectionDate());
			cValues.put("ref_dates", collection.getRefDates());
			cValues.put("ref_amounts", collection.getRefAmounts());
			cValues.put("ref_numbers", collection.getRefNumbers());
			cValues.put("total_collection", collection.getTotalCollection());
			cValues.put("status", collection.getStatus());
			cValues.put("applied_amount", collection.getAppliedAmount());
			cValues.put("salesman_id", MainActivity.salesmanId);	
			
			// added sept. 29, 2014
			
			cValues.put("payment_method", collection.getPaymentMethod());
			cValues.put("check_date", FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, collection.getCheckDate()));
			cValues.put("check_no", collection.getCheckNo());
			cValues.put("bank", collection.getBankId());
			cValues.put("amount", collection.getAmount());
					
			try
			{
				db.insert("collections", "nullColumnHack", cValues);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
						
			db.close();
		}
		
		public void updateCollection(CustomerCollection collection)
		{
			myType = "update";
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues cValues = new ContentValues();
			cValues.put("status", collection.getStatus());
			cValues.put("applied_amount", collection.getAppliedAmount());
			
			// added sept. 29, 2014
			cValues.put("payment_method", collection.getPaymentMethod());
			cValues.put("check_date", FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, collection.getCheckDate()));
			cValues.put("check_no", collection.getCheckNo());
			cValues.put("bank", collection.getBankId());
			cValues.put("amount", collection.getAmount());
			
			db.update("collections", cValues, "collection_no=? and salesman_id=?", new String[] { String.valueOf(collection.getCollectionNo()), String.valueOf(MainActivity.salesmanId) } );
		}
		
		// end collections corner	
	
		// uoms corner
		
		public ItemUoms getUOM(long uomID)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			
			String sqlUQuery = "select id,short,name from uoms where id=?";
			
			Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(uomID)});
			
			if (cursor != null)
			{
				if (cursor.moveToFirst())
				{
					ItemUoms itemUoms = new ItemUoms(qStringValue(cursor, "name"), qStringValue(cursor, "short"), qIntValue(cursor, "id")); 
				
					return itemUoms;
				}
			}
			
			return null;
		}
		
		public ArrayList<ItemUoms> getUOMs()
		{
			try {
				return new GetUOMsTask().execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		class GetUOMsTask extends AsyncTask<String, Void, ArrayList<ItemUoms>>{
			
			protected ArrayList<ItemUoms> doInBackground(String... params) {
				SQLiteDatabase db = getWritableDatabase();
				ArrayList<ItemUoms> uoms = new ArrayList<ItemUoms>();
				String sqlUQuery;
				
				sqlUQuery = "select name, id, short from uoms";
				
				Cursor cursor = db.rawQuery(sqlUQuery, null);
				
				if (cursor.moveToFirst())
				{
					do {
						ItemUoms _itemUoms = new ItemUoms(qStringValue(cursor, "name"), qStringValue(cursor, "short"), qIntValue(cursor, "id"));
						
						uoms.add(_itemUoms);
						
					} while(cursor.moveToNext());
				}
				
				return uoms;
			}
		}
		
		public void addNewUOMs(ItemUoms uom)
		{
			myType = "save";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues cValues = new ContentValues();
			//cValues.put("order_no", orderHeader.getOrderNo());
			cValues.put("id", uom.getId());
			cValues.put("name", uom.getName());
			cValues.put("short", uom.getShort());
					
			try
			{
				db.insert("uoms", "nullColumnHack", cValues);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
						
			db.close();
		}

		// tax types corner
		
		public TaxTypes getTaxType(long taxTypeID)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			
			String sqlUQuery = "select id,name,rate from tax_types where id=?";
			
			Cursor cursor = db.rawQuery(sqlUQuery, new String[]{String.valueOf(taxTypeID)});
			
			if (cursor != null)
			{
				if (cursor.moveToFirst())
				{
					TaxTypes taxTypes = new TaxTypes(qStringValue(cursor, "name"), qDoubleValue(cursor, "rate"), qIntValue(cursor, "id")); 
				
					return taxTypes;
				}
			}
			
			return null;
		}
		
		public ArrayList<TaxTypes> getTaxTypes()
		{
			try {
				return new GetTaxTypesTask().execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		class GetTaxTypesTask extends AsyncTask<String, Void, ArrayList<TaxTypes>>{
			
			protected ArrayList<TaxTypes> doInBackground(String... params) {
				SQLiteDatabase db = getWritableDatabase();
				ArrayList<TaxTypes> tax_types = new ArrayList<TaxTypes>();
				String sqlUQuery;
				
				sqlUQuery = "select name, id, rate from tax_types";
				
				Cursor cursor = db.rawQuery(sqlUQuery, null);
				
				if (cursor.moveToFirst())
				{
					do {
						TaxTypes _taxTypes = new TaxTypes(qStringValue(cursor, "name"), qDoubleValue(cursor, "rate"), qIntValue(cursor, "id"));
						
						tax_types.add(_taxTypes);
						
					} while(cursor.moveToNext());
				}
				
				return tax_types;
			}
		}
		
		public void addNewTaxTypes(TaxTypes tax_type)
		{
			myType = "save";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			ContentValues cValues = new ContentValues();
			//cValues.put("order_no", orderHeader.getOrderNo());
			cValues.put("id", tax_type.getId());
			cValues.put("name", tax_type.getName());
			cValues.put("rate", tax_type.getRate());
					
			try
			{
				db.insert("tax_types", "nullColumnHack", cValues);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
						
			db.close();
		}
		// query
		
		public ArrayList<String[]> getReports(String query)
		{
			try {
				return new GetReportsTask().execute(query).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		class GetReportsTask extends AsyncTask<String, Void, ArrayList<String[]>>{
			
			protected ArrayList<String[]> doInBackground(String... params) {
				SQLiteDatabase db = getWritableDatabase();
				ArrayList<String[]> collections = new ArrayList<String[]>();
				String sqlUQuery;
				
				sqlUQuery = params[0];
				
				Cursor cursor = db.rawQuery(sqlUQuery, null);
				
				if (cursor.moveToFirst())
				{				
					do {
						ArrayList<String> results = new ArrayList<String>();
						
						for (int i = 0; i < cursor.getColumnCount(); i++)
						{
							try
							{
								if (i == 0)
								{
									results.add(String.format("%08d", Integer.valueOf(cursor.getString(i))));
								}
								else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING)
								{
									if (cursor.getColumnName(i).equals("applied_amount"))
									{
										String[] separated = cursor.getString(i).split(",");
										double sum = 0.0;
										for (int o = 0; o < separated.length; o++)
										{
											sum += Double.valueOf(separated[o]);
										}
										
										results.add(new DecimalFormat("#,###,###.##").format(sum));
									}
									else
									{
										results.add(cursor.getString(i));
									}
								}
								else
								{
									DecimalFormat formatter = new DecimalFormat("#,###,###.##");
									
									results.add(formatter.format(Double.parseDouble(cursor.getString(i))));
								}
							}
							catch (NumberFormatException ex)
							{
								results.add(cursor.getString(i));
							}
						}
						
						
						collections.add(results.toArray(new String[results.size()]));
						
					} while(cursor.moveToNext());
				}
				
				return collections;
			}
		}		
		
		// end
		
	private int qIntValue(Cursor c, String s)
	{
		return c.getInt(c.getColumnIndex(s));
	}
	
	private String qStringValue(Cursor c, String s)
	{
		return c.getString(c.getColumnIndex(s));
	}
	
	private double qDoubleValue(Cursor c, String s)
	{
		return c.getDouble(c.getColumnIndex(s));
	}
}
