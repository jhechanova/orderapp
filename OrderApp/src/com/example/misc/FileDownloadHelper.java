package com.example.misc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.helper.FunctionHelper;

public class FileDownloadHelper {
	
	private String downloadFile = "";
	private String downloadFileUri = "";
	
    private int serverResponseCode = 0;       
     
    /**********  File Path *************/
    private String downloadFilePath = Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/";
    private String downloadServerUri = "http://" + FunctionHelper.ipaddr + "/img/";
     
    public FileDownloadHelper() {
    }
             
    public void downloadFile(String sourceFileUri) {
		downloadFile = downloadFilePath + sourceFileUri;
		downloadFileUri = downloadServerUri + sourceFileUri;
		  
		new DownloadFiles().execute();
    }
    
    class DownloadFiles extends AsyncTask<Void, Void, Void> {
    	@Override
		protected void onPreExecute() {}

		protected Void doInBackground(Void... args) {
			
			try{
		        URL url = new URL(downloadFileUri); //you can write here any link

		        File dloadFile =  new File(downloadFile);
		        
		        if (dloadFile.exists ()) dloadFile.delete (); 

		        /* Open a connection to that URL. */
		        URLConnection ucon = url.openConnection();
		        InputStream inputStream = null;
		        HttpURLConnection httpConn = (HttpURLConnection)ucon;
		        httpConn.setRequestMethod("GET");
		        httpConn.connect();

		        if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
		        	inputStream = httpConn.getInputStream();
		        }
		        
	            FileOutputStream fos = new FileOutputStream(dloadFile);
	            int size = 1024*1024;
		        byte[] buf = new byte[size];
		        int byteRead;
		        while (((byteRead = inputStream.read(buf)) != -1)) {
		        	fos.write(buf, 0, byteRead);
		        }

		        fos.close();

		    }
			catch(IOException io)
		    {
				io.printStackTrace();
		    }
		    catch(Exception e)
		    {   
		        e.printStackTrace();
		    }
			 
			return null;
		}

		protected void onPostExecute() {}
    }
}
