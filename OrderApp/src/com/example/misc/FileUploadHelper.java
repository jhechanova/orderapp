package com.example.misc;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.helper.FunctionHelper;

public class FileUploadHelper {
	
	private String uploadFile = "";
	private String fileName = "";
	
    private int serverResponseCode = 0;       
     
    /**********  File Path *************/
    private String uploadFilePath = Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/";
    private String upLoadServerUri = FunctionHelper.urlnologin;
     
    public FileUploadHelper() {
    }
             
    public void uploadFile(String sourceFileUri) {
		fileName = sourceFileUri;
		uploadFile = uploadFilePath + sourceFileUri;
		  
		new UploadFiles().execute();
    }
    
    class UploadFiles extends AsyncTask<Void, Void, Void> {
    	@Override
		protected void onPreExecute() {}

		protected Void doInBackground(Void... args) {
			
			HttpURLConnection conn = null;
			DataOutputStream dos = null;  
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";
			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1 * 1024 * 1024; 
			File sourceFile = new File(uploadFile); 
			   
 
       try { 
            
             // open a URL connection to the Servlet
		   FileInputStream fileInputStream = new FileInputStream(sourceFile);
		   URL url = new URL(upLoadServerUri);
		    
		   // Open a HTTP  connection to  the URL
		   conn = (HttpURLConnection) url.openConnection(); 
		   conn.setDoInput(true); // Allow Inputs
		   conn.setDoOutput(true); // Allow Outputs
		   conn.setUseCaches(false); // Don't use a Cached Copy
		   conn.setRequestMethod("POST");
		   conn.setRequestProperty("Connection", "Keep-Alive");
		   conn.setRequestProperty("ENCTYPE", "multipart/form-data");
		   conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);   
            
           dos = new DataOutputStream(conn.getOutputStream());
  
           dos.writeBytes(twoHyphens + boundary + lineEnd); 
           dos.writeBytes("Content-Disposition: form-data; name=\"uploadfile\";filename=\"" + fileName + "\"" + lineEnd);
           dos.writeBytes(lineEnd);
  
           // create a buffer of  maximum size
           bytesAvailable = fileInputStream.available(); 
           bufferSize = Math.min(bytesAvailable, maxBufferSize);
           buffer = new byte[bufferSize];
           // read file and write it into form...
           bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
              
           while (bytesRead > 0) {
                
             dos.write(buffer, 0, bufferSize);
             bytesAvailable = fileInputStream.available();
             bufferSize = Math.min(bytesAvailable, maxBufferSize);
             bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
              
            }
  
           // send multipart form data necesssary after file data...
           dos.writeBytes(lineEnd);
           dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
  
           // Responses from the server (code and message)
		   serverResponseCode = conn.getResponseCode();
		   String serverResponseMessage = conn.getResponseMessage();
		     
		   Log.i("uploadFile", "HTTP Response is : "
		   + serverResponseMessage + ": " + serverResponseCode);
		        
		   //close the streams //
	       fileInputStream.close();
	       dos.flush();
	       dos.close();
         
		  } catch (MalformedURLException ex) {
		      ex.printStackTrace();
		  } catch (Exception e) {
			  e.printStackTrace();  
		  }
       	return null;
			       

		}

		protected void onPostExecute() {}
    }
}
