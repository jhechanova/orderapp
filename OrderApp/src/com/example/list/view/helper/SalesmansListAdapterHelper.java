package com.example.list.view.helper;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.classobj.CustomerProfile;
import com.example.classobj.SalesmanProfile;
import com.example.orderapp.R;

public class SalesmansListAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<SalesmanProfile> data;

    public SalesmansListAdapterHelper (FragmentActivity a, ArrayList<SalesmanProfile> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	SalesmansViewHolder _salesmanViewHolder;
    	
    	if (convertView == null)
    	{
    		_salesmanViewHolder = new SalesmansViewHolder();
    		convertView = inflater.inflate(R.layout.salesman_list_view, null);

    		_salesmanViewHolder.salesmanName = (TextView)convertView.findViewById(R.id.salesman_name);
    		
    		convertView.setTag(_salesmanViewHolder);
    	}
    	else
    	{
    		_salesmanViewHolder = (SalesmansViewHolder) convertView.getTag();
    	}
        
    	final SalesmanProfile _salesmanProfile = data.get(position);
        
    	_salesmanViewHolder.salesmanName.setText(_salesmanProfile.getSalesmanName());
        
        return convertView;
    }
    
    public class SalesmansViewHolder
    {
    	TextView salesmanName;
    }
}