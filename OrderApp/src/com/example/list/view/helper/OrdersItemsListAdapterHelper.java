package com.example.list.view.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.classobj.ItemDetails;
import com.example.classobj.ItemProfile;
import com.example.orderapp.OrderFormActivity;
import com.example.orderapp.R;

public class OrdersItemsListAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<ItemProfile> data;
    private ArrayList<ItemDetails> qItem;
    private boolean isListing;

    public OrdersItemsListAdapterHelper (FragmentActivity a, ArrayList<ItemProfile> d, ArrayList<ItemDetails> q, boolean l){
        activity = a;
        data = d;
        qItem = q;
        isListing = l;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	ItemsViewHolder _itemViewHolder;
    	
    	if (convertView == null)
    	{
    		_itemViewHolder = new ItemsViewHolder();
    		convertView = inflater.inflate(R.layout.orders_items_list, null);

    		_itemViewHolder.description = (TextView)convertView.findViewById(R.id.oidescription);
    		_itemViewHolder.price = (TextView)convertView.findViewById(R.id.oiprice);
    		_itemViewHolder.qty = (TextView)convertView.findViewById(R.id.oiqty);
    		_itemViewHolder.checkStatus = (CheckBox)convertView.findViewById(R.id.oistatus);
    		_itemViewHolder.totalprice = (TextView)convertView.findViewById(R.id.oitotalprice);
    		_itemViewHolder.discount = (TextView)convertView.findViewById(R.id.oidiscount);
    		_itemViewHolder.qoh = (TextView)convertView.findViewById(R.id.oiqoh);
    		
    		convertView.setTag(_itemViewHolder);
    	}
    	else
    	{
    		_itemViewHolder = (ItemsViewHolder) convertView.getTag();
    	}
    	
        final ItemProfile _itemProfile = data.get(position);
        
        _itemViewHolder.description.setText(_itemProfile.getDescription());
        _itemViewHolder.price.setText(_itemProfile.getPrice().toString());
        
        if (qItem != null && qItem.size() > 0)
        {
        	if (OrderFormActivity.existFromItemDetails(_itemProfile.getItemId()))
        	{
	        	final ItemDetails _itemDetail = qItem.get(OrderFormActivity.getIndexFromItemDetails(_itemProfile.getItemId()));
        		double totalprice = (_itemProfile.getPrice()-_itemDetail.getItemDesc()) * _itemDetail.getItemQty();
        	
        		DecimalFormat twoDForm =new DecimalFormat("#.##");
        		totalprice = Double.valueOf(twoDForm.format(totalprice));
        		
        		_itemViewHolder.discount.setText(String.valueOf(_itemDetail.getItemDesc()));
	        	_itemViewHolder.qty.setText(String.valueOf(_itemDetail.getItemQty()));
	        	_itemViewHolder.totalprice.setText(String.valueOf(totalprice));
	        	_itemViewHolder.qoh.setText(String.valueOf(_itemProfile.getQty()));
	        	
	        	if (isListing)
	        		_itemViewHolder.checkStatus.setChecked(false);
	        	else
	        		_itemViewHolder.checkStatus.setChecked(_itemDetail.getToBeAdded());
        	}
        	else
        	{
        		_itemViewHolder.checkStatus.setChecked(false);
            	_itemViewHolder.qty.setText("0.0");
            	_itemViewHolder.totalprice.setText("0.0");
            	_itemViewHolder.discount.setText("0.0");
            	_itemViewHolder.qoh.setText("0.0");
        	}
        }
        else
        {
            _itemViewHolder.checkStatus.setChecked(false);
        	_itemViewHolder.qty.setText("0.0");
        	_itemViewHolder.totalprice.setText("0.0");
        	_itemViewHolder.discount.setText("0.0");
        	_itemViewHolder.qoh.setText("0.0");
        }
        
        return convertView;
    }
    
    public class ItemsViewHolder
    {
    	TextView description;
    	TextView price;
    	CheckBox checkStatus;
    	TextView qty;
    	TextView totalprice;
    	TextView discount;
    	TextView qoh;
    	
    	public CheckBox getCheckBox()
    	{
    		return checkStatus;
    	}
    	
    	public TextView getQtyFld()
    	{
    		return qty;
    	}
    	
    	public TextView getTotalPrice()
    	{
    		return totalprice;
    	}
    	
    	public TextView getDiscountFld()
    	{
    		return discount;
    	}
    }
}