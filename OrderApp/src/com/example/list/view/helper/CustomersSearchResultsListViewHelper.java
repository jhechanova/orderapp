package com.example.list.view.helper;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.classobj.CustomerProfile;
import com.example.helper.ManageBitmapHelper;
import com.example.orderapp.R;

public class CustomersSearchResultsListViewHelper extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<CustomerProfile> data;

    public CustomersSearchResultsListViewHelper (Activity a, ArrayList<CustomerProfile> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	CustomersViewHolder _customerViewHolder;
    	
    	if (convertView == null)
    	{
    		_customerViewHolder = new CustomersViewHolder();
    		convertView = inflater.inflate(R.layout.customers_list_view, null);

    		_customerViewHolder.custname = (TextView)convertView.findViewById(R.id.custname);
    		_customerViewHolder.custcontact = (TextView)convertView.findViewById(R.id.custcontact);
    		_customerViewHolder.custaddress = (TextView)convertView.findViewById(R.id.custaddress);
    		_customerViewHolder.custimage = (ImageView)convertView.findViewById(R.id.custimage);
    		
    		convertView.setTag(_customerViewHolder);
    	}
    	else
    	{
    		_customerViewHolder = (CustomersViewHolder) convertView.getTag();
    	}
    	
        final CustomerProfile _customerProfile = data.get(position);
        _customerViewHolder.custname.setText(_customerProfile.getCustomerName());
        _customerViewHolder.custcontact.setText(_customerProfile.getContactNo());
        _customerViewHolder.custaddress.setText(_customerProfile.getAddress());
        
        if(!_customerProfile.getCompImagePath().equals(""))
        {
	        File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/" + _customerProfile.getCompImagePath());

			if (imgFile.exists())
			{
				//imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());				
				//messageListImage.setImageBitmap(imgBitmap);
				new ManageBitmapHelper(convertView.getContext()).loadBitmap((int) _customerProfile.getCustomerId(), 
						_customerViewHolder.custimage, imgFile.getAbsolutePath());
			}
			else
			{
				_customerViewHolder.custimage.setImageBitmap(null);
			}
        }
        else
		{
			_customerViewHolder.custimage.setImageBitmap(null);
		}
        
        return convertView;
    }
    
    public class CustomersViewHolder
    {
    	TextView custname;
    	TextView custcontact;
    	TextView custaddress;
    	ImageView custimage;
    }
}