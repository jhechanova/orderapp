package com.example.list.view.helper;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.classobj.CustomerProfile;
import com.example.orderapp.R;

public class OrdersCustomersListAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<CustomerProfile> data;
    private boolean isListing;

    public OrdersCustomersListAdapterHelper (FragmentActivity a, ArrayList<CustomerProfile> d, boolean l){
        activity = a;
        data = d;
        isListing = l;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	CustomersViewHolder _customerViewHolder;
    	
    	if (convertView == null)
    	{
    		_customerViewHolder = new CustomersViewHolder();
    		convertView = inflater.inflate(R.layout.orders_customers_list, null);

    		_customerViewHolder.customerName = (TextView)convertView.findViewById(R.id.occustomername);
    		
    		convertView.setTag(_customerViewHolder);
    	}
    	else
    	{
    		_customerViewHolder = (CustomersViewHolder) convertView.getTag();
    	}
        
    	final CustomerProfile _customerProfile = data.get(position);
        
    	_customerViewHolder.customerName.setText(_customerProfile.getCustomerName());
        
        return convertView;
    }
    
    public class CustomersViewHolder
    {
    	TextView customerName;
    }
}