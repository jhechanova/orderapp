package com.example.list.view.helper;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.classobj.CustomerProfile;
import com.example.classobj.MenuDetails;
import com.example.classobj.SalesmanProfile;
import com.example.orderapp.R;

public class MenuListAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<MenuDetails> data;

    public MenuListAdapterHelper (FragmentActivity a, ArrayList<MenuDetails> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	MenuViewHolder _menunViewHolder;
    	
    	if (convertView == null)
    	{
    		_menunViewHolder = new MenuViewHolder();
    		convertView = inflater.inflate(R.layout.menu_list_view, null);

    		_menunViewHolder.menuLabel = (TextView)convertView.findViewById(R.id.menu_label);
    		
    		convertView.setTag(_menunViewHolder);
    	}
    	else
    	{
    		_menunViewHolder = (MenuViewHolder) convertView.getTag();
    	}
        
    	final MenuDetails _menuDetail = data.get(position);
        
    	_menunViewHolder.menuLabel.setText(_menuDetail.getMenuLabel());
        
        return convertView;
    }
    
    public class MenuViewHolder
    {
    	TextView menuLabel;
    }
}