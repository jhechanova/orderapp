package com.example.list.view.helper;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.classobj.ItemProfile;
import com.example.orderapp.R;

public class ItemsListViewHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<ItemProfile> data;

    public ItemsListViewHelper (FragmentActivity a, ArrayList<ItemProfile> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	ItemsViewHolder _itemViewHolder;
    	
    	if (convertView == null)
    	{
    		_itemViewHolder = new ItemsViewHolder();
    		convertView = inflater.inflate(R.layout.items_list_view, null);

    		_itemViewHolder.sku = (TextView)convertView.findViewById(R.id.sku);
    		_itemViewHolder.description = (TextView)convertView.findViewById(R.id.description);
    		_itemViewHolder.price = (TextView)convertView.findViewById(R.id.price);
    		_itemViewHolder.qty = (TextView)convertView.findViewById(R.id.qty);
    		
    		convertView.setTag(_itemViewHolder);
    	}
    	else
    	{
    		_itemViewHolder = (ItemsViewHolder) convertView.getTag();
    	}
    	
        final ItemProfile _itemProfile = data.get(position);
        _itemViewHolder.sku.setText(_itemProfile.getSKU());
        _itemViewHolder.description.setText(_itemProfile.getDescription());
        _itemViewHolder.price.setText(_itemProfile.getPrice().toString());
        _itemViewHolder.qty.setText(_itemProfile.getQty().toString());
        
        return convertView;
    }
    
    public class ItemsViewHolder
    {
    	TextView sku;
    	TextView description;
    	TextView price;
    	TextView qty;
    }
}