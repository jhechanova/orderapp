package com.example.list.view.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.classobj.CustomerProfile;
import com.example.classobj.OrderHeader;
import com.example.orderapp.MainActivity;
import com.example.orderapp.R;

public class OrdersListAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<OrderHeader> data;

    public OrdersListAdapterHelper (FragmentActivity a, ArrayList<OrderHeader> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	OrderViewHolder _orderViewHolder;
    	
    	if (convertView == null)
    	{
    		_orderViewHolder = new OrderViewHolder();
    		convertView = inflater.inflate(R.layout.orders_list_view, null);

    		_orderViewHolder.order_date = (TextView)convertView.findViewById(R.id.order_date);
    		_orderViewHolder.order_total_price = (TextView)convertView.findViewById(R.id.order_total_price);
    		_orderViewHolder.order_no = (TextView) convertView.findViewById(R.id.order_no);
    		_orderViewHolder.customer_name = (TextView) convertView.findViewById(R.id.order_customer_name);
    		_orderViewHolder.status = (TextView) convertView.findViewById(R.id.order_status);
    		
    		convertView.setTag(_orderViewHolder);
    	}
    	else
    	{
    		_orderViewHolder = (OrderViewHolder) convertView.getTag();
    	}
    	
        final OrderHeader _orderHeader = data.get(position);
        _orderViewHolder.order_date.setText(_orderHeader.getOrderDate());
        _orderViewHolder.order_no.setText(String.format("%08d", _orderHeader.getOrderNo()));
        CustomerProfile _customerProfile = MainActivity._deskpadDbHelper.getCustomer(_orderHeader.getCustomerId());
        _orderViewHolder.customer_name.setText(_customerProfile.getCustomerName());
        
        DecimalFormat formatter = new DecimalFormat("#,###,###.##");
        
        _orderViewHolder.order_total_price.setText(formatter.format(_orderHeader.getOrderTotalPrice()));
        _orderViewHolder.status.setText(_orderHeader.getStatus());

        return convertView;
    }
    
    public class OrderViewHolder
    {
    	TextView order_date;
    	TextView order_total_price;
    	TextView customer_name;
    	TextView order_no;
    	TextView status;
    }
}