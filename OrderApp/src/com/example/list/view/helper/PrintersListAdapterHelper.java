package com.example.list.view.helper;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.classobj.SalesmanProfile;
import com.example.orderapp.R;

public class PrintersListAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<BluetoothDevice> data;

    public PrintersListAdapterHelper (FragmentActivity a, ArrayList<BluetoothDevice> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	PrintersViewHolder _printersViewHolder;
    	
    	if (convertView == null)
    	{
    		_printersViewHolder = new PrintersViewHolder();
    		convertView = inflater.inflate(R.layout.printer_list_view, null);

    		_printersViewHolder.printerName = (TextView)convertView.findViewById(R.id.printer_name);
    		
    		convertView.setTag(_printersViewHolder);
    	}
    	else
    	{
    		_printersViewHolder = (PrintersViewHolder) convertView.getTag();
    	}
        
    	final BluetoothDevice _device = data.get(position);
        
    	_printersViewHolder.printerName.setText(_device.getName() + " ( " + _device.getAddress() + " )");
        
        return convertView;
    }
    
    public class PrintersViewHolder
    {
    	TextView printerName;
    }
}