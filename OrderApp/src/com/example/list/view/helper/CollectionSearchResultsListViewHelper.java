package com.example.list.view.helper;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.classobj.CustomerCollection;
import com.example.classobj.CustomerProfile;
import com.example.helper.ManageBitmapHelper;
import com.example.list.view.helper.CollectionsListAdapterHelper.CollectionViewHolder;
import com.example.orderapp.R;

public class CollectionSearchResultsListViewHelper extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<CustomerCollection> data;

    public CollectionSearchResultsListViewHelper (Activity a, ArrayList<CustomerCollection> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	CollectionViewHolder _collectionViewHolder;
    	
    	if (convertView == null)
    	{
    		_collectionViewHolder = new CollectionViewHolder();
    		convertView = inflater.inflate(R.layout.collections_list_view, null);

    		_collectionViewHolder.collection_total = (TextView)convertView.findViewById(R.id.collection_total);
    		_collectionViewHolder.customer_name = (TextView) convertView.findViewById(R.id.collection_customer_name);
    		_collectionViewHolder.status = (TextView) convertView.findViewById(R.id.collection_status);
    		_collectionViewHolder.collected_total = (TextView)convertView.findViewById(R.id.collected_total);
    		_collectionViewHolder.balance_total=(TextView)convertView.findViewById(R.id.balance_total);
    		
    		convertView.setTag(_collectionViewHolder);
    	}
    	else
    	{
    		_collectionViewHolder = (CollectionViewHolder) convertView.getTag();
    	}
    	
    	final CustomerCollection _collection = data.get(position);
        _collectionViewHolder.customer_name.setText(_collection.getCustomerName());
        
        DecimalFormat formatter = new DecimalFormat("#,###,###.##");
        
        double appliedAmnt = 0.0; 
		for (int o=0; o<_collection.getAppliedAmount().split(",").length; o++)
			appliedAmnt += Double.valueOf(_collection.getAppliedAmount().split(",")[o]);
		
        double total = _collection.getTotalCollection() - appliedAmnt;
        
        _collectionViewHolder.collection_total.setText(formatter.format(_collection.getTotalCollection()));
        _collectionViewHolder.collected_total.setText(formatter.format(appliedAmnt));
        _collectionViewHolder.balance_total.setText(formatter.format(total));
        
        _collectionViewHolder.status.setText(_collection.getStatus());

        return convertView;
    }
    
    public class CollectionViewHolder
    {
    	TextView collection_total;
    	TextView collected_total;
    	TextView balance_total;
    	TextView customer_name;
    	TextView status;
    }
}