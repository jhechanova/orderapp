package com.example.list.view.helper;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.classobj.CollectionDetails;
import com.example.classobj.CustomerCollection;
import com.example.misc.AlertDialogManager;
import com.example.orderapp.CollectionFormActivity;
import com.example.orderapp.R;

public class CollectionsDetailsAdapterHelper extends BaseAdapter {

    private FragmentActivity activity;
    private LayoutInflater inflater;
    private ArrayList<CollectionDetails> data;
    
    private CollectionDetailViewHolder _collectionDetailViewHolder;
    private Context contxt;
    private CustomerCollection collection;

    public CollectionsDetailsAdapterHelper (FragmentActivity a, ArrayList<CollectionDetails> d, Context c, CustomerCollection cc){
        activity = a;
        data = d;
        contxt = c;
        collection = cc;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	final CollectionDetails _collectionDetails = data.get(position);
    	
    	if (convertView == null)
    	{
    		_collectionDetailViewHolder = new CollectionDetailViewHolder();
    		convertView = inflater.inflate(R.layout.collections_list, null);

    		// collections detail fields
    		_collectionDetailViewHolder.dr_number = (TextView) convertView.findViewById(R.id.dr_number);
    		_collectionDetailViewHolder.dr_date = (TextView) convertView.findViewById(R.id.dr_date);
    		_collectionDetailViewHolder.dr_amount = (TextView) convertView.findViewById(R.id.dr_amount);
    		_collectionDetailViewHolder.applied_amount = (EditText) convertView.findViewById(R.id.applied_amount);
    		_collectionDetailViewHolder.balance = (TextView) convertView.findViewById(R.id.balance);
            
            _collectionDetailViewHolder.applied_amount.addTextChangedListener(new CustomTextWatcher(_collectionDetails, 
            		_collectionDetailViewHolder.applied_amount, _collectionDetailViewHolder.dr_amount, _collectionDetailViewHolder.balance));
    		
    		convertView.setTag(_collectionDetailViewHolder);
    		_collectionDetailViewHolder.applied_amount.setTag(_collectionDetails);
    	}
    	else
    	{
    		_collectionDetailViewHolder = (CollectionDetailViewHolder) convertView.getTag();
    	}
    	
    	DecimalFormat formatter = new DecimalFormat("#,###,###.##");
    	
    	_collectionDetailViewHolder.dr_number.setText(String.format("%08d", _collectionDetails.getDRNumber()));
    	_collectionDetailViewHolder.dr_date.setText(_collectionDetails.getDRDate());
    	_collectionDetailViewHolder.dr_amount.setText(formatter.format(_collectionDetails.getDRAmount()));
    	_collectionDetailViewHolder.applied_amount.setText(String.valueOf(_collectionDetails.getAppliedAmount()));
    	_collectionDetailViewHolder.balance.setText(formatter.format(_collectionDetails.getBalance()));
    	
        return convertView;
    }
    
    private class CustomTextWatcher implements TextWatcher
    {
    	private CollectionDetails details; 
        private EditText appliedAmount;
        private TextView drAmount;
        private TextView balance;

        public CustomTextWatcher(CollectionDetails details, EditText appliedAmount, TextView drAmount, TextView balance)
        {
            this.details = details;
            this.appliedAmount = appliedAmount;
            this.drAmount = drAmount;
            this.balance = balance;
        }
        
        @Override
        public void afterTextChanged(Editable s){
        	
        	NumberFormat format = NumberFormat.getInstance(Locale.US);
        	Number number = null;
			try {
				number = format.parse(this.drAmount.getText().toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        	double applied = 0.0;
        	double dr_amount = number.doubleValue();
        	
        	if (this.appliedAmount.getText().toString().length() > 0)
        	{
        		applied = Double.valueOf(this.appliedAmount.getText().toString());
        	}
        	
        	if (applied > 0.0 && applied > dr_amount)
        	{
        		AlertDialogManager adm = new AlertDialogManager(contxt);
				adm.fail("Information", 
						"The applied amount inputed is greater than the dr amount ( DR Amount : " + dr_amount + " ) . Applied amount will change to zero.");
				adm.show();
				
				this.appliedAmount.setText("");
				applied = 0.0;
        	}
        	
        	this.details.setAppliedAmount(applied);
        	this.balance.setText(String.valueOf(dr_amount-applied));
        	
        	// added sept. 29, 2014
        	Log.e("test", String.valueOf(collection.getAmount()));
        	
        	double totalAppliedAmount = 0.0;
    		
    		for (CollectionDetails cdet : data)
    		{						
    			totalAppliedAmount += Double.valueOf(cdet.getAppliedAmount());
    		}

    		if (totalAppliedAmount > collection.getAmount())
    		{
    			AlertDialogManager adm = new AlertDialogManager(contxt);
				adm.fail("Information", 
						"The total applied amount is greater than the amount received ( Amount : " + collection.getAmount() + " ) . Applied amount will change to zero.");
				adm.show();
				
				this.appliedAmount.setText("");
				applied = 0.0;
    		}
    		
    		this.details.setAppliedAmount(applied);
        	this.balance.setText(String.valueOf(dr_amount-applied));
        	
        	// end
        	
        	CollectionFormActivity.UpdateTotalAmounts();
        }

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub
			
		}
    }
    
    public class CollectionDetailViewHolder
	{
		TextView dr_number;
		TextView dr_date;
		TextView dr_amount;
		EditText applied_amount;
		TextView balance;
	}
}