package com.example.orderapp;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.classobj.CustomerCollection;
import com.example.classobj.OrderHeader;
import com.example.list.view.helper.CollectionsListAdapterHelper;
import com.example.list.view.helper.OrdersListAdapterHelper;

public class CollectionsListActivity extends Fragment implements ListView.OnItemClickListener {

	private ArrayList<CustomerCollection> collectionsList;
	
	private CollectionsListAdapterHelper _collectionsAdapterHelper;
	private ListView _listView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_collections_list, container, false);
        _listView = (ListView)rootView.findViewById(R.id.collections_list_view);
		
        collectionsList = new ArrayList<CustomerCollection>();
		
        //collectionsList = MainActivity._deskpadDbHelper.getCollections();
        collectionsList = MainActivity._deskpadDbHelper.getSelectedCollections("b.route_days = " + (int)Math.pow(2, Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1));
		
		if (collectionsList != null)
		{
			_collectionsAdapterHelper = new CollectionsListAdapterHelper(getActivity(), collectionsList);
			
			_listView.setAdapter(_collectionsAdapterHelper);
			
			_listView.setOnItemClickListener(this);
		}
		
        return rootView;
	}

	@Override
	public void onResume() {
		if(_collectionsAdapterHelper != null){
			collectionsList = new ArrayList<CustomerCollection>();
			//collectionsList = MainActivity._deskpadDbHelper.getCollections();
			collectionsList = MainActivity._deskpadDbHelper.getSelectedCollections("b.route_days = " + (int)Math.pow(2, Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1));

			_collectionsAdapterHelper = new CollectionsListAdapterHelper(getActivity(), collectionsList);
			
			_listView.setAdapter(_collectionsAdapterHelper);
		}
		super.onResume();
	}
	
	@Override
    public void onItemClick(AdapterView<?> ad, View v, int position, long id) {
        // TODO Auto-generated method stub
		CustomerCollection _collection = (CustomerCollection)collectionsList.get(position);
		
		Intent collectionViewIntent = new Intent(getActivity(), CollectionFormActivity.class);
		collectionViewIntent.putExtra("collection_no", _collection.getCollectionNo());
				
		startActivity(collectionViewIntent);
    }
}
