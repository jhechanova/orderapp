package com.example.orderapp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.classobj.CustomerProfile;
import com.example.helper.FunctionHelper;
import com.example.misc.AlertDialogManager;

public class CustomerFormActivity extends FragmentActivity {

	// buttons
	private Button customer_save_btn;
	private Button customer_update_btn;
	private Button customer_geo_btn;
	// end
	
	private static final int MEDIA_TYPE_IMAGE = 1;
	private static final int CAPTURE_PROF_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int CAPTURE_COMP_IMAGE_ACTIVITY_REQUEST_CODE = 200;
	
	private long customerId;
	
	private CustomerProfile _customerProfile;
	private CustomersViewHolder _customersViewHolder;
	
	private Uri fileUri;
	private static String profFileName = "";
	private static String compFileName = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customers_form);
		
		Intent orderViewIntent = getIntent();
		customerId = orderViewIntent.getLongExtra("customer_id", 0);
		
		customer_save_btn = (Button) findViewById(R.id.customer_save_btn);
		customer_update_btn = (Button) findViewById(R.id.customer_update_btn);
		customer_geo_btn = (Button) findViewById(R.id.customer_geo_btn);
		
		// orders header fields
		_customersViewHolder = new CustomersViewHolder();
		_customersViewHolder.customer_id_field = (TextView) findViewById(R.id.customer_id_field);
		_customersViewHolder.customer_name_field = (EditText) findViewById(R.id.customer_name_field);
		_customersViewHolder.customer_address_field = (EditText) findViewById(R.id.customer_address_field);
		_customersViewHolder.customer_contact_no_field = (EditText) findViewById(R.id.customer_contact_no_field);
		_customersViewHolder.customer_geo_latitude_field = (EditText) findViewById(R.id.customer_geo_latitude_field);
		_customersViewHolder.customer_geo_longitude_field = (EditText) findViewById(R.id.customer_geo_longitude_field);
		_customersViewHolder.customer_tin_field = (EditText) findViewById(R.id.customer_tin_field);
		
		_customersViewHolder.customer_prof_image = (ImageView) findViewById(R.id.customer_prof_image);
		_customersViewHolder.customer_comp_image = (ImageView) findViewById(R.id.customer_comp_image);
		
		_customersViewHolder.customer_term_field = (Spinner) findViewById(R.id.customer_term_field);
		_customersViewHolder.customer_tax_type_field = (Spinner) findViewById(R.id.customer_tax_type_field);
		
		ArrayAdapter<String> termArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetPaymentTerms() ); 
		termArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_customersViewHolder.customer_term_field.setAdapter(termArrAdapter);
		
		ArrayAdapter<String> taxTypeArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetTaxTypes() ); 
		taxTypeArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_customersViewHolder.customer_tax_type_field.setAdapter(taxTypeArrAdapter);
		
		customer_save_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SaveNewCustomer("new");
			}
		});
		
		customer_update_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SaveNewCustomer("new");
			}
		});
		
		_customersViewHolder.customer_prof_image.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				captureImage(CAPTURE_PROF_IMAGE_ACTIVITY_REQUEST_CODE);
			}
		});
		
		_customersViewHolder.customer_comp_image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				captureImage(CAPTURE_COMP_IMAGE_ACTIVITY_REQUEST_CODE);
			}
		});
		
		customer_geo_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				_customersViewHolder.customer_geo_latitude_field.setText(String.valueOf(MainActivity._latitude));
				_customersViewHolder.customer_geo_longitude_field.setText(String.valueOf(MainActivity._longitude));
				
				Toast.makeText(CustomerFormActivity.this, "lat = " + MainActivity._latitude
						+ "; long = " + MainActivity._longitude, Toast.LENGTH_SHORT).show();
			}
		});
		
		if (customerId > 0)
		{
			customer_save_btn.setVisibility(View.INVISIBLE);
			
			_customerProfile = MainActivity._deskpadDbHelper.getCustomer(customerId);
			
			if (_customerProfile != null)
			{
				_customersViewHolder.customer_id_field.setText(String.format("%08d", _customerProfile.getCustomerId()));
				_customersViewHolder.customer_name_field.setText(_customerProfile.getCustomerName());
				_customersViewHolder.customer_address_field.setText(_customerProfile.getAddress());
				_customersViewHolder.customer_contact_no_field.setText(_customerProfile.getContactNo());
				_customersViewHolder.customer_geo_latitude_field.setText(String.valueOf(_customerProfile.getGeoLatitude()));
				_customersViewHolder.customer_geo_longitude_field.setText(String.valueOf(_customerProfile.getGeoLongitude()));
				
				_customersViewHolder.customer_tin_field.setText(_customerProfile.getTin());
				
				_customersViewHolder.customer_term_field.setSelection(FunctionHelper.GetPaymentTermsIndx(_customerProfile.getTerm()));
				_customersViewHolder.customer_tax_type_field.setSelection(FunctionHelper.GetTaxTypesIndx(_customerProfile.getTaxType()));
				
				// modified and added
				if (!_customerProfile.getProfImagePath().equals(""))
				{
					File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/" + _customerProfile.getProfImagePath());
				
					if (imgFile.exists())
					{
						Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
						
						_customersViewHolder.customer_prof_image.setImageBitmap(imgBitmap);
					}
				}
				
				if (!_customerProfile.getCompImagePath().equals(""))
				{
					File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/" + _customerProfile.getCompImagePath());
					
					if (imgFile.exists())
					{
						Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
						
						_customersViewHolder.customer_comp_image.setImageBitmap(imgBitmap);
					}
				}
				
				// end
			}
		}
		else
		{
			customer_update_btn.setVisibility(View.INVISIBLE);
			
			_customersViewHolder.customer_id_field.setText("New Customer");
		}
	}

	private void captureImage(int rqCode)
	{
		// create Intent to take a picture and return control to the calling application
	    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	
	    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE, rqCode); // create a file to save the image
	    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
	    intent.putExtra("file", fileUri);

    	// start the image capture Intent
    	startActivityForResult(intent, rqCode);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{		
		if (requestCode == CAPTURE_PROF_IMAGE_ACTIVITY_REQUEST_CODE || 
				requestCode == CAPTURE_COMP_IMAGE_ACTIVITY_REQUEST_CODE)
		{
			if (resultCode == RESULT_OK)
			{
				Log.e("orderapp", fileUri.getPath());
				
				File imgFile = new File(fileUri.getPath());
				
				if (imgFile.exists())
				{
					Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
					
					switch(requestCode)
					{
						case CAPTURE_PROF_IMAGE_ACTIVITY_REQUEST_CODE:
							_customersViewHolder.customer_prof_image.setImageBitmap(imgBitmap);
							break;
						case CAPTURE_COMP_IMAGE_ACTIVITY_REQUEST_CODE:
							_customersViewHolder.customer_comp_image.setImageBitmap(imgBitmap);
							break;
					}
				}
				else
				{
					Toast.makeText(CustomerFormActivity.this, "Error", Toast.LENGTH_LONG).show();
				}
			}
			else if (resultCode == RESULT_CANCELED)
			{

			}
			else
			{

			}
		}
	}
	
	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type, int rqCode){
	      return Uri.fromFile(getOutputMediaFile(type, rqCode));
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type, int rqCode){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.
		
		if (Environment.getExternalStorageState().compareTo("mounted") != 0)
			return null;    
	    
	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile = null;
	    if (type == MEDIA_TYPE_IMAGE){
	        switch(rqCode)
	        {
		        case CAPTURE_PROF_IMAGE_ACTIVITY_REQUEST_CODE:
		        	profFileName = "PROF_IMG_" + timeStamp + ".jpg";
		        	mediaFile = new File(Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/" + profFileName);
		        	break;
		        case CAPTURE_COMP_IMAGE_ACTIVITY_REQUEST_CODE:
		        	compFileName = "COMP_IMG_" + timeStamp + ".jpg";
		        	mediaFile = new File(Environment.getExternalStorageDirectory().getPath() + "/OrderApp/images/" + compFileName);
		        	break;
	        }
	    } else {
	        return null;
	    }

	    return mediaFile;
	}
	
	private void SaveNewCustomer(String status)
	{	
		if (_customersViewHolder.customer_name_field.getText().length() == 0)
		{
			AlertDialogManager adm = new AlertDialogManager(CustomerFormActivity.this);
			adm.fail("Error", 
					"Customer name cannot be empty.");
			adm.show();

			return;
		}
		
		double latitude = (_customersViewHolder.customer_geo_latitude_field.getText().length() == 0)
				? 0.0 : Double.valueOf(_customersViewHolder.customer_geo_latitude_field.getText().toString());
		double longitude = (_customersViewHolder.customer_geo_longitude_field.getText().length() == 0)
				? 0.0 : Double.valueOf(_customersViewHolder.customer_geo_longitude_field.getText().toString());
		
		CustomerProfile _customerProfile = new CustomerProfile(customerId, _customersViewHolder.customer_name_field.getText().toString(), 
				_customersViewHolder.customer_address_field.getText().toString(), _customersViewHolder.customer_contact_no_field.getText().toString(),
				profFileName, latitude, longitude, 
				(int)Math.pow(2, Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1),
				compFileName, _customersViewHolder.customer_tin_field.getText().toString(),
				(int)FunctionHelper.paymentTermsList.get(_customersViewHolder.customer_term_field.getSelectedItemPosition()).getId(),
				(int)FunctionHelper.taxTypesList.get(_customersViewHolder.customer_tax_type_field.getSelectedItemPosition()).getId());
		
		if (customerId > 0)
		{
			MainActivity._deskpadDbHelper.updateCustomer(_customerProfile);
			//FunctionHelper.customerList.add(_customerProfile);
			int cindx = FunctionHelper.GetCustomersIndx(customerId);
			CustomerProfile _cf_customerProfile = FunctionHelper.customerList.get(cindx);
			_cf_customerProfile.setCustomerId(customerId);
			_cf_customerProfile.setCustomerName(_customerProfile.getCustomerName());
			_cf_customerProfile.setAddress(_customerProfile.getAddress());
			_cf_customerProfile.setCompImagePath(_customerProfile.getCompImagePath());
			_cf_customerProfile.setContactNo(_customerProfile.getContactNo());
			_cf_customerProfile.setGeoLatitude(_customerProfile.getGeoLatitude());
			_cf_customerProfile.setGeoLongitude(_customerProfile.getGeoLongitude());
			_cf_customerProfile.setProfImagePath(_customerProfile.getProfImagePath());
			_cf_customerProfile.setRouteDays(_customerProfile.getRouteDays());
			_cf_customerProfile.setTaxType(_customerProfile.getTaxType());
			_cf_customerProfile.setTerm(_customerProfile.getTerm());
			_cf_customerProfile.setTin(_customerProfile.getTin());
		}
		else
		{
			long Insert_ID = MainActivity._deskpadDbHelper.addNewCustomer(_customerProfile);
			_customerProfile.setCustomerId(Insert_ID);
			FunctionHelper.customerList.add(_customerProfile);
		}
		
		onBackPressed();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.order_view, menu);
		return true;
	}
	
	public class CustomersViewHolder
	{
		ImageView customer_prof_image;
		ImageView customer_comp_image;
		TextView customer_id_field;
		EditText customer_name_field;
		EditText customer_address_field;
		EditText customer_contact_no_field;
		EditText customer_geo_latitude_field;
		EditText customer_geo_longitude_field;
		Spinner customer_term_field;
		Spinner customer_tax_type_field;
		EditText customer_tin_field;
	}
	
}
