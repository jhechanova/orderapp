package com.example.orderapp;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calendar.CalendarView;
import com.example.calendar.CalendarView.OnCellTouchListener;
import com.example.calendar.Cell;
import com.example.classobj.CustomerProfile;
import com.example.classobj.ItemDetails;
import com.example.classobj.ItemProfile;
import com.example.classobj.OrderHeader;
import com.example.classobj.PaymentTerms;
import com.example.classobj.SalesmanProfile;
import com.example.helper.FunctionHelper;
import com.example.list.view.helper.OrdersItemsListAdapterHelper;
import com.example.misc.AlertDialogManager;

public class OrderFormActivity extends FragmentActivity implements com.example.calendar.CalendarView.OnCellTouchListener {

	// buttons
	private Button save_order;
	private Button update_order;
	private Button approve_order;
	private Button print_order;
	
	// items list
	private Dialog orderItemDialog;
	
	public static CustomerProfile _customerProfile;
	// end
	
	private long orderNO;
	
	OrderHeader _orderHeader;
	OrdersViewHolder _ordersViewHolder;
	OrdersDetailViewHolder _ordersDetailViewHolder;
	
	private CalendarView mView = null;
	private Dialog calendarDialog;
	private ImageButton calNext;
	private ImageButton calPrev;
	private TextView calDate;
	
	public static ArrayList<ItemDetails> _itemDetails;
	
	public ArrayList<ItemProfile> _itemList;
	public ArrayList<ItemProfile> _itemListDel;
	private ListView _listView;
	
	private double vatableAmnt = 0.0;
	private double vatAmnt = 0.0;	
	
	private Button calObj = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orders_form);
		
		_itemDetails = new ArrayList<ItemDetails>();
		
		_itemList = new ArrayList<ItemProfile>();
		_itemListDel = new ArrayList<ItemProfile>();
		
		Intent orderViewIntent = getIntent();
		orderNO = orderViewIntent.getLongExtra("order_no", 0);
		
		save_order = (Button) findViewById(R.id.save_order);
		update_order = (Button) findViewById(R.id.update_order);
		approve_order = (Button) findViewById(R.id.approve_order);
		print_order = (Button) findViewById(R.id.print_order);
		
		// orders header fields
		_ordersViewHolder = new OrdersViewHolder();
		_ordersViewHolder.order_date = (Button) findViewById(R.id.order_date_fld);
		_ordersViewHolder.order_total_price = (TextView) findViewById(R.id.order_total_price_fld);
		_ordersViewHolder.order_customer_name = (AutoCompleteTextView) findViewById(R.id.order_customer_name);
		/**_ordersViewHolder.order_address = (EditText) findViewById(R.id.order_address);
		_ordersViewHolder.order_contact_no = (EditText) findViewById(R.id.order_contact_no);**/
		_ordersViewHolder.order_customer_id = (EditText) findViewById(R.id.order_customer_id);
		_ordersViewHolder.order_no = (TextView) findViewById(R.id.order_no);
		_ordersViewHolder.payment_terms = (Spinner) findViewById(R.id.order_payment_terms);	
		_ordersViewHolder.payment_method = (Spinner) findViewById(R.id.order_payment_method); // added sept. 27, 2014
		_ordersViewHolder.bank = (Spinner) findViewById(R.id.order_bank);
		_ordersViewHolder.amount = (EditText) findViewById(R.id.order_amount);
		_ordersViewHolder.check_date = (Button) findViewById(R.id.order_check_date);
		_ordersViewHolder.check_no = (EditText) findViewById(R.id.order_check_no);
		_ordersViewHolder.bank_layout = (LinearLayout) findViewById(R.id.order_bank_layout);
		_ordersViewHolder.check_no_layout = (LinearLayout) findViewById(R.id.order_check_no_layout);
		_ordersViewHolder.check_date_layout = (LinearLayout) findViewById(R.id.order_check_date_layout);
		
		// orders detail fields
		_ordersDetailViewHolder = new OrdersDetailViewHolder();
		_ordersDetailViewHolder.order_detail_description = (AutoCompleteTextView) findViewById(R.id.oidescription_fld);
		_ordersDetailViewHolder.order_detail_price = (TextView) findViewById(R.id.oiprice_fld);
		_ordersDetailViewHolder.order_detail_qoh = (TextView) findViewById(R.id.oiqoh_fld);
		_ordersDetailViewHolder.order_detail_discount = (EditText) findViewById(R.id.oidiscount_fld);
		_ordersDetailViewHolder.order_detail_qty = (EditText) findViewById(R.id.oiqty_fld);
		_ordersDetailViewHolder.order_detail_total = (TextView) findViewById(R.id.oitotalprice_fld);
		_ordersDetailViewHolder.order_detail_add = (Button) findViewById(R.id.oiadd_btn);
		_ordersDetailViewHolder.order_detail_itemid = (EditText) findViewById(R.id.oiitemid_fld);
		
		ArrayAdapter<String> customerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, 
				FunctionHelper.GetCustomers());
        _ordersViewHolder.order_customer_name.setAdapter(customerAdapter);
        
        _ordersViewHolder.order_customer_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            		            	
	            	CustomerProfile _customerProfile = FunctionHelper.customerList.get(FunctionHelper.GetCustomersIndx(_ordersViewHolder.order_customer_name.getText().toString()));
	            	
					_ordersViewHolder.order_customer_id.setText(String.valueOf(_customerProfile.getCustomerId()));
					/**_ordersViewHolder.order_address.setText(_customerProfile.getAddress()); 
					_ordersViewHolder.order_contact_no.setText(_customerProfile.getContactNo());**/
					_ordersViewHolder.payment_terms.setSelection(FunctionHelper.GetPaymentTermsIndx(_customerProfile.getTerm()));
	            }
	      });
        
        ArrayAdapter<String> itemAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, 
				FunctionHelper.GetItems());
        _ordersDetailViewHolder.order_detail_description.setAdapter(itemAdapter);
        
        _ordersDetailViewHolder.order_detail_description.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            		            	
	            	ItemProfile _itemProfile = FunctionHelper.itemsList.get(FunctionHelper.GetItemsIndx(_ordersDetailViewHolder.order_detail_description.getText().toString()));
	            	
	            	_ordersDetailViewHolder.order_detail_itemid.setText(String.valueOf(_itemProfile.getItemId()));
	            	_ordersDetailViewHolder.order_detail_price.setText(String.valueOf(_itemProfile.getPrice()));
	            	_ordersDetailViewHolder.order_detail_qoh.setText(String.valueOf(_itemProfile.getQty()));
	            }
	      });
        
        TextWatcher tw = new TextWatcher() {
            public void afterTextChanged(Editable s){
            	
            	double discount = 0.0;
            	double qty = 0.0;
            	double price = Double.valueOf(_ordersDetailViewHolder.order_detail_price.getText().toString());
            	double qoh = Double.valueOf(_ordersDetailViewHolder.order_detail_qoh.getText().toString());
            	
            	if (_ordersDetailViewHolder.order_detail_discount.getText().toString().length() > 0)
            	{
            		discount = Double.valueOf(_ordersDetailViewHolder.order_detail_discount.getText().toString());
            	}
            	if (_ordersDetailViewHolder.order_detail_qty.getText().toString().length() > 0)
            	{
            		qty = Double.valueOf(_ordersDetailViewHolder.order_detail_qty.getText().toString());
            	}
            	
            	if (qty > 0.0 && qty > qoh)
            	{
            		AlertDialogManager adm = new AlertDialogManager(OrderFormActivity.this);
					adm.fail("Information", 
							"The quantity inputed is greater than the current quantity on hand ( QoH : " + qoh + " ) . Quantity will change to zero.");
					adm.show();
					
					_ordersDetailViewHolder.order_detail_qty.setText("");
					qty = 0.0;
            	}
            		
            	_ordersDetailViewHolder.order_detail_total.setText(String.valueOf((price-discount)*qty));
            }
            public void  beforeTextChanged(CharSequence s, int start, int count, int after){
              // you can check for enter key here
            }
            public void  onTextChanged (CharSequence s, int start, int before,int count) {
            	
            } 
        };
        
        _ordersDetailViewHolder.order_detail_discount.addTextChangedListener(tw);
        _ordersDetailViewHolder.order_detail_qty.addTextChangedListener(tw);
        
        _ordersDetailViewHolder.order_detail_add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (_ordersDetailViewHolder.order_detail_itemid.getText().length() == 0
						|| _ordersDetailViewHolder.order_detail_qty.getText().length() == 0)
				{
					AlertDialogManager adm = new AlertDialogManager(OrderFormActivity.this);
					adm.fail("Error", 
							"Either item description or quantity field is empty.");
					adm.show();

					return;
				}
				
				long item_id = Long.valueOf(_ordersDetailViewHolder.order_detail_itemid.getText().toString());
						
				if (_itemDetails != null)
				{
					_itemDetails.add(new ItemDetails());
					_itemDetails.get(_itemDetails.size() - 1).setItemId(item_id);
					
					double orderDetailDiscount = (_ordersDetailViewHolder.order_detail_discount.getText().length() == 0)
							? 0.0 : Double.valueOf(_ordersDetailViewHolder.order_detail_discount.getText().toString());
					
					_itemDetails.get(_itemDetails.size() - 1).setItemDesc(orderDetailDiscount);
					_itemDetails.get(_itemDetails.size() - 1).setItemQty(Double.valueOf(_ordersDetailViewHolder.order_detail_qty.getText().toString()));
					_itemDetails.get(_itemDetails.size() - 1).setToBeAdded(true);
				}
				
				// init default
				
				_ordersDetailViewHolder.order_detail_description.setText("");
				_ordersDetailViewHolder.order_detail_price.setText("0.0");
				_ordersDetailViewHolder.order_detail_qoh.setText("0.0");
				_ordersDetailViewHolder.order_detail_qty.setText("");
				_ordersDetailViewHolder.order_detail_discount.setText("");
				_ordersDetailViewHolder.order_detail_total.setText("0.0");
				_ordersDetailViewHolder.order_detail_itemid.setText("");
				
				displayItemsAdded();
			}
		});
        
		ArrayAdapter<String> paymentTypeArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetPaymentTerms() ); 
		paymentTypeArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_ordersViewHolder.payment_terms.setAdapter(paymentTypeArrAdapter);
		
		// added sept. 27, 2014
		ArrayAdapter<String> bankArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetBanks() ); 
		bankArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_ordersViewHolder.bank.setAdapter(bankArrAdapter);
		
		ArrayAdapter<String> paymentMethodArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetPaymentMethods() ); 
		paymentMethodArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_ordersViewHolder.payment_method.setAdapter(paymentMethodArrAdapter);
		
		save_order.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SaveNewOrder("new");
			}
		});
		
		update_order.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SaveNewOrder("new");
			}
		});
		
		approve_order.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SaveNewOrder("approve");
			}
		});
		
		print_order.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				vatableAmnt = _orderHeader.getOrderTotalPrice() / 1.12;
				vatAmnt = vatableAmnt * 0.12;
				
				if (_orderHeader.getPaymentTermsId() == 1)
					PrintOR();
				else
					PrintDR();
			}
		});
		
		_ordersViewHolder.order_date.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showCalendar();
				
				calObj = _ordersViewHolder.order_date;
			}
		});
		
		// added sept. 27, 2014
		_ordersViewHolder.check_date.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showCalendar();
				
				calObj = _ordersViewHolder.check_date;
			}
		});
		
		_ordersViewHolder.payment_method.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (_ordersViewHolder.payment_method.getSelectedItem().toString() == "Check")
				{
					// added sept. 27, 2014
					_ordersViewHolder.bank_layout.setVisibility(View.VISIBLE);
					_ordersViewHolder.check_date_layout.setVisibility(View.VISIBLE);
					_ordersViewHolder.check_no_layout.setVisibility(View.VISIBLE);
				}
				else if (_ordersViewHolder.payment_method.getSelectedItem().toString() == "Cash")
				{
					_ordersViewHolder.bank_layout.setVisibility(View.GONE);
					_ordersViewHolder.check_date_layout.setVisibility(View.GONE);
					_ordersViewHolder.check_no_layout.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		if (orderNO > 0)
		{
			save_order.setVisibility(View.INVISIBLE);
			print_order.setVisibility(View.INVISIBLE);
			
			_orderHeader = MainActivity._deskpadDbHelper.getOrder(orderNO);
			
			if (_orderHeader != null)
			{
				this._customerProfile = MainActivity._deskpadDbHelper.getCustomer(_orderHeader.getCustomerId());
				_ordersViewHolder.order_date.setText(_orderHeader.getOrderDate());
				_ordersViewHolder.order_customer_name.setText(this._customerProfile.getCustomerName());
				_ordersViewHolder.order_customer_id.setText(String.valueOf(_orderHeader.getCustomerId()));
				/**_ordersViewHolder.order_address.setText(_orderHeader.getAddress());
				_ordersViewHolder.order_contact_no.setText(_orderHeader.getContactNo());**/
				_ordersViewHolder.order_total_price.setText(String.valueOf(_orderHeader.getOrderTotalPrice()));
				_ordersViewHolder.order_no.setText(String.format("%08d", _orderHeader.getOrderNo()));
				
				// added sept. 27, 2014
				_ordersViewHolder.payment_method.setSelection(FunctionHelper.GetPaymentMethodsIndx(_orderHeader.getPaymentMethod()));
				_ordersViewHolder.bank.setSelection(FunctionHelper.GetBanksIndx(_orderHeader.getBankId()));
				_ordersViewHolder.check_date.setText(_orderHeader.getCheckDate());
				_ordersViewHolder.check_no.setText(_orderHeader.getCheckNo());
				_ordersViewHolder.amount.setText(String.valueOf(_orderHeader.getAmount()));
								
				_ordersViewHolder.payment_terms.setSelection(FunctionHelper.GetPaymentTermsIndx(_orderHeader.getPaymentTermsId()));
				
				// added sept. 27, 2014
				if (_orderHeader.getPaymentMethod() == "Check")
				{
					// added sept. 27, 2014
					_ordersViewHolder.bank_layout.setVisibility(View.VISIBLE);
					_ordersViewHolder.check_date_layout.setVisibility(View.VISIBLE);
					_ordersViewHolder.check_no_layout.setVisibility(View.VISIBLE);
				}
				else if (_orderHeader.getPaymentMethod() == "Cash")
				{
					_ordersViewHolder.bank_layout.setVisibility(View.GONE);
					_ordersViewHolder.check_date_layout.setVisibility(View.GONE);
					_ordersViewHolder.check_no_layout.setVisibility(View.GONE);
				}
				
				List<String> _itemIds = null;
				List<String> _itemQtys = null;
				List<String> _itemDesc = null;
				
				if (!_orderHeader.getOrderItems().isEmpty())
					_itemIds = Arrays.asList(_orderHeader.getOrderItems().split(","));
				if (!_orderHeader.getOrderItemsQty().isEmpty())
					_itemQtys = Arrays.asList(_orderHeader.getOrderItemsQty().split(","));
				if (!_orderHeader.getOrderItemsDesc().isEmpty())
					_itemDesc = Arrays.asList(_orderHeader.getOrderItemsDesc().split(","));
								
				if (_itemIds != null)
				{
					for (int indx = 0; indx < _itemIds.size(); indx++)
					{
						ItemDetails _itemDetails = new ItemDetails();
						_itemDetails.setItemId(Long.valueOf(_itemIds.get(indx)));
						_itemDetails.setItemQty(Double.valueOf(_itemQtys.get(indx)));
						_itemDetails.setItemDesc(Double.valueOf(_itemDesc.get(indx)));
						_itemDetails.setToBeAdded(true);
						
						this._itemDetails.add(_itemDetails);
					}
				}

				displayItemsAdded();
			}
		}
		else
		{
			update_order.setVisibility(View.INVISIBLE);
			approve_order.setVisibility(View.INVISIBLE);
			print_order.setVisibility(View.INVISIBLE);
			
			// added sept. 27, 2014
			_ordersViewHolder.bank_layout.setVisibility(View.GONE);
			_ordersViewHolder.check_date_layout.setVisibility(View.GONE);
			_ordersViewHolder.check_no_layout.setVisibility(View.GONE);
			
			final Calendar dateNow = Calendar.getInstance();
	        SimpleDateFormat format = new SimpleDateFormat("EEE, MMM dd, yyyy");
	        
			_ordersViewHolder.order_date.setText(format.format(dateNow.getTime()));
			_ordersViewHolder.order_no.setText("New Order");
			
			// added sept. 27, 2014
			_ordersViewHolder.check_date.setText(format.format(dateNow.getTime()));
		}
		
		if (_orderHeader != null && _orderHeader.getStatus().equals("approve"))
		{
			update_order.setVisibility(View.INVISIBLE);
			approve_order.setVisibility(View.INVISIBLE);
			
			print_order.setVisibility(View.VISIBLE);
		}
	}

	private void PrintDR()
	{
		MainActivity._printerConf.connectPrinter();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (MainActivity._printerConf.mIsConnected)
				{					
					// DR
					MainActivity._printerConf.printLineFeed(2, false);
					MainActivity._printerConf.printHeader(); // consist of company name, address and reg tin
					
					
					PrintHeader("DELIVERY RECEIPT NO. " + FunctionHelper.lpad(_orderHeader.getOrderNo(), 8));
					
					
					PrintData();
					
					
					MainActivity._printerConf.printText("Terms : " + _orderHeader.getPaymentTermsName(), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();	
					
					PrintMethod();
					
					MainActivity._printerConf.printDivider();
					
					// details printing
					PrintDetail();
					
					MainActivity._printerConf.printDivider();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("VATable",20), String.format("%.02f", vatableAmnt)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("12% VAT", 20), String.format("%.02f", vatAmnt)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.indentString("VAT-Exempt", 20), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace("Total Sales (VAT Inclusive)", String.format("%.02f", vatableAmnt + vatAmnt)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.indentString("12% VAT Included", 15),
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					if (_orderHeader.getAmount() > 0)
					{
						MainActivity._printerConf.printDivider();
						MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Amount Deposit", 20), String.format("%.02f", _orderHeader.getAmount())), 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
						MainActivity._printerConf.printNextLine();
						MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Balance", 20), String.format("%.02f", (vatableAmnt + vatAmnt) - _orderHeader.getAmount())), 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
						MainActivity._printerConf.printNextLine();
					}
					MainActivity._printerConf.printLineFeed(1, false);
					PaymentTerms _paymentTerms = MainActivity._deskpadDbHelper.getPaymentTerm(_orderHeader.getPaymentTermsId());
					
					try
					{
						SimpleDateFormat format = null;
						
						format = new SimpleDateFormat(FunctionHelper.fldFormat);
						Date dat = format.parse(_orderHeader.getOrderDate());
						Calendar cal = Calendar.getInstance();
						cal.setTime(dat);
						cal.add(Calendar.DAY_OF_MONTH, _paymentTerms.getDays());
						
						format = new SimpleDateFormat(FunctionHelper.printFormat);	
						
						MainActivity._printerConf.printText("1. Payment Date: " + format.format(cal.getTime()) , 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
					
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText("2. Full payment required to issue VAT OR.", 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText("3. Please present this receipt upon full payment to claim VAT OR", 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printLineFeed(2, false);
					
					// footer
					MainActivity._printerConf.printText("This serves as your Delivery Receipt", 
							MainActivity._printerConf.acenter, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText("For feedback , please call us at (260-3610 / 260-8426)", 
							MainActivity._printerConf.acenter, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					
					MainActivity._printerConf.printLineFeed(4, true);
					
					MainActivity._printerConf.disconnectPrinter();
				}
				else
					Toast.makeText(getApplicationContext(), "No printer found. Either bluetooth is off or printer is off.", Toast.LENGTH_SHORT).show();
			}
		}, 5000);
	}
	
	// added sept. 29, 2014
	private void PrintMethod()
	{
		if (_orderHeader.getPaymentMethod().equals("Cash"))
		{
			MainActivity._printerConf.printText("Payment Method: " + _orderHeader.getPaymentMethod(), 
					MainActivity._printerConf.aleft, 
					MainActivity._printerConf.fontC, 
					MainActivity._printerConf.sizeh1);
			MainActivity._printerConf.printNextLine();
		}
		else if (_orderHeader.getPaymentMethod().equals("Check"))
		{
			MainActivity._printerConf.printText("Payment Method: " + _orderHeader.getPaymentMethod(), 
					MainActivity._printerConf.aleft, 
					MainActivity._printerConf.fontC, 
					MainActivity._printerConf.sizeh1);
			MainActivity._printerConf.printNextLine();
			MainActivity._printerConf.printText("Bank: " + _orderHeader.getBankName(), 
					MainActivity._printerConf.aleft, 
					MainActivity._printerConf.fontC, 
					MainActivity._printerConf.sizeh1);
			MainActivity._printerConf.printNextLine();
			MainActivity._printerConf.printText(FunctionHelper.concatWithSpace("Check No.:" + _orderHeader.getCheckNo(), 
					"Check Date:" + FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.printFormat, _orderHeader.getCheckDate())), 
					MainActivity._printerConf.aleft, 
					MainActivity._printerConf.fontC, 
					MainActivity._printerConf.sizeh1);
			MainActivity._printerConf.printNextLine();
		}
	}
	
	private void PrintHeader(String header)
	{
		MainActivity._printerConf.printText(header, 
				MainActivity._printerConf.aleft, 
				MainActivity._printerConf.fontC, 
				MainActivity._printerConf.sizeh1);
		MainActivity._printerConf.printNextLine();
		Calendar cal = Calendar.getInstance();
		MainActivity._printerConf.printText(
				FunctionHelper.concatWithSpace(
						FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.printFormat, _orderHeader.getOrderDate()),
						cal.getTime().getHours()+":"+cal.getTime().getMinutes()
				), 
				MainActivity._printerConf.aleft, 
				MainActivity._printerConf.fontC, 
				MainActivity._printerConf.sizeh1);
		MainActivity._printerConf.printNextLine();
		MainActivity._printerConf.printDivider();
	}
	
	private void PrintData()
	{
		SalesmanProfile _salesmanProfile = MainActivity._deskpadDbHelper.getSelectedSalesman();
		
		MainActivity._printerConf.printText("Salesman: " + _salesmanProfile.getSalesmanName(), 
				MainActivity._printerConf.aleft, 
				MainActivity._printerConf.fontC, 
				MainActivity._printerConf.sizeh1);
		MainActivity._printerConf.printNextLine();
		MainActivity._printerConf.printText("Customer: " + OrderFormActivity._customerProfile.getCustomerName(),
				MainActivity._printerConf.aleft, 
				MainActivity._printerConf.fontC, 
				MainActivity._printerConf.sizeh1);
		MainActivity._printerConf.printNextLine();
		MainActivity._printerConf.printText("Address: " + OrderFormActivity._customerProfile.getAddress(),
				MainActivity._printerConf.aleft, 
				MainActivity._printerConf.fontC, 
				MainActivity._printerConf.sizeh1);
		MainActivity._printerConf.printNextLine();
		MainActivity._printerConf.printText("TIN: " + OrderFormActivity._customerProfile.getTin(),
				MainActivity._printerConf.aleft, 
				MainActivity._printerConf.fontC, 
				MainActivity._printerConf.sizeh1);
		MainActivity._printerConf.printNextLine();
	}
	
	private void PrintDetail()
	{
		for (ItemDetails itemDetail : _itemDetails)
		{
			ItemProfile itemProfile = MainActivity._deskpadDbHelper.getItem(itemDetail.getItemId());
			MainActivity._printerConf.printText(
					FunctionHelper.concatWithSpace(
							(int)itemDetail.getItemQty() + " " + FunctionHelper.uomsList.get(FunctionHelper.GetUOMsIndx(itemProfile.getUOM())).getShort()
							+ " " + itemProfile.getDescription() + " @" + String.format("%.02f", itemProfile.getPrice()),
							String.format("%.02f", (itemProfile.getPrice() -  itemDetail.getItemDesc()) * (itemDetail.getItemQty()))
					),
					MainActivity._printerConf.aleft, 
					MainActivity._printerConf.fontC, 
					MainActivity._printerConf.sizeh1);
			MainActivity._printerConf.printNextLine();
			MainActivity._printerConf.printText("   Disc: " + String.format("%.02f", itemDetail.getItemDesc()),
					MainActivity._printerConf.aleft, 
					MainActivity._printerConf.fontC, 
					MainActivity._printerConf.sizeh1);
			MainActivity._printerConf.printNextLine();
		}
	}
	
	private void PrintOR()
	{
		MainActivity._printerConf.connectPrinter();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (MainActivity._printerConf.mIsConnected)
				{			
					
					// OR
					MainActivity._printerConf.printLineFeed(2, false);
					MainActivity._printerConf.printHeader(); // consist of company name, address and reg tin
					
					
					PrintHeader(FunctionHelper.concatWithSpace("OFFICIAL RECEIPT", "REFERENCE NO. " + FunctionHelper.lpad(_orderHeader.getOrderNo(), 8)));
					
					
					PrintData();
					
					PrintMethod();
					
					MainActivity._printerConf.printDivider();
					// details printing
					
					PrintDetail();
				
					
					MainActivity._printerConf.printDivider();					
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("VATable",20), String.format("%.02f", vatableAmnt)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("12% VAT", 20), String.format("%.02f", vatAmnt)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Amount Due", 20), String.format("%.02f", vatableAmnt + vatAmnt)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printDivider();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Amount Pay", 20), String.format("%.02f", _orderHeader.getAmount())), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Change", 20), String.format("%.02f", _orderHeader.getAmount() - (vatableAmnt + vatAmnt))), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printLineFeed(2, false);
					
					// footer
					MainActivity._printerConf.printText("Thank you!", 
							MainActivity._printerConf.acenter, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					
					MainActivity._printerConf.printLineFeed(4, true);
					
					MainActivity._printerConf.disconnectPrinter();
				}
				else
					Toast.makeText(getApplicationContext(), "No printer found. Either bluetooth is off or printer is off.", Toast.LENGTH_SHORT).show();
			}
		}, 5000);
	}
	
	private void displayItemsAdded()
	{
		ListView _listView = (ListView)findViewById(R.id.order_items_added);
        Button _button = (Button)findViewById(R.id.delete_order_items);
		
        if (_orderHeader != null && _orderHeader.getStatus().equals("approve"))
		{
        	_button.setVisibility(View.INVISIBLE);
		}
        else
        {
        	_button.setVisibility(View.VISIBLE);
        }
        
		OrdersItemsListAdapterHelper _itemAdapterHelper;
		
		StringBuilder sbItemId = new StringBuilder();
		StringBuilder sbItemQty = new StringBuilder();
		StringBuilder sbItemDesc = new StringBuilder();
		
		for (ItemDetails idet : _itemDetails)
		{
			if (!idet.getToBeAdded())
				continue;
			
			sbItemId.append(idet.getItemId());
			sbItemQty.append(idet.getItemQty());
			sbItemDesc.append(idet.getItemDesc());
			
			if(sbItemId.length() > 0)
				sbItemId.append(",");
			
			if(sbItemQty.length() > 0)
				sbItemQty.append(",");
			
			if(sbItemDesc.length() > 0)
				sbItemDesc.append(",");
		}
		
		_itemListDel = MainActivity._deskpadDbHelper.getSelectedItems(sbItemId.toString().replaceAll(",$", ""));
		
		Double orderTotalPrice = 0.0;
		for (ItemProfile ip : _itemListDel)
		{
			orderTotalPrice += (ip.getPrice()-_itemDetails.get(getIndexFromItemDetails(ip.getItemId())).getItemDesc()) * _itemDetails.get(getIndexFromItemDetails(ip.getItemId())).getItemQty();
		}
		DecimalFormat twoDForm =new DecimalFormat("#.##");
		orderTotalPrice = Double.valueOf(twoDForm.format(orderTotalPrice));
		_ordersViewHolder.order_total_price.setText(String.valueOf(orderTotalPrice));
		
		if (_itemListDel != null)
		{
			_itemAdapterHelper = new OrdersItemsListAdapterHelper(this, _itemListDel, _itemDetails, true);
			
			_listView.setAdapter(_itemAdapterHelper);
			
			_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapter, View view, int position, long arg)   {

				    // TODO Auto-generated method stub
					ItemProfile _itemProfile = (ItemProfile)OrderFormActivity.this._itemListDel.get(position);
					
					com.example.list.view.helper.OrdersItemsListAdapterHelper.ItemsViewHolder _itemViewHolder = (com.example.list.view.helper.OrdersItemsListAdapterHelper.ItemsViewHolder) view.getTag();
					
					if (_itemViewHolder.getCheckBox().isChecked())
					{
						_itemViewHolder.getCheckBox().setChecked(false);
						
						if (_itemDetails.size() > 0)
						{
							if (existFromItemDetails(_itemProfile.getItemId()))
								_itemDetails.get(getIndexFromItemDetails(_itemProfile.getItemId())).setToBeDeleted(false);
						}
					}
					else
					{
						_itemViewHolder.getCheckBox().setChecked(true);
						
						if (_itemDetails != null)
						{
							if (existFromItemDetails(_itemProfile.getItemId()))
								_itemDetails.get(getIndexFromItemDetails(_itemProfile.getItemId())).setToBeDeleted(true);
						}
					}
				}
			});
			
			_button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<ItemDetails> _itemDetailsTmp = new ArrayList<ItemDetails>();
					_itemDetailsTmp = _itemDetails;
					
					for(ItemDetails idet : _itemDetailsTmp)
					{
						if (idet.getToBeDeleted())
							_itemDetails.get(getIndexFromItemDetails(idet.getItemId())).setToBeAdded(false);
					}
					
					displayItemsAdded();
				}
			});
			
			FunctionHelper.getListViewSize(_listView);
		}
	}
	
	private void showCalendar()
	{		        		
		calendarDialog = new Dialog(this);

		calendarDialog.setTitle("Set Date");
		calendarDialog.setContentView(R.layout.activity_calendar_view);
		
		mView = (CalendarView) calendarDialog.findViewById(R.id.calendar);
		mView.setOnCellTouchListener((OnCellTouchListener) this);
		
		calNext = (ImageButton) calendarDialog.findViewById(R.id.calendarNext);
		calPrev = (ImageButton) calendarDialog.findViewById(R.id.calendarPrev);
		calDate = (TextView) calendarDialog.findViewById(R.id.calendarDate);
		
		setCalDate();
		
		calNext.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mView.nextMonth();
				setCalDate();
			}
		});
		
		calPrev.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mView.previousMonth();
				setCalDate();
			}
		});
				
		calendarDialog.show();
	}
	
	@Override
	public void onTouch(Cell cell) {
		// TODO Auto-generated method stub
		int year  = mView.getYear();
		int month = mView.getMonth();
		int day   = cell.getDayOfMonth();
		
		final Calendar dat = Calendar.getInstance();
        dat.set(Calendar.YEAR, year);
        dat.set(Calendar.MONTH, month);
        dat.set(Calendar.DAY_OF_MONTH, day);
        
        // show result
        SimpleDateFormat format = new SimpleDateFormat("EEE, MMM dd, yyyy");
        calObj.setText(format.format(dat.getTime()));
	
        calendarDialog.dismiss();
	}
	
	private void setCalDate()
	{
		final Calendar dat = Calendar.getInstance();
        dat.set(Calendar.YEAR, mView.getYear());
        dat.set(Calendar.MONTH, mView.getMonth());
        
		SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");
		calDate.setText(format.format(dat.getTime()));
	}
	
	private void SaveNewOrder(String status)
	{
		if (_itemDetails.isEmpty() || _ordersViewHolder.order_customer_id.getText().length() == 0 
				|| _ordersViewHolder.payment_terms.getSelectedItemPosition() == -1
				|| _ordersViewHolder.payment_method.getSelectedItemPosition() == -1)
		{
			AlertDialogManager adm = new AlertDialogManager(OrderFormActivity.this);
			adm.fail("Error", 
					"Either is customer field is empty or payment terms field is empty or payment method field is empty or no item added.");
			adm.show();
			
			return;
		}
		
		StringBuilder sbItemId = new StringBuilder();
		StringBuilder sbItemQty = new StringBuilder();
		StringBuilder sbItemDesc = new StringBuilder();
		
		for (ItemDetails idet : _itemDetails)
		{			
			if (!idet.getToBeAdded())
				continue;
			
			sbItemId.append(idet.getItemId());
			sbItemQty.append(idet.getItemQty());
			sbItemDesc.append(idet.getItemDesc());
			
			if(sbItemId.length() > 0)
				sbItemId.append(",");
			
			if(sbItemQty.length() > 0)
				sbItemQty.append(",");
			
			if(sbItemDesc.length() > 0)
				sbItemDesc.append(",");
		}
				
		String _pmethod = _ordersViewHolder.payment_method.getSelectedItem().toString();
		double _pamount = 0.0;
		if (_ordersViewHolder.amount.getText().toString().length() > 0)
    	{
			_pamount = Double.valueOf(_ordersViewHolder.amount.getText().toString());
    	}
				
//		if (FunctionHelper.paymentTermsList.get(_ordersViewHolder.payment_terms.getSelectedItemPosition()).getId() == 1) // considered COD
//		{
//			_pmethod = "Cash";
//			_pamount = Double.valueOf(_ordersViewHolder.order_total_price.getText().toString());
//		}
		
		OrderHeader _orderHeader = new OrderHeader(orderNO, Long.parseLong(_ordersViewHolder.order_customer_id.getText().toString()), 
				_ordersViewHolder.order_date.getText().toString(),
				sbItemId.toString().replaceAll(",$", ""), sbItemQty.toString().replaceAll(",$", ""),
				Double.valueOf(_ordersViewHolder.order_total_price.getText().toString()),
				FunctionHelper.paymentTermsList.get(_ordersViewHolder.payment_terms.getSelectedItemPosition()).getId(),
				status, sbItemDesc.toString().replaceAll(",$", ""),
				_pmethod, 
				_pamount,
				_ordersViewHolder.check_no.getText().toString(), _ordersViewHolder.check_date.getText().toString(),
				FunctionHelper.banksList.get(_ordersViewHolder.bank.getSelectedItemPosition()).getId());
		
		if (orderNO > 0)
			MainActivity._deskpadDbHelper.updateOrder(_orderHeader);
		else
			MainActivity._deskpadDbHelper.addNewOrder(_orderHeader);
		
		if (status.equals("approve"))
		{

			for (ItemDetails idet : _itemDetails)
			{
				if (!idet.getToBeAdded())
					continue;
				
				MainActivity._deskpadDbHelper.updateItemQty(idet.getItemId(), idet.getItemQty());
				
				ArrayList<ItemProfile> _sItemProfile = MainActivity._deskpadDbHelper.getSelectedItems(String.valueOf(idet.getItemId()));
				FunctionHelper.SetItemQty(idet.getItemId(), _sItemProfile.get(0).getQty());
			}
		}
		
		onBackPressed();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.order_view, menu);
		return true;
	}

	public static int getIndexFromItemDetails(long itemId)
	{
		for (int indx = 0; indx < _itemDetails.size(); indx++)
		{
			ItemDetails itemDetails = _itemDetails.get(indx);
			if (itemId == itemDetails.getItemId())
			{
				return indx;
			}
		}
		
		return 0;
	}
	
	public static boolean existFromItemDetails(long itemId)
	{
		for (int indx = 0; indx < _itemDetails.size(); indx++)
		{
			ItemDetails itemDetails = _itemDetails.get(indx);
			if (itemId == itemDetails.getItemId())
			{
				return true;
			}
		}
		
		return false;
	}
	
	public class OrdersViewHolder
	{
		Button order_date;
		AutoCompleteTextView order_customer_name;
		EditText order_customer_id;
		/**EditText order_address;
		EditText order_contact_no;**/
		TextView order_total_price;
		TextView order_no;
		Spinner payment_terms;
		Spinner payment_method; // added sept. 27, 2014
		EditText amount;
		Spinner bank;
		EditText check_no;
		Button check_date;
		
		LinearLayout bank_layout;
		LinearLayout check_no_layout;
		LinearLayout check_date_layout;
	}

	public class OrdersDetailViewHolder
	{
		AutoCompleteTextView order_detail_description;
		EditText order_detail_qty;
		EditText order_detail_discount;
		TextView order_detail_price;
		TextView order_detail_total;
		EditText order_detail_itemid;
		Button order_detail_add;
		TextView order_detail_qoh;
	}
	
}
