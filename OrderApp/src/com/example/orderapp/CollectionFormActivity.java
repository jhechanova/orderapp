package com.example.orderapp;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.example.calendar.CalendarView;
import com.example.calendar.CalendarView.OnCellTouchListener;
import com.example.calendar.Cell;
import com.example.classobj.CollectionDetails;
import com.example.classobj.CustomerCollection;
import com.example.classobj.CustomerProfile;
import com.example.classobj.SalesmanProfile;
import com.example.helper.FunctionHelper;
import com.example.list.view.helper.CollectionsDetailsAdapterHelper;
import com.example.misc.AlertDialogManager;

public class CollectionFormActivity extends FragmentActivity implements com.example.calendar.CalendarView.OnCellTouchListener {

	// buttons
	private Button update_collection;
	private Button approve_collection;
	private Button print_collection;
	
	private long collectionNO;
	
	private CustomerCollection _collection;
	private static CollectionViewHolder _collectionViewHolder;
	
	private static ArrayList<CollectionDetails> _collectionDetails;

	private ListView _listView;
	
	private Button calObj;
	
	private CalendarView mView = null;
	private Dialog calendarDialog;
	private ImageButton calNext;
	private ImageButton calPrev;
	private TextView calDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_collections_form);
		
		_collectionDetails = new ArrayList<CollectionDetails>();
		
		Intent orderViewIntent = getIntent();
		collectionNO = orderViewIntent.getLongExtra("collection_no", 0);
		
		update_collection = (Button) findViewById(R.id.update_collection);
		approve_collection = (Button) findViewById(R.id.approve_collection);
		print_collection = (Button) findViewById(R.id.print_collection);
		
		// collections header fields
		_collectionViewHolder = new CollectionViewHolder();
		_collectionViewHolder.collection_no = (TextView) findViewById(R.id.collection_no);
		_collectionViewHolder.collection_date = (TextView) findViewById(R.id.collection_date);
		_collectionViewHolder.collection_customer_name = (TextView) findViewById(R.id.collection_customer_name);
		_collectionViewHolder.collection_customer_id = (EditText) findViewById(R.id.collection_customer_id);	
		
		_collectionViewHolder.payment_method = (Spinner) findViewById(R.id.collection_payment_method); // added sept. 27, 2014
		_collectionViewHolder.bank = (Spinner) findViewById(R.id.collection_bank);
		_collectionViewHolder.amount = (EditText) findViewById(R.id.collection_amount);
		_collectionViewHolder.check_date = (Button) findViewById(R.id.collection_check_date);
		_collectionViewHolder.check_no = (EditText) findViewById(R.id.collection_check_no);
		_collectionViewHolder.bank_layout = (LinearLayout) findViewById(R.id.collection_bank_layout);
		_collectionViewHolder.check_no_layout = (LinearLayout) findViewById(R.id.collection_check_no_layout);
		_collectionViewHolder.check_date_layout = (LinearLayout) findViewById(R.id.collection_check_date_layout);
		
		// totals
		_collectionViewHolder.total_dr_amount = (TextView) findViewById(R.id.collections_total_amount);
		_collectionViewHolder.total_applied_amount = (TextView) findViewById(R.id.collections_total_applied);
		_collectionViewHolder.total_balance = (TextView) findViewById(R.id.collections_total_balance);
		
		// added sept. 27, 2014
		ArrayAdapter<String> bankArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetBanks() ); 
		bankArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_collectionViewHolder.bank.setAdapter(bankArrAdapter);
		
		ArrayAdapter<String> paymentMethodArrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, FunctionHelper.GetPaymentMethods() ); 
		paymentMethodArrAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_collectionViewHolder.payment_method.setAdapter(paymentMethodArrAdapter);
		
		update_collection.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				UpdateCollection("new");
			}
		});
		
		approve_collection.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				UpdateCollection("approve");
			}
		});
			
		print_collection.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PrintCollection();
			}
		});
		
		_collectionViewHolder.check_date.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showCalendar();
				
				calObj = _collectionViewHolder.check_date;
			}
		});
		
		_collectionViewHolder.payment_method.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (_collectionViewHolder.payment_method.getSelectedItem().toString() == "Check")
				{
					// added sept. 27, 2014
					_collectionViewHolder.bank_layout.setVisibility(View.VISIBLE);
					_collectionViewHolder.check_date_layout.setVisibility(View.VISIBLE);
					_collectionViewHolder.check_no_layout.setVisibility(View.VISIBLE);
				}
				else if (_collectionViewHolder.payment_method.getSelectedItem().toString() == "Cash")
				{
					_collectionViewHolder.bank_layout.setVisibility(View.GONE);
					_collectionViewHolder.check_date_layout.setVisibility(View.GONE);
					_collectionViewHolder.check_no_layout.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		_collection = MainActivity._deskpadDbHelper.getCollection(collectionNO);
		
		if (_collection != null)
		{
			_collectionViewHolder.collection_no.setText(String.format("%08d", _collection.getCollectionNo()));
			_collectionViewHolder.collection_date.setText(_collection.getCollectionDate());
			_collectionViewHolder.collection_customer_name.setText(_collection.getCustomerName());
			_collectionViewHolder.collection_customer_id.setText(String.valueOf(_collection.getCustomerId()));
			
			// added sept. 27, 2014
			Log.e("test", "a"+_collection.getPaymentMethod()+"b" +  String.valueOf(FunctionHelper.GetPaymentMethodsIndx(_collection.getPaymentMethod())));
			_collectionViewHolder.payment_method.setSelection(FunctionHelper.GetPaymentMethodsIndx(_collection.getPaymentMethod()));
			_collectionViewHolder.bank.setSelection(FunctionHelper.GetBanksIndx(_collection.getBankId()));
			_collectionViewHolder.check_date.setText(_collection.getCheckDate());
			_collectionViewHolder.check_no.setText(_collection.getCheckNo());
			_collectionViewHolder.amount.setText(String.valueOf(_collection.getAmount()));
			
			// added sept. 27, 2014
			if (_collection.getPaymentMethod() == "Check")
			{
				// added sept. 27, 2014
				_collectionViewHolder.bank_layout.setVisibility(View.VISIBLE);
				_collectionViewHolder.check_date_layout.setVisibility(View.VISIBLE);
				_collectionViewHolder.check_no_layout.setVisibility(View.VISIBLE);
			}
			else if (_collection.getPaymentMethod() == "Cash")
			{
				_collectionViewHolder.bank_layout.setVisibility(View.GONE);
				_collectionViewHolder.check_date_layout.setVisibility(View.GONE);
				_collectionViewHolder.check_no_layout.setVisibility(View.GONE);
			}
						
			List<String> _refNumbers = null;
			List<String> _refDates = null;
			List<String> _refAmounts = null;
			List<String> _appliedAmounts = null;
			
			if (!_collection.getRefNumbers().isEmpty())
				_refNumbers = Arrays.asList(_collection.getRefNumbers().split(","));
			if (!_collection.getRefDates().isEmpty())
				_refDates = Arrays.asList(_collection.getRefDates().split(","));
			if (!_collection.getRefAmounts().isEmpty())
				_refAmounts = Arrays.asList(_collection.getRefAmounts().split(","));
			if (!_collection.getAppliedAmount().isEmpty())
				_appliedAmounts = Arrays.asList(_collection.getAppliedAmount().split(","));
			
			double totalDRAmount = 0.0;
			double totalAppliedAmount = 0.0;
			double totalBalance = 0.0;
			
			if (_refNumbers != null)
			{
				for (int indx = 0; indx < _refNumbers.size(); indx++)
				{
					totalDRAmount += Double.valueOf(_refAmounts.get(indx));
					totalAppliedAmount += Double.valueOf(_appliedAmounts.get(indx));
					totalBalance += Double.valueOf(Double.valueOf(_refAmounts.get(indx))-Double.valueOf(_appliedAmounts.get(indx)));
							
					CollectionDetails _collectionDetails = new CollectionDetails();
					_collectionDetails.setDRNumber(Long.valueOf(_refNumbers.get(indx)));
					_collectionDetails.setDRDate(_refDates.get(indx));
					_collectionDetails.setDRAmount(Double.valueOf(_refAmounts.get(indx)));
					_collectionDetails.setAppliedAmount(Double.valueOf(_appliedAmounts.get(indx)));
					_collectionDetails.setBalance(Double.valueOf(_refAmounts.get(indx))-Double.valueOf(_appliedAmounts.get(indx)));
					
					this._collectionDetails.add(_collectionDetails);
				}
			}

			DecimalFormat formatter = new DecimalFormat("#,###,###.##");
			
			_collectionViewHolder.total_dr_amount.setText(formatter.format(totalDRAmount));
			_collectionViewHolder.total_applied_amount.setText(formatter.format(totalAppliedAmount));
			_collectionViewHolder.total_balance.setText(formatter.format(totalBalance));
			
			displayCollectionsAdded();
		}
		
		if (_collection != null && _collection.getStatus().equals("approve"))
		{
			update_collection.setVisibility(View.INVISIBLE);
			approve_collection.setVisibility(View.INVISIBLE);
			print_collection.setVisibility(View.VISIBLE);
		}
		
		if (_collection.getAmount() == 0.0)
    	{
    		AlertDialogManager adm = new AlertDialogManager(CollectionFormActivity.this);
			adm.fail("Information", 
					"Amount value is equal to zero. Please fill first payment method and amount then save before filling the applied amount field in the details.");
			adm.show();
    	}
	}

	private void showCalendar()
	{		        		
		calendarDialog = new Dialog(this);

		calendarDialog.setTitle("Set Date");
		calendarDialog.setContentView(R.layout.activity_calendar_view);
		
		mView = (CalendarView) calendarDialog.findViewById(R.id.calendar);
		mView.setOnCellTouchListener((OnCellTouchListener) this);
		
		calNext = (ImageButton) calendarDialog.findViewById(R.id.calendarNext);
		calPrev = (ImageButton) calendarDialog.findViewById(R.id.calendarPrev);
		calDate = (TextView) calendarDialog.findViewById(R.id.calendarDate);
		
		setCalDate();
		
		calNext.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mView.nextMonth();
				setCalDate();
			}
		});
		
		calPrev.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mView.previousMonth();
				setCalDate();
			}
		});
				
		calendarDialog.show();
	}
	
	@Override
	public void onTouch(Cell cell) {
		// TODO Auto-generated method stub
		int year  = mView.getYear();
		int month = mView.getMonth();
		int day   = cell.getDayOfMonth();
		
		final Calendar dat = Calendar.getInstance();
        dat.set(Calendar.YEAR, year);
        dat.set(Calendar.MONTH, month);
        dat.set(Calendar.DAY_OF_MONTH, day);
        
        // show result
        SimpleDateFormat format = new SimpleDateFormat("EEE, MMM dd, yyyy");
        calObj.setText(format.format(dat.getTime()));
	
        calendarDialog.dismiss();
	}
	
	private void setCalDate()
	{
		final Calendar dat = Calendar.getInstance();
        dat.set(Calendar.YEAR, mView.getYear());
        dat.set(Calendar.MONTH, mView.getMonth());
        
		SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");
		calDate.setText(format.format(dat.getTime()));
	}
	
	private void displayCollectionsAdded()
	{
		ListView _listView = (ListView)findViewById(R.id.collections_list);
		
		CollectionsDetailsAdapterHelper _collectionsDetailsAdapterHelper;
		
		_collectionsDetailsAdapterHelper = new CollectionsDetailsAdapterHelper(this, _collectionDetails, CollectionFormActivity.this, _collection);
		
		_listView.setAdapter(_collectionsDetailsAdapterHelper);
		
		FunctionHelper.getListViewSize(_listView);
	}

	private void UpdateCollection(String status)
	{		
		if ( _collectionViewHolder.payment_method.getSelectedItemPosition() == -1)
		{
			AlertDialogManager adm = new AlertDialogManager(CollectionFormActivity.this);
			adm.fail("Error", 
					"Payment method field is empty.");
			adm.show();
			
			return;
		}
				
		StringBuilder sbAppliedAmount = new StringBuilder();
		
		for (CollectionDetails cdet : _collectionDetails)
		{						
			sbAppliedAmount.append(cdet.getAppliedAmount());
			
			if(sbAppliedAmount.length() > 0)
				sbAppliedAmount.append(",");
		}
				
		_collection.setAppliedAmount(sbAppliedAmount.toString().replaceAll(", $", ""));
		_collection.setStatus(status);
		
		// added sept. 29, 2014
		double _pamount = 0.0;
		if (_collectionViewHolder.amount.getText().toString().length() > 0)
    	{
			_pamount = Double.valueOf(_collectionViewHolder.amount.getText().toString());
    	}
				
		_collection.setPaymentMethod(_collectionViewHolder.payment_method.getSelectedItem().toString());
		_collection.setBankId(FunctionHelper.banksList.get(_collectionViewHolder.bank.getSelectedItemPosition()).getId());
		_collection.setCheckDate(_collectionViewHolder.check_date.getText().toString());
		_collection.setCheckNo(_collectionViewHolder.check_no.getText().toString());
		_collection.setAmount(_pamount);

		MainActivity._deskpadDbHelper.updateCollection(_collection);
		
		onBackPressed();
	}
	
	private void PrintCollection()
	{
		MainActivity._printerConf.connectPrinter();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (MainActivity._printerConf.mIsConnected)
				{
					SalesmanProfile _salesmanProfile = MainActivity._deskpadDbHelper.getSelectedSalesman();
					CustomerProfile _customerProfile = MainActivity._deskpadDbHelper.getCustomer(_collection.getCustomerId());
					
					// OR
					MainActivity._printerConf.printLineFeed(2, false);
					MainActivity._printerConf.printHeader(); // consist of company name, address and reg tin
					
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace("COLLECTION RECEIPT", "REF NO. " + FunctionHelper.lpad(_collection.getCollectionNo(), 8)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					Calendar cal = Calendar.getInstance();
					MainActivity._printerConf.printText(
							FunctionHelper.concatWithSpace(
									FunctionHelper.ConvertDate(FunctionHelper.dbFormat, FunctionHelper.printFormat, _collection.getCollectionDate()),
									cal.getTime().getHours()+":"+cal.getTime().getMinutes()
							), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printDivider();
					MainActivity._printerConf.printText("Salesman: " + _salesmanProfile.getSalesmanName(), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText("Customer: " + _customerProfile.getCustomerName(),
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText("Address: " + _customerProfile.getAddress(),
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText("TIN: " + _customerProfile.getTin(),
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					if (_collection.getPaymentMethod().equals("Cash"))
					{
						MainActivity._printerConf.printText("Payment Method: " + _collection.getPaymentMethod(), 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
						MainActivity._printerConf.printNextLine();
					}
					else if (_collection.getPaymentMethod().equals("Check"))
					{
						MainActivity._printerConf.printText("Payment Method: " + _collection.getPaymentMethod(), 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
						MainActivity._printerConf.printNextLine();
						MainActivity._printerConf.printText("Bank: " + _collection.getBankName(), 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
						MainActivity._printerConf.printNextLine();
						MainActivity._printerConf.printText(FunctionHelper.concatWithSpace("Check No.:" + _collection.getCheckNo(), 
								"Check Date:" + FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.printFormat, _collection.getCheckDate())), 
								MainActivity._printerConf.aleft, 
								MainActivity._printerConf.fontC, 
								MainActivity._printerConf.sizeh1);
						MainActivity._printerConf.printNextLine();
					}
					MainActivity._printerConf.printDivider();
					
					// details printing
					for (CollectionDetails _collectionDetail : _collectionDetails)
					{
						if (_collectionDetail.getAppliedAmount()>0)
						{
							MainActivity._printerConf.printText(
									FunctionHelper.concatWithSpace(
											FunctionHelper.lpad(_collectionDetail.getDRNumber(), 8) + " " 
											+ _collectionDetail.getDRDate() + " (Amnt: " + _collectionDetail.getDRAmount() + ")",
											String.format("%.02f", _collectionDetail.getAppliedAmount())
									),
									MainActivity._printerConf.aleft, 
									MainActivity._printerConf.fontC, 
									MainActivity._printerConf.sizeh1);
							MainActivity._printerConf.printNextLine();
							MainActivity._printerConf.printText("   Bal: " + String.format("%.02f", _collectionDetail.getBalance()),
									MainActivity._printerConf.aleft, 
									MainActivity._printerConf.fontC, 
									MainActivity._printerConf.sizeh1);
							MainActivity._printerConf.printNextLine();
						}
					}
					
					MainActivity._printerConf.printDivider();
					/**MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("VATable",20), String.format("%.02f", _orderHeader.getOrderTotalPrice())), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("12% VAT", 20), String.format("%.02f", _orderHeader.getOrderTotalPrice() * .12)), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();**/
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Amount Due", 20), String.format("%.02f", Double.valueOf(_collectionViewHolder.total_applied_amount.getText().toString()))), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printDivider();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Amount Pay", 20), String.format("%.02f", _collection.getAmount())), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printText(FunctionHelper.concatWithSpace(FunctionHelper.indentString("Change", 20), String.format("%.02f", _collection.getAmount() - Double.valueOf(_collectionViewHolder.total_applied_amount.getText().toString()))), 
							MainActivity._printerConf.aleft, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					MainActivity._printerConf.printNextLine();
					MainActivity._printerConf.printLineFeed(2, false);
					
					// footer
					MainActivity._printerConf.printText("Thank you!", 
							MainActivity._printerConf.acenter, 
							MainActivity._printerConf.fontC, 
							MainActivity._printerConf.sizeh1);
					
					MainActivity._printerConf.printLineFeed(4, true);
					
					MainActivity._printerConf.disconnectPrinter();
				}
				else
					Toast.makeText(getApplicationContext(), "No printer found. Either bluetooth is off or printer is off.", Toast.LENGTH_SHORT).show();
			}
		}, 5000);
	}
	
	public static void UpdateTotalAmounts()
	{
		double totalAppliedAmount = 0.0;
		double totalBalance = 0.0;
		
		for (CollectionDetails cdet : _collectionDetails)
		{						
			totalAppliedAmount += Double.valueOf(cdet.getAppliedAmount());
			totalBalance += Double.valueOf(Double.valueOf(cdet.getDRAmount())-Double.valueOf(cdet.getAppliedAmount()));
		}

		DecimalFormat formatter = new DecimalFormat("#,###,###.##");
		
		_collectionViewHolder.total_applied_amount.setText(formatter.format(totalAppliedAmount));
		_collectionViewHolder.total_balance.setText(formatter.format(totalBalance));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.order_view, menu);
		return true;
	}
	
	public class CollectionViewHolder
	{
		TextView collection_no;
		TextView collection_date;
		TextView collection_customer_name;
		EditText collection_customer_id;
		
		TextView total_dr_amount;
		TextView total_applied_amount;
		TextView total_balance;
		
		Spinner payment_method; // added sept. 27, 2014
		EditText amount;
		Spinner bank;
		EditText check_no;
		Button check_date;
		
		LinearLayout bank_layout;
		LinearLayout check_no_layout;
		LinearLayout check_date_layout;
	}	
}
