package com.example.orderapp;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.classobj.ItemProfile;
import com.example.list.view.helper.ItemsListViewHelper;

public class ItemsListActivity extends Fragment {

	private ArrayList<ItemProfile> itemsList;
	
	private ItemsListViewHelper _itemAdapterHelper;
	private ListView _listView;
	
	private ViewGroup ct;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
		ct = container;
        View rootView = inflater.inflate(R.layout.activity_items_list, container, false);
        _listView = (ListView)rootView.findViewById(R.id.items_list_view);

        itemsList = new ArrayList<ItemProfile>();
		
		itemsList = MainActivity._deskpadDbHelper.getItems();
		
		if (itemsList != null)
		{		
			_itemAdapterHelper = new ItemsListViewHelper(getActivity(), itemsList);
			
			_listView.setAdapter(_itemAdapterHelper);
		}
		
        return rootView;
    }
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		if(_itemAdapterHelper != null){
			itemsList = new ArrayList<ItemProfile>();
			itemsList = MainActivity._deskpadDbHelper.getItems();

			_itemAdapterHelper = new ItemsListViewHelper(getActivity(), itemsList);
			
			_listView.setAdapter(_itemAdapterHelper);
		}
		
		super.onResume();
	}
}
