package com.example.orderapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.SearchManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.adapter.NavDrawerListAdapter;
import com.example.classobj.Banks;
import com.example.classobj.CustomerCollection;
import com.example.classobj.CustomerProfile;
import com.example.classobj.ItemProfile;
import com.example.classobj.ItemUoms;
import com.example.classobj.MenuDetails;
import com.example.classobj.OrderHeader;
import com.example.classobj.PaymentTerms;
import com.example.classobj.SalesmanProfile;
import com.example.classobj.TaxTypes;
import com.example.helper.DatabaseHelper;
import com.example.helper.DirectoryHelper;
import com.example.helper.FunctionHelper;
import com.example.helper.GeoLocationHelper;
import com.example.helper.NavDrawerItem;
import com.example.helper.PrinterConnectionHelper;
import com.example.helper.WIFIConnectionHelper;
import com.example.list.view.helper.MenuListAdapterHelper;
import com.example.list.view.helper.SalesmansListAdapterHelper;
import com.example.misc.AlertDialogManager;
import com.example.misc.FileDownloadHelper;
import com.example.misc.FileUploadHelper;
import com.example.misc.JSONParser;
import com.example.misc.ProgressDialogManager;

public class MainActivity extends FragmentActivity {
	private int REQUEST_ENABLE_BT = 600;
	public static int _latitude = 0;
	public static int _longitude = 0;
	
	private DirectoryHelper _dirHelper;
	public static DatabaseHelper _deskpadDbHelper;
   
    private WIFIConnectionHelper _wifiConf = new WIFIConnectionHelper();
    public static PrinterConnectionHelper _printerConf;
    
    private ProgressDialogManager pDialog = null;
	private Context parentContext = null;
	
	private Dialog loginDialog;
	private Dialog menuDialog;
	
	private EditText loginServer;
	private EditText logInUsername;
	private EditText logInPassword;
	private Button logInButton;
	
	private Menu _menu;
	public static long salesmanId = 0;
	
	public static MenuItem _action_new_customer;
	public static MenuItem _action_new_order;
	public static MenuItem _action_search;
    
    // nav items
    
	public static int selectedMenuIndx;
	
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
 
    // nav drawer title
    private CharSequence mDrawerTitle;
 
    // used to store app title
    private CharSequence mTitle;
 
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
 
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    
    // end
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        
		setContentView(R.layout.activity_main);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		/** creating directory **/
        _dirHelper = new DirectoryHelper();
        _dirHelper.CreateDirectory("/OrderApp");
        _dirHelper.CreateDirectory("/OrderApp/db");
        _dirHelper.CreateDirectory("/OrderApp/images"); // added
        _dirHelper.CreateDirectory("/OrderApp/reports"); // added
        
        /** creating database **/
        _deskpadDbHelper = new DatabaseHelper(this, "orderapp");
        _deskpadDbHelper.createTable();
        
		SalesmanProfile _salesmanProfile = _deskpadDbHelper.getSelectedSalesman();
		
		if (_salesmanProfile != null)
		{
			salesmanId = _salesmanProfile.getSalesmanId();
			
            getActionBar().setSubtitle(_salesmanProfile.getSalesmanName());
		}

       	InitializeList();
		
		// nav items
		
		mTitle = mDrawerTitle = getTitle();
		 
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
 
        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
 
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
 
        navDrawerItems = new ArrayList<NavDrawerItem>();
 
        // adding nav drawer items to array
        for (int i = 0; i < navMenuTitles.length; i++)
        {
        	navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons.getResourceId(i, -1)));	
        }         
 
        // Recycle the typed array
        navMenuIcons.recycle();
 
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
 
        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
 
        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
 
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }
 
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
 
        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
        }
		
		// end
        
        _printerConf = new PrinterConnectionHelper(this);
        		
       	this.registerReceiver(_wifiConf.myWifiReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)); // registering WIFI service
       	
       	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
   	    if (!mBluetoothAdapter.isEnabled()) {
   	    	mBluetoothAdapter.enable();
   	    }
   	    
		LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
   	    if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
   	    {
   	    	Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
   	    	startActivity(intent);
   	    }
   	    
   	    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new GeoLocationHelper());
	}
	
	// nav items
	
	/**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }
	
    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
    	boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        
        menu.findItem(R.id.action_sync).setVisible(false);
        
        menu.findItem(R.id.action_new_customer).setVisible(false);
        menu.findItem(R.id.action_new_order).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        
        if(!drawerOpen)
        {
	        switch (selectedMenuIndx) {
				case 1:
					menu.findItem(R.id.action_new_customer).setVisible(true);
					menu.findItem(R.id.action_search).setVisible(true);
					break;
				case 2:
					menu.findItem(R.id.action_new_order).setVisible(true);
					break;
				case 3:
					menu.findItem(R.id.action_search).setVisible(true);
					break;
				case 4:
					menu.findItem(R.id.action_sync).setVisible(true);
				default:
					break;
			}
        }

        return super.onPrepareOptionsMenu(menu);
    }
 
    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        selectedMenuIndx = position;
        switch (position) {
	        case 0:
	            fragment = new ItemsListActivity();
	            break;
	        case 1:
	            fragment = new CustomersListActivity();
	            break;
	        case 2:
	            fragment = new OrdersListActivity();
	            break;
	        case 3:
	            fragment = new CollectionsListActivity();
	            break;
	        case 4:
	        	fragment = new ReportsListActivity();
	        	break;
	 
	        default:
	            break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
 
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }
 
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
    
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
 
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
	// end

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		this._menu = menu;
		
		this._action_new_customer = menu.findItem(R.id.action_new_customer);
		this._action_new_order = menu.findItem(R.id.action_new_order);
		this._action_search = menu.findItem(R.id.action_search);
		
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
		
		return true;
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	// TODO Auto-generated method stub
    	
    	// nav items
    	
    	// toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
    	
    	// end
    	
    	switch (item.getItemId())
    	{
    		case R.id.action_sync:
    			LogingIn();
    			return true;
    		case R.id.action_new_customer:
    			Intent customerViewIntent = new Intent(MainActivity.this, CustomerFormActivity.class);
				startActivity(customerViewIntent);
    			return true;
    		case R.id.action_new_order:
    			Intent orderViewIntent = new Intent(MainActivity.this, OrderFormActivity.class);
				startActivity(orderViewIntent);
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
    
    public void LogingIn()
    {
		try
    	{
	    	Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
	    	int returnVal = p1.waitFor();
	    	boolean reachable = (returnVal==0);
	    	if (!reachable)
	    	{
	    		AlertDialogManager _alertDialog = new AlertDialogManager(this);
	    		
	    		_alertDialog.fail("Error", "No connection established. " +
	    	    				"Either no internet connection or the server is unreachable.");
	    		_alertDialog.show();
	    		
	    		return;
	    	}
    	}
    	catch(Exception ex)
    	{
    		
    	}
    	

    	pDialog = new ProgressDialogManager(this);
    	loginDialog = new Dialog(this);
		
    	loginDialog.setTitle("Login");
    	loginDialog.setContentView(R.layout.login_form);
    	
    	loginDialog.show();
    	
    	addTextFieldEventListener();
    	addButtonEventListener();
    }
    
    private void addTextFieldEventListener() {
    	logInUsername = (EditText) loginDialog.findViewById(R.id.username_fld);
    	logInPassword = (EditText) loginDialog.findViewById(R.id.password_fld);
    	loginServer = (EditText) loginDialog.findViewById(R.id.server_fld);
    	
    	loginServer.setOnKeyListener(new OnKeyListener() {
    		
    		public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
    			if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
    				checkLogin();
    				return true;
    			}
    			
    			return false;
    		}
    		
    	});
    	
    	logInUsername.setOnKeyListener(new OnKeyListener() {
    		
    		public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
    			if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
    				checkLogin();
    				return true;
    			}
    			
    			return false;
    		}
    		
    	});
    	
    	logInPassword.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
				if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
    				checkLogin();
    				return true;
    			}
    			
    			return false;
			}
		});
    	
    }
    
    public void addButtonEventListener() {
    	
    	logInButton = (Button) loginDialog.findViewById(R.id.login_btn);
    	
    	logInButton.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				checkLogin();
			}
    		
    	});
    }
    
    private void checkLogin() {
    	
    	AlertDialogManager alertDialog = new AlertDialogManager(this);
    	
    	if(logInUsername.getText().length() == 0 || logInPassword.getText().length() == 0
    			|| loginServer.getText().length() == 0) {
	    	alertDialog.fail("Error", "Server, username and password fields must not be empty.");
	    	alertDialog.show();
		} else {
			try
	    	{
		    	Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 " + loginServer.getText().toString());
		    	int returnVal = p1.waitFor();
		    	boolean reachable = (returnVal==0);
		    	if (!reachable)
		    	{
		    		AlertDialogManager _alertDialog = new AlertDialogManager(this);
		    		
		    		_alertDialog.fail("Error", "No connection established to " + loginServer.getText().toString() +
		    				".The server is unreachable.");
		    		_alertDialog.show();
		    		
		    		return;
		    	}
	    	}
	    	catch(Exception ex)
	    	{
	    		ex.printStackTrace();
	    	}            
			FunctionHelper.ipaddr = loginServer.getText().toString();
			FunctionHelper.url = "http://" + FunctionHelper.ipaddr + "/app/api.cf";
			FunctionHelper.urlnologin = "http://" + FunctionHelper.ipaddr + "/app/apinologin.cf";
		    
		    new LoggingIn().execute(logInUsername.getText().toString(), logInPassword.getText().toString());
		}
    }
    
    class LoggingIn extends AsyncTask<String, Void, String> {

		AlertDialogManager alertDialog = new AlertDialogManager(MainActivity.this);
    			
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			pDialog.create("Logging In. Please wait...");
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			FunctionHelper.params.clear();
			
			FunctionHelper.params.add(new BasicNameValuePair("cookie_user", args[0]));
			FunctionHelper.params.add(new BasicNameValuePair("cookie_pass", args[1]));
			FunctionHelper.params.add(new BasicNameValuePair("op", "i"));
			
			JSONParser jsonParser = new JSONParser();
			String jsonString = jsonParser.makeHttpRequest(FunctionHelper.url, FunctionHelper.params);
			
			Log.d("Create Response:", jsonString);
			
			return jsonString;
		}

		protected void onPostExecute(String result) {	
			pDialog.dismiss();
			
			try {				
				final JSONObject jsonObj = new JSONObject(result);
				if (jsonObj.getString("session").equals("invalid user/password"))
				{
		    		alertDialog.fail("Error", "Username or password is invalid.");
			    	alertDialog.show();
				}
				else if (jsonObj.getString("session").equals("expired"))
				{
		    		alertDialog.fail("Error", "Session expired.");
			    	alertDialog.show();
				}
				else
				{
					FunctionHelper.session = jsonObj.getString("session");
					Toast.makeText(MainActivity.this, "Success Login", Toast.LENGTH_LONG).show();
					
					loginDialog.dismiss();
					
					GetSalesman(jsonObj);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

    class LoggingOut extends AsyncTask<Void, Void, String> {
    			
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog.create("Logging Out. Please wait...");
			pDialog.show();
		}

		protected String doInBackground(Void... args) {
			FunctionHelper.params.clear();
			
			FunctionHelper.params.add(new BasicNameValuePair("tpl", "/qcgi/login")); // to be observed
			FunctionHelper.params.add(new BasicNameValuePair("step", "1"));
			FunctionHelper.params.add(new BasicNameValuePair("op", "l"));
			
			JSONParser jsonParser = new JSONParser();
			String jsonString = jsonParser.makeHttpRequest(FunctionHelper.url, FunctionHelper.params);
			
			Log.d("Create Response:", jsonString);
			
			return jsonString;
		}

		protected void onPostExecute(String result) {
			pDialog.dismiss();
			
			menuDialog.dismiss();
			
			InitializeList();
			
			displayView(0);
		}
	}
    
    private void GetSalesman(JSONObject jsonObj)
    {
		// salesman listing				
		try {
			JSONArray salesmanArray = null;
	    	final ArrayList<SalesmanProfile> _salesmanList = new ArrayList<SalesmanProfile>();
	    	
			salesmanArray = new JSONArray(jsonObj.getString("salesmans"));
		
			for (int i = 0; i < salesmanArray.length(); i++)
			{
				if (salesmanArray.get(i).toString().equals("null")) continue;
				
				JSONObject salesmanField = salesmanArray.getJSONObject(i);
				
				SalesmanProfile _salesmanProfile = new SalesmanProfile(salesmanField.getLong("id"),
						salesmanField.getString("name"));
				
				MainActivity._deskpadDbHelper.addNewSalesmans(_salesmanProfile);
				
				_salesmanList.add(_salesmanProfile);
			}
			
			final Dialog salesmanDialog = new Dialog(MainActivity.this);
			salesmanDialog.setTitle("Salesman");
			salesmanDialog.setContentView(R.layout.login_salesman_list);
			ListView _listView = (ListView) salesmanDialog.findViewById(R.id.salesman_list_view);
			
			SalesmansListAdapterHelper _salesmanAdapterHelper = new SalesmansListAdapterHelper(MainActivity.this, _salesmanList);
			_listView.setAdapter(_salesmanAdapterHelper);
						    	
			salesmanDialog.show();
			
			_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapter, View view, int position, long arg)   {
	
				    // TODO Auto-generated method stub
					SalesmanProfile _salesmanProfile = (SalesmanProfile)_salesmanList.get(position);
					salesmanId = _salesmanProfile.getSalesmanId();
					
					MainActivity._deskpadDbHelper.updateSelectedSalesman(_salesmanProfile.getSalesmanId());

					getActionBar().setSubtitle(_salesmanProfile.getSalesmanName());
					
					salesmanDialog.dismiss();
					
					// menu
					menuDialog = new Dialog(MainActivity.this);
					final ArrayList<MenuDetails> _menuList = new ArrayList<MenuDetails>() {{
						add(new MenuDetails(0, "Sync data from server to tablet"));
						add(new MenuDetails(1, "Sync data from tablet to server"));
						add(new MenuDetails(2,"Log Out"));
					}};
					
					menuDialog.setTitle("Menu");
					menuDialog.setContentView(R.layout.login_menu_list);
					ListView _listView = (ListView) menuDialog.findViewById(R.id.menu_list_view);
					
					MenuListAdapterHelper _menuAdapterHelper = new MenuListAdapterHelper(MainActivity.this, _menuList);
					_listView.setAdapter(_menuAdapterHelper);
					
					menuDialog.setCancelable(false);
			    	
					menuDialog.show();
					
					_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> adapter, View view, int position, long arg)   {
			
						    // TODO Auto-generated method stub
							MenuDetails _menuDetail = (MenuDetails)_menuList.get(position);
							
							switch(_menuDetail.getMenuIndx())
							{
								case 0:
									// sync data from server to tablet
									AlertDialogManager aD = new AlertDialogManager(MainActivity.this);
									//AlertDialog.Builder aD = new AlertDialog.Builder(MainActivity.this);
									aD.confirm("Confirmation", "Do you want to continue syncing data? All data will be deleted and replaced.",
										new DialogInterface.OnClickListener() {
									
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												MainActivity._deskpadDbHelper.dropTable();
												_dirHelper.CleanDirectory("/OrderApp/images");
												_dirHelper.CleanDirectory("/OrderApp/reports");
												
												new SyncData().execute("getItems");
												new SyncData().execute("getCollections");
										    	new SyncData().execute("getPaymentTerms");
										    	new SyncData().execute("getUOM");
										    	new SyncData().execute("getBanks");
										    	new SyncData().execute("getTaxTypes");
										    	new SyncData().execute("getCustomers");
												
												dialog.cancel();
											}
										}, 
										new DialogInterface.OnClickListener() {
											
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												dialog.cancel();
											}
										});
									
									aD.show();
									
									break;
								case 1:
									// sync data from tablet to server
									ArrayList<CustomerProfile> _customerProfile = 
										MainActivity._deskpadDbHelper.getSelectedCustomers("customer_id in (" + 
												MainActivity._deskpadDbHelper.getCustomersUpdated() + ")");
									new UploadCustomersToServer().execute(_customerProfile);
									
									ArrayList<OrderHeader> _orderHeaders = MainActivity._deskpadDbHelper.getOrders();
									new UploadOrdersToServer().execute(_orderHeaders);

									break;
								case 2:
									new LoggingOut().execute();
									break;
								default:
									break;
							}
					    	
					    	//menuDialog.dismiss();
						}
					});
				}
			});
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public class UploadOrdersToServer extends AsyncTask<ArrayList<OrderHeader>, Void, String> {
    	@Override
    	protected void onPreExecute() {
    		super.onPreExecute();
			pDialog.create("Syncing orders data to server. Please wait...");
			pDialog.show();
    	}
        	
    	protected void onPostExecute(String result) {
    		pDialog.dismiss();
    	}

		@Override
		protected String doInBackground(ArrayList<OrderHeader>... args) {
			// TODO Auto-generated method stub
			ArrayList<OrderHeader> _orderHeaders = args[0];			

    		String jsonString = "{\"orders\":[";
    		
    		for (int indx = 0; indx < _orderHeaders.size(); indx++)
    		{
    			OrderHeader _orderHeader = _orderHeaders.get(indx);
    			if (_orderHeader.getStatus().equals("approve"))
    			{
    				String[] items = _orderHeader.getOrderItems().split(",");
    				String[] discounts = _orderHeader.getOrderItemsDesc().split(",");
    				String[] quantities = _orderHeader.getOrderItemsQty().split(",");
    				String itemsObj = "[";
    				long consignment_no = 0;
    				
    				for (int i = 0; i < items.length; i++)
    				{
    					itemsObj += "{\"item_id\":\"" + items[i] + "\", \"item_qty\":\"" + quantities[i] + "\", "
    	    					+ "\"item_disc\":\"" + discounts[i] + "\"},";
    					
    					if (consignment_no == 0)
    					{
    						ItemProfile _itemProfile = MainActivity._deskpadDbHelper.getItem(Long.valueOf(items[i]));
    						consignment_no = _itemProfile.getConsignmentNo();
    					}
    				}
    				
    				itemsObj = itemsObj.replaceAll(",$", "") + "]";
    				
	    			jsonString += "{\"consignment_no\":\"" + consignment_no + "\", \"order_no\":\"" + _orderHeader.getOrderNo() + "\", \"salesman_id\":\"" + salesmanId + "\", "
	    					+ "\"customer_id\":\"" + _orderHeader.getCustomerId() + "\", \"order_date\":\"" + 
	    					FunctionHelper.ConvertDate(FunctionHelper.fldFormat, FunctionHelper.dbFormat, _orderHeader.getOrderDate()) + "\", "
	    					+ "\"items\":" + itemsObj + ", \"payment_terms\":\"" + _orderHeader.getPaymentTermsId() + "\", "
	    					+ "\"order_total_price\":\"" + _orderHeader.getOrderTotalPrice() + "\"},";
    			}
    		}
    		
    		jsonString = jsonString.replaceAll(",$", "") + "]}";
    		
    		if (_orderHeaders.size() > 0)
    		{
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.clear();
				
				params.add(new BasicNameValuePair("tpl", "save"));
				params.add(new BasicNameValuePair("tph", "saveOrder"));
				params.add(new BasicNameValuePair("json_data", jsonString));    		
							
				JSONParser jsonParser = new JSONParser();
				jsonParser.makeHttpRequest(FunctionHelper.url + "/" + FunctionHelper.session, params);
    		}
			
			return null;
		}
    }
    
    public class UploadCustomersToServer extends AsyncTask<ArrayList<CustomerProfile>, Void, String> {
    	@Override
    	protected void onPreExecute() {
    		super.onPreExecute();
			pDialog.create("Syncing customers data to server. Please wait...");
			pDialog.show();
    	}
        	
    	protected void onPostExecute(String result) {
    		pDialog.dismiss();
    	}

		@Override
		protected String doInBackground(ArrayList<CustomerProfile>... args) {
			// TODO Auto-generated method stub
			ArrayList<CustomerProfile> _customerProfiles = args[0];			

    		String jsonString = "{\"customers\":[";
    		
    		for (int indx = 0; indx < _customerProfiles.size(); indx++)
    		{
    			CustomerProfile _customerProfile = _customerProfiles.get(indx);
    			    				
    			jsonString += "{\"partner_id\":\"" + _customerProfile.getCustomerId() + "\", \"name\":\"" + _customerProfile.getCustomerName() + "\", "
    					+ "\"address\":\"" + _customerProfile.getAddress() + "\", \"tel_line_no\":\"" + _customerProfile.getContactNo() + "\", "
    					+ "\"lat\":\"" + _customerProfile.getGeoLatitude() + "\", \"long\":\"" + _customerProfile.getGeoLongitude() + "\", "
    					+ "\"prof_image\":\"" + _customerProfile.getProfImagePath() + "\", \"comp_image\":\"" + _customerProfile.getCompImagePath() + "\", "
    					+ "\"route_days\":\"" + _customerProfile.getRouteDays() + "\", \"tin\":\"" + _customerProfile.getTin() + "\", "
    					+ "\"term\":\"" + _customerProfile.getTerm() + "\", \"tax_type\":\"" + _customerProfile.getTaxType() + "\"},"; // modified
    		}
    		
    		jsonString = jsonString.replaceAll(",$", "") + "]}";
    		
    		if (_customerProfiles.size() > 0)
    		{
	    		List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.clear();
				
				params.add(new BasicNameValuePair("tpl", "save"));
				params.add(new BasicNameValuePair("tph", "saveCustomer"));
				params.add(new BasicNameValuePair("json_data", jsonString));
				params.add(new BasicNameValuePair("salesman", String.valueOf(salesmanId)));
							
				JSONParser jsonParser = new JSONParser();
				jsonParser.makeHttpRequest(FunctionHelper.url + "/" + FunctionHelper.session, params);
				
				for (CustomerProfile _customerProfile : _customerProfiles)
				{
					new FileUploadHelper().uploadFile(_customerProfile.getProfImagePath());
					new FileUploadHelper().uploadFile(_customerProfile.getCompImagePath()); // added
				}
    		}
			
			return null;
		}
    }
    
    public class SyncData extends AsyncTask<String, Void, String> {
		
    	private String _type = "";
    			
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog.create("Syncing data from server. Please wait...");
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.clear();
			
			params.add(new BasicNameValuePair("tpl", "get"));
			params.add(new BasicNameValuePair("tph", args[0]));	
			params.add(new BasicNameValuePair("salesman", String.valueOf(salesmanId)));
			
			JSONParser jsonParser = new JSONParser();
			String jsonString = jsonParser.makeHttpRequest(FunctionHelper.url + "/" + FunctionHelper.session, params);
			
			Log.d("Create Response:", jsonString);
			
			if (args[0].equals("getItems"))
				_type = "items";
			else if (args[0].equals("getCustomers"))
				_type = "customers";
			else if (args[0].equals("getPaymentTerms"))
				_type = "paymentterms";
			else if (args[0].equals("getCollections"))
				_type = "collections";
			else if (args[0].equals("getUOM"))
				_type = "uoms";
			else if (args[0].equals("getBanks"))
				_type = "banks";
			else if (args[0].equals("getTaxTypes"))
				_type = "tax_types";
			
			return jsonString;
		}

		protected void onPostExecute(String result) {
						
			JSONArray moduleArray = null;
			
			try {	
				JSONObject jsonObj = new JSONObject(result);
				moduleArray = jsonObj.getJSONArray(_type);
				
				for (int i = 0; i < moduleArray.length(); i++)
				{
					JSONObject moduleField = moduleArray.getJSONObject(i);
					
					if (_type.equals("items"))
					{
						ItemProfile _itemProfile = new ItemProfile(moduleField.getLong("id"), moduleField.getString("sku"), 
								moduleField.getString("description"), moduleField.getDouble("price"), moduleField.getDouble("qty"), 
								moduleField.getLong("consignment_no"), moduleField.getInt("uom"));
						MainActivity._deskpadDbHelper.addNewItem(_itemProfile);
					}
					else if (_type.equals("customers"))
					{
						CustomerProfile _customerProfile = new CustomerProfile(moduleField.getLong("customer_id"), moduleField.getString("customer_name"),
								moduleField.getString("address"), moduleField.getString("contact_no"), 
								moduleField.getString("prof_image_path"), moduleField.getDouble("geo_latitude"), moduleField.getDouble("geo_longitude"),
								moduleField.getInt("route_days"), moduleField.getString("comp_image_path"), moduleField.getString("tin"),
								moduleField.getInt("term"), moduleField.getInt("tax_type")); // modified - check
						MainActivity._deskpadDbHelper.addNewCustomer(_customerProfile);
						
						// modified and added
						if (!_customerProfile.getProfImagePath().isEmpty())
						{
							new FileDownloadHelper().downloadFile(_customerProfile.getProfImagePath());
						}
						if (!_customerProfile.getCompImagePath().isEmpty())
						{
							new FileDownloadHelper().downloadFile(_customerProfile.getCompImagePath());
						}
						// end
					}
					else if (_type.equals("paymentterms"))
					{
						PaymentTerms _paymentTerms = new PaymentTerms(moduleField.getString("payment_name"), 
								moduleField.getLong("payment_id"), moduleField.getInt("payment_days"), moduleField.getInt("payment_days_allowable"));
						MainActivity._deskpadDbHelper.addNewPaymentTerms(_paymentTerms);
					}
					else if (_type.equals("collections"))
					{
						CustomerCollection _customerCollection = new CustomerCollection(moduleField.getLong("customer_id"), 
								moduleField.getString("ref_dates"), moduleField.getString("ref_amounts"), 
								moduleField.getString("ref_numbers"), moduleField.getDouble("total_collection"));
						
						String appliedAmnt = ""; 
						for (int o=0; o<moduleField.getString("ref_numbers").split(",").length; o++)
							appliedAmnt += "0.0,";
						
						_customerCollection.setAppliedAmount(appliedAmnt.replaceAll(",$", ""));
						_customerCollection.setStatus("new");
						
						String collection_date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
						_customerCollection.setCollectionDate(collection_date);
						
						MainActivity._deskpadDbHelper.addNewCollection(_customerCollection);
					}
					else if (_type.equals("uoms"))
					{
						ItemUoms _itemUOMs = new ItemUoms(moduleField.getString("uom_name"), moduleField.getString("uom_short"), 
								moduleField.getLong("uom_id"));
						MainActivity._deskpadDbHelper.addNewUOMs(_itemUOMs);
					}
					else if (_type.equals("tax_types"))
					{
						TaxTypes _taxTypes = new TaxTypes(moduleField.getString("tax_type_name"), moduleField.getDouble("tax_type_rate"), 
								moduleField.getLong("tax_type_id"));
						MainActivity._deskpadDbHelper.addNewTaxTypes(_taxTypes);
					}
					else if (_type.equals("banks"))
					{
						Banks _banks = new Banks(moduleField.getString("bank_name"), moduleField.getLong("bank_id"),
								moduleField.getString("bank_code"));
						MainActivity._deskpadDbHelper.addNewBanks(_banks);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			if (_type.equals("customers"))
			{
				pDialog.dismiss();
			}
			
		}
	}
    
    private void InitializeList()
    {
    	if (FunctionHelper.customerList != null && FunctionHelper.customerList.size() > 0)
        	FunctionHelper.customerList.clear();
	
        if (FunctionHelper.paymentTermsList != null && FunctionHelper.paymentTermsList.size() > 0)
        	FunctionHelper.paymentTermsList.clear();
		
		if (FunctionHelper.itemsList != null && FunctionHelper.itemsList.size() > 0)
			FunctionHelper.itemsList.clear();
		
		if (FunctionHelper.uomsList != null && FunctionHelper.uomsList.size() > 0)
			FunctionHelper.uomsList.clear();
		
		if (FunctionHelper.taxTypesList != null && FunctionHelper.taxTypesList.size() > 0)
			FunctionHelper.taxTypesList.clear();
		
		// added sept. 27, 2014
		if (FunctionHelper.banksList != null && FunctionHelper.banksList.size() > 0)
			FunctionHelper.banksList.clear();
		
		List<Banks> _banksList = _deskpadDbHelper.getBanks();
		FunctionHelper.banksList.addAll(_banksList);
		
		List<CustomerProfile> _customerProfile = _deskpadDbHelper.getCustomers();
    	FunctionHelper.customerList.addAll(_customerProfile);
    
    	List<PaymentTerms> _paymentTerms = _deskpadDbHelper.getPaymentTerms();
    	FunctionHelper.paymentTermsList.addAll(_paymentTerms);
    	
		List<ItemProfile> _itemsProfile = _deskpadDbHelper.getItems();
		FunctionHelper.itemsList.addAll(_itemsProfile);
		
		List<ItemUoms> _itemUOMs = _deskpadDbHelper.getUOMs();
		FunctionHelper.uomsList.addAll(_itemUOMs);
		
		List<TaxTypes> _taxTypes = _deskpadDbHelper.getTaxTypes();
		FunctionHelper.taxTypesList.addAll(_taxTypes);
    }
}
