package com.example.orderapp;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.classobj.MenuDetails;
import com.example.helper.PDFCreateHelper;
import com.example.list.view.helper.MenuListAdapterHelper;

public class ReportsListActivity extends Fragment implements ListView.OnItemClickListener {

	private ListView _listView;
		
	private ArrayList<MenuDetails> _menuList = null;
	private MenuListAdapterHelper _menuAdapterHelper = null;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
        View rootView = inflater.inflate(R.layout.activity_reports_list, container, false);
        _listView = (ListView)rootView.findViewById(R.id.reports_list_view);

        _menuList = new ArrayList<MenuDetails>() {{
			add(new MenuDetails(0, "Cash Sales Summary"));
			add(new MenuDetails(1,"Credit Sales Summary"));
			add(new MenuDetails(2,"Product Sold Summary"));
			add(new MenuDetails(3,"Cash Collection Summary"));
			add(new MenuDetails(4,"Product Remaining Quantity"));
		}};
		
		_menuAdapterHelper = new MenuListAdapterHelper(getActivity(), _menuList);
		_listView.setAdapter(_menuAdapterHelper);
        _listView.setOnItemClickListener(this);
		
        return rootView;
    }
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		_menuAdapterHelper = new MenuListAdapterHelper(getActivity(), _menuList);
		_listView.setAdapter(_menuAdapterHelper);
        _listView.setOnItemClickListener(this);
		
		super.onResume();
	}
	
	@Override
    public void onItemClick(AdapterView<?> ad, View v, int position, long id) {
        // TODO Auto-generated method stub
		MenuDetails _menuDetail = (MenuDetails)_menuList.get(position);
		
		switch(_menuDetail.getMenuIndx())
		{
			case 0:
				GenerateCashSales();
				break;
			case 1:
				GenerateCreditSales();
				break;
			case 2:
				GenerateProductSold();
				break;
			case 3:
				GenerateCashCollection();
				break;
			case 4:
				GenerateProductInventory();
				break;
			default:
				break;
		}
    }
	
    private void GenerateCashSales()
    {
    	// menu
		final ArrayList<String[]> _cashSalesList = MainActivity._deskpadDbHelper.getReports("select a.order_no, " +
					"b.customer_name, a.order_total_price, "+
					"a.payment_method, a.check_no, a.check_date, c.name as bank" +
					" from order_header a left join customers b on a.customer_id=b.customer_id" +
					" left join banks c on a.bank=c.id" +
					" where a.payment_terms=1 and a.amount=0 and a.salesman_id=" + MainActivity.salesmanId);
		final ArrayList<String[]> _partialSalesList = MainActivity._deskpadDbHelper.getReports("select a.order_no, " +
				"b.customer_name, a.amount as order_total_price, " +
				"a.payment_method, a.check_no, a.check_date, c.name as bank" +
				" from order_header a left join customers b on a.customer_id=b.customer_id" +
				" left join banks c on a.bank=c.id" +
				" where a.amount > 0 and a.salesman_id=" + MainActivity.salesmanId);
		
		_cashSalesList.addAll(_partialSalesList);
		
		new PDFCreateHelper().createPDF("CashSalesSummary.pdf", getActivity(), _cashSalesList);
		
		File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/OrderApp/reports/CashSalesSummary.pdf");
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		startActivity(intent);
    }
    
    private void GenerateCreditSales()
    {
    	// menu
		final ArrayList<String[]> _creditSalesList = MainActivity._deskpadDbHelper.getReports("select a.order_no, " +
				"c.name, b.customer_name, a.order_total_price -a.amount as order_total_price" +
				" from order_header a left join customers b on a.customer_id=b.customer_id" +
				" left join payment_terms c on a.payment_terms=c.id" +
				" where a.payment_terms!=1 and a.salesman_id=" + MainActivity.salesmanId);
		
		new PDFCreateHelper().createPDF("CreditSalesSummary.pdf", getActivity(), _creditSalesList);

		File file = new File(Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/OrderApp/reports/CreditSalesSummary.pdf");
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		startActivity(intent);
    }
    
    private void GenerateProductSold()
    {
    	// menu
    	final ArrayList<String[]> _itemsList = MainActivity._deskpadDbHelper.getReports("select item_id, description, price from " +
    			"items where salesman_id="+MainActivity.salesmanId);
    	ArrayList<String[]> _ordersList = MainActivity._deskpadDbHelper.getReports("select order_no, order_items_discount, " +
				"order_items_qty, order_items from order_header where salesman_id="+MainActivity.salesmanId+" and status='approve'");
    	final ArrayList<String[]> _prodSoldList = new ArrayList<String[]>();
    	
    	for (String[] s : _itemsList)
    	{
    		//Toast.makeText(getActivity(), s[0]+" "+s[1]+" "+s[2], Toast.LENGTH_LONG).show();
    		
    		ArrayList<String> orderNos = new ArrayList<String>();
    		ArrayList<String> qtys = new ArrayList<String>();
    		ArrayList<String> sprices = new ArrayList<String>();
    		ArrayList<String> discounts = new ArrayList<String>();
    		ArrayList<String> stotals = new ArrayList<String>();
    		
    		double subTotal = 0.0;
    		
    		for (String[] s1 : _ordersList)
    		{
    			
    			for (int i = 0; i < s1[3].split(",").length; i++)
    			{
    				if (Long.valueOf(s[0]) == Long.valueOf(s1[3].split(",")[i]))
    				{
    					//Toast.makeText(getActivity(), s1[0]+" "+s1[1]+" "+s1[2]+" "+s1[3], Toast.LENGTH_LONG).show();
    					
    					orderNos.add(s1[0]);
    					qtys.add(String.format("%.2f", Double.valueOf(s1[2].split(",")[i])));
    					sprices.add(String.format("%.2f", Double.valueOf(s[2])));
    					discounts.add(String.format("%.2f", Double.valueOf(s1[1].split(",")[i])));
    					stotals.add(String.format("%.2f", (Double.valueOf(s[2]) - Double.valueOf(s1[1].split(",")[i])) * Double.valueOf(s1[2].split(",")[i])));
    					
    					subTotal += (Double.valueOf(s[2]) - Double.valueOf(s1[1].split(",")[i])) * Double.valueOf(s1[2].split(",")[i]);
    					
    					break;
    				}
    			}
    		}
    		
    		_prodSoldList.add(new String[]{s[1], new DecimalFormat("#,###,###.##").format(subTotal), 
    				orderNos.toString().replace("[", "").replace("]", "").replace(" ", ""),
    				qtys.toString().replace("[", "").replace("]", "").replace(" ", ""), 
    				sprices.toString().replace("[", "").replace("]", "").replace(" ", ""), 
    				discounts.toString().replace("[", "").replace("]", "").replace(" ", ""), 
    				stotals.toString().replace("[", "").replace("]", "").replace(" ", "")});
    	}
		
		new PDFCreateHelper().createPDF("ProductSoldSummary.pdf", getActivity(), _prodSoldList);
		
		File file = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/OrderApp/reports/ProductSoldSummary.pdf");
		Intent intent = new Intent(Intent.ACTION_VIEW);
  		intent.setDataAndType(Uri.fromFile(file), "application/pdf");
  		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
  		startActivity(intent);
    }
    
    private void GenerateCashCollection()
    {
    	// menu		
		final ArrayList<String[]> _cashCollectionList = MainActivity._deskpadDbHelper.getReports("select a.collection_no, " +
						"b.customer_name, a.applied_amount" +
						" from collections a left join customers b on a.customer_id=b.customer_id" +
						" where status='approve' and a.salesman_id=" + MainActivity.salesmanId);
				
		new PDFCreateHelper().createPDF("CashCollectionSummary.pdf", getActivity(), _cashCollectionList);

		File file = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/OrderApp/reports/CashCollectionSummary.pdf");
		Intent intent = new Intent(Intent.ACTION_VIEW);
  		intent.setDataAndType(Uri.fromFile(file), "application/pdf");
  		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
  		startActivity(intent);
    }
    
    private void GenerateProductInventory()
    {
    	// menu		
		final ArrayList<String[]> _productInventoryList = MainActivity._deskpadDbHelper.getReports("select description, qty " +
				"from items where salesman_id=" + MainActivity.salesmanId);
		
		new PDFCreateHelper().createPDF("ProductRemainingQuantity.pdf", getActivity(), _productInventoryList);

		File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/OrderApp/reports/ProductRemainingQuantity.pdf");
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		startActivity(intent);
    }
}
