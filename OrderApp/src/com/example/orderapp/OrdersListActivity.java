package com.example.orderapp;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.classobj.OrderHeader;
import com.example.list.view.helper.OrdersListAdapterHelper;

public class OrdersListActivity extends Fragment implements ListView.OnItemClickListener {

	private ArrayList<OrderHeader> ordersList;
	
	private OrdersListAdapterHelper _orderAdapterHelper;
	private ListView _listView;
	//private Button _button;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_orders_list, container, false);
        _listView = (ListView)rootView.findViewById(R.id.orders_list_view);
        //_button = (Button)rootView.findViewById(R.id.add_order);
		
		ordersList = new ArrayList<OrderHeader>();
		
		ordersList = MainActivity._deskpadDbHelper.getOrders();
		
		if (ordersList != null)
		{
			_orderAdapterHelper = new OrdersListAdapterHelper(getActivity(), ordersList);
			
			_listView.setAdapter(_orderAdapterHelper);
			
			_listView.setOnItemClickListener(this);
			/**_button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent orderViewIntent = new Intent(getActivity(), OrderFormActivity.class);
					
					startActivity(orderViewIntent);
				}
			});**/
		}
		
        return rootView;
	}

	@Override
	public void onResume() {
		if(_orderAdapterHelper != null){
			ordersList = new ArrayList<OrderHeader>();
			ordersList = MainActivity._deskpadDbHelper.getOrders();

			_orderAdapterHelper = new OrdersListAdapterHelper(getActivity(), ordersList);
			
			_listView.setAdapter(_orderAdapterHelper);
		}
		super.onResume();
	}
	
	@Override
    public void onItemClick(AdapterView<?> ad, View v, int position, long id) {
        // TODO Auto-generated method stub
		OrderHeader _orderHeader = (OrderHeader)ordersList.get(position);
		
		Intent orderViewIntent = new Intent(getActivity(), OrderFormActivity.class);
		orderViewIntent.putExtra("order_no", _orderHeader.getOrderNo());
				
		startActivity(orderViewIntent);
    }
}
