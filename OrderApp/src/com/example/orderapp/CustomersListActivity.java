package com.example.orderapp;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.classobj.CustomerProfile;
import com.example.list.view.helper.CustomersListViewHelper;

public class CustomersListActivity extends Fragment implements ListView.OnItemClickListener {

	private ArrayList<CustomerProfile> customersList;
	
	private CustomersListViewHelper _customerAdapterHelper;
	private ListView _listView;
	//private Button _button;
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
        View rootView = inflater.inflate(R.layout.activity_customers_list, container, false);
        _listView = (ListView)rootView.findViewById(R.id.customers_list_view);
        //_button = (Button)rootView.findViewById(R.id.add_customer);

        customersList = new ArrayList<CustomerProfile>();
		
        //customersList = MainActivity._deskpadDbHelper.getCustomers();
        customersList = MainActivity._deskpadDbHelper.getSelectedCustomers("route_days = " + (int)Math.pow(2, Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1));
		
		if (customersList != null)
		{		
			_customerAdapterHelper = new CustomersListViewHelper(getActivity(), customersList);
			
			_listView.setAdapter(_customerAdapterHelper);
			_listView.setOnItemClickListener(this);
			
			/**_button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent customerViewIntent = new Intent(getActivity(), CustomerFormActivity.class);
							
					startActivity(customerViewIntent);
				}
			});**/
		}
		
        return rootView;
    }
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		if(_customerAdapterHelper != null){
			customersList = new ArrayList<CustomerProfile>();
			//customersList = MainActivity._deskpadDbHelper.getCustomers();
			customersList = MainActivity._deskpadDbHelper.getSelectedCustomers("route_days = " + (int)Math.pow(2, Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1));

			_customerAdapterHelper = new CustomersListViewHelper(getActivity(), customersList);
			
			_listView.setAdapter(_customerAdapterHelper);
		}
		
		super.onResume();
	}
	
	@Override
    public void onItemClick(AdapterView<?> ad, View v, int position, long id) {
        // TODO Auto-generated method stub
		CustomerProfile _customerProfile = (CustomerProfile)customersList.get(position);

		Intent customerViewIntent = new Intent(getActivity(), CustomerFormActivity.class);
		customerViewIntent.putExtra("customer_id", _customerProfile.getCustomerId());
				
		startActivity(customerViewIntent);
    }
}
