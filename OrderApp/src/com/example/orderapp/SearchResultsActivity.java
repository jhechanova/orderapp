package com.example.orderapp;

import java.util.ArrayList;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.classobj.CustomerCollection;
import com.example.classobj.CustomerProfile;
import com.example.list.view.helper.CollectionSearchResultsListViewHelper;
import com.example.list.view.helper.CollectionsListAdapterHelper;
import com.example.list.view.helper.CustomersListViewHelper;
import com.example.list.view.helper.CustomersSearchResultsListViewHelper;

public class SearchResultsActivity extends Activity implements ListView.OnItemClickListener {

	private ArrayList<CustomerProfile> customersList;
	private ArrayList<CustomerCollection> collectionsList;
	
	private CustomersSearchResultsListViewHelper _customerAdapterHelper;
	private CollectionSearchResultsListViewHelper _collectionsAdapterHelper;
	private ListView _listView;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        
        _listView = (ListView) findViewById(R.id.search_results_list_view);
        //_button = (Button)rootView.findViewById(R.id.add_customer);

        customersList = new ArrayList<CustomerProfile>();
        collectionsList = new ArrayList<CustomerCollection>();
		
        handleIntent(getIntent());
    }
	
	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		setIntent(intent);
		handleIntent(intent);
	}
	
	private void handleIntent(Intent intent)
	{
		if (Intent.ACTION_SEARCH.equals(intent.getAction()))
		{
			String query = intent.getStringExtra(SearchManager.QUERY);
			
			switch(MainActivity.selectedMenuIndx)
			{
				case 1:
					customersList = MainActivity._deskpadDbHelper.getSelectedCustomers("customer_name LIKE '%" + query + "%'");
					
					if (customersList != null)
					{		
						_customerAdapterHelper = new CustomersSearchResultsListViewHelper(SearchResultsActivity.this, customersList);
						
						_listView.setAdapter(_customerAdapterHelper);
						_listView.setOnItemClickListener(this);
					}
					break;
				case 3:
					collectionsList = MainActivity._deskpadDbHelper.getSelectedCollections("b.customer_name LIKE '%" + query +"%'");
					
					if (collectionsList != null)
					{
						_collectionsAdapterHelper = new CollectionSearchResultsListViewHelper(SearchResultsActivity.this, collectionsList);
						
						_listView.setAdapter(_collectionsAdapterHelper);
						_listView.setOnItemClickListener(this);
					}
					break;
			}
			
			Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
    public void onItemClick(AdapterView<?> ad, View v, int position, long id) {
        // TODO Auto-generated method stub
		switch(MainActivity.selectedMenuIndx)
		{
			case 1 :
				CustomerProfile _customerProfile = (CustomerProfile)customersList.get(position);
		
				Intent customerViewIntent = new Intent(SearchResultsActivity.this, CustomerFormActivity.class);
				customerViewIntent.putExtra("customer_id", _customerProfile.getCustomerId());
						
				startActivity(customerViewIntent);
				break;
			case 3:
				CustomerCollection _collection = (CustomerCollection)collectionsList.get(position);
				
				Intent collectionViewIntent = new Intent(SearchResultsActivity.this, CollectionFormActivity.class);
				collectionViewIntent.putExtra("collection_no", _collection.getCollectionNo());
						
				startActivity(collectionViewIntent);
				break;
		}
    }
}
