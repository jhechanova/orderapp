package com.example.classobj;

public class MenuDetails {
	int _menu_indx;
	String _menu_label;
	
	public MenuDetails() {}
	
	public MenuDetails(int menu_indx, String menu_label)
	{
		this._menu_indx = menu_indx;
		this._menu_label = menu_label;
	}
	
	public int getMenuIndx()
	{
		return _menu_indx;
	}
	
	public String getMenuLabel()
	{
		return _menu_label;
	}
}
