package com.example.classobj;

public class ItemProfile {
	long _item_id;
	String _sku;
	String _description;
	Double _price;
	Double _qty;
	long _consignment_no;
	int _uom;
	
	public ItemProfile() {}
	
	public ItemProfile(long item_id, String sku, String description, Double price, Double qty, long consignment_no, int uom)
	{
		this._item_id = item_id;
		this._sku = sku;
		this._description = description;
		this._price = price;
		this._qty = qty;
		this._consignment_no = consignment_no;
		this._uom = uom;
	}
	
	public long getItemId()
	{
		return _item_id;
	}
	
	public String getSKU()
	{
		return _sku;
	}
	
	public String getDescription()
	{
		return _description;
	}
	
	public Double getPrice()
	{
		return _price;
	}
	
	public Double getQty()
	{
		return _qty;
	}
	
	public void setQty(double qty)
	{
		this._qty = qty;
	}
	
	public long getConsignmentNo()
	{
		return this._consignment_no;
	}
	
	public int getUOM()
	{
		return this._uom;
	}
}