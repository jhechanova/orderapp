package com.example.classobj;

public class CustomerCollection {
	long _customer_id;
	String _ref_dates;
	String _ref_amounts;
	String _ref_numbers;
	double _total_collection;
	
	long _collection_no;
	String _collection_date;
	String _customer_name;
	
	String _applied_amount;
	String _status; // new, approve
	
	String _payment_method; // added sept. 27, 2014
	double _amount;
	String _check_no;
	String _check_date;
	long _bank_id;
	String _bank_name;
	
	public CustomerCollection() {}
	
	public CustomerCollection(long customer_id, String ref_dates,
			String ref_amounts, String ref_numbers, double total_collection)
	{
		this._customer_id = customer_id;
		this._ref_dates = ref_dates;
		this._ref_amounts = ref_amounts;
		this._ref_numbers = ref_numbers;
		this._total_collection = total_collection;
		
		this._payment_method = "";
		this._amount = 0.0;
		this._check_date = "";
		this._check_no = "";
		this._bank_id = 0;
		this._bank_name = "";
	}
	
	public CustomerCollection(long customer_id, String ref_dates,
			String ref_amounts, String ref_numbers, double total_collection,
			String payment_method, double amount,
			String check_no, String check_date, long bank_id)
	{
		this._customer_id = customer_id;
		this._ref_dates = ref_dates;
		this._ref_amounts = ref_amounts;
		this._ref_numbers = ref_numbers;
		this._total_collection = total_collection;
		
		this._payment_method = payment_method;
		this._amount = amount;
		this._check_date = check_date;
		this._check_no = check_no;
		this._bank_id = bank_id;
		this._bank_name = "";
	}
	
	public void setCustomerName(String customer_name)
	{
		this._customer_name = customer_name;
	}
	
	public void setAppliedAmount(String applied_amount)
	{
		this._applied_amount = applied_amount;
	}
	
	public String getAppliedAmount()
	{
		return this._applied_amount;
	}
	
	public void setStatus(String status)
	{
		this._status = status;
	}
	
	public String getStatus()
	{
		return this._status;
	}
	
	public long getCustomerId()
	{
		return this._customer_id;
	}
	
	public String getRefDates()
	{
		return this._ref_dates;
	}
	
	public String getRefAmounts()
	{
		return this._ref_amounts;
	}
	
	public String getRefNumbers()
	{
		return this._ref_numbers;
	}
	
	public double getTotalCollection()
	{
		return this._total_collection;
	}
	
	public void setCollectionNo(long collection_no)
	{
		this._collection_no = collection_no;
	}
	
	public long getCollectionNo()
	{
		return this._collection_no;
	}
	
	public void setCollectionDate(String collection_date)
	{
		this._collection_date = collection_date;
	}
	
	public String getCollectionDate()
	{
		return this._collection_date;
	}
	
	public String getCustomerName()
	{
		return this._customer_name;
	}
	
	public String getPaymentMethod() // modified sept. 27, 2014
	{
		return _payment_method;
	}
	
	public void setPaymentMethod(String payment_method)
	{
		this._payment_method = payment_method;
	}
	
	public double getAmount()
	{
		return _amount;
	}
	
	public void setAmount(double amount)
	{
		this._amount = amount;
	}
	
	public String getCheckNo()
	{
		return _check_no;
	}
	
	public void setCheckNo(String check_no)
	{
		this._check_no = check_no;
	}
	
	public String getCheckDate()
	{
		return _check_date;
	}
	
	public void setCheckDate(String check_date)
	{
		this._check_date = check_date;
	}
	
	public long getBankId()
	{
		return _bank_id;
	}
	
	public void setBankId(long bank_id)
	{
		this._bank_id = bank_id;
	}
	
	public String getBankName()
	{
		return _bank_name;
	}
	
	public void setBankName(String bank_name)
	{
		this._bank_name = bank_name;
	}
}