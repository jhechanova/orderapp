package com.example.classobj;

public class SalesmanProfile {
	long _salesman_id;
	String _salesman_name;
	boolean _selected;
	
	public SalesmanProfile() {}
	
	public SalesmanProfile(long salesman_id, String salesman_name)
	{
		this._salesman_id = salesman_id;
		this._salesman_name = salesman_name;
		this._selected = false;
	}
	
	public SalesmanProfile(long salesman_id, String salesman_name, boolean selected)
	{
		this._salesman_id = salesman_id;
		this._salesman_name = salesman_name;
		this._selected = selected;
	}
	
	public long getSalesmanId()
	{
		return _salesman_id;
	}
	
	public String getSalesmanName()
	{
		return _salesman_name;
	}
	
	public boolean isSelected()
	{
		return _selected;
	}
	
	public void isSelected(boolean selected)
	{
		this._selected = selected;
	}
}
