package com.example.classobj;

public class CollectionDetails {

	private long dRNumber;
	private String dRDate;
	private double dRAmount;
	private double appliedAmount;
	private double balance;
	
	public CollectionDetails() {}
	
	public void setDRNumber(long _dRNumber)
	{
		this.dRNumber = _dRNumber;
	}
		
	public long getDRNumber()
	{
		return this.dRNumber;
	}
	
	public void setDRDate(String _dRDate)
	{
		this.dRDate = _dRDate;
	}
	
	public String getDRDate()
	{
		return this.dRDate;
	}
	
	public void setDRAmount(double _dRAmount)
	{
		this.dRAmount = _dRAmount;
	}
	
	public double getDRAmount()
	{
		return this.dRAmount;
	}
	
	public void setAppliedAmount(double _appliedAmount)
	{
		this.appliedAmount = _appliedAmount;
	}
	
	public double getAppliedAmount()
	{
		return this.appliedAmount;
	}
	
	public void setBalance(double _balance)
	{
		this.balance = _balance;
	}
	
	public double getBalance()
	{
		return this.balance;
	}
}
