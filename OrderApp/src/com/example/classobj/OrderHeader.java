package com.example.classobj;

public class OrderHeader {
	long _order_no;
	long _customer_id;
	String _order_date;
	/**String _address;
	String _contact_no;
	String _customer_name;**/
	String _order_items;
	String _order_items_qty;
	double _order_total_price;
	String _order_items_discount;
	
	long _payment_terms_id;
	String _payment_terms_name;
	String _status; // new, approve
	
	String _payment_method; // added sept. 27, 2014
	double _amount;
	String _check_no;
	String _check_date;
	long _bank_id;
	String _bank_name;
	
	
	public OrderHeader() {}
	
	public OrderHeader(long order_no, long customer_id, String order_date,
			String order_items, String order_items_qty, double order_total_price, 
			long payment_terms_id, String status, String order_items_discount, String payment_method, double amount,
			String check_no, String check_date, long bank_id) // save
	{
		this._order_no = order_no;
		this._customer_id = customer_id;
		this._order_date = order_date;
		this._order_items = order_items;
		this._order_items_qty = order_items_qty;
		this._order_total_price = order_total_price;
		/**this._address = "";
		this._contact_no = "";
		this._customer_name = "";**/
		this._payment_terms_name = "";
		this._payment_terms_id = payment_terms_id;
		this._status = status;
		this._order_items_discount = order_items_discount;
		
		this._payment_method = payment_method;
		this._amount = amount;
		this._check_date = check_date;
		this._check_no = check_no;
		this._bank_id = bank_id;
		this._bank_name = "";
	}
	
	/**public OrderHeader(long order_no, long customer_id, String customer_name, String order_date,
			String address, String contact_no, String order_items, String order_items_qty, double order_total_price, 
			long payment_terms_id, String payment_terms_name, String status, String order_items_discount)**/
	public OrderHeader(long order_no, long customer_id, String order_date, 
			String order_items, String order_items_qty, double order_total_price, 
			long payment_terms_id, String payment_terms_name, String status, String order_items_discount
			, String payment_method, double amount, String check_no, String check_date, long bank_id, String bank_name) // get
	{
		this._order_no = order_no;
		this._customer_id = customer_id;
		this._order_date = order_date;
		this._order_items = order_items;
		this._order_items_qty = order_items_qty;
		this._order_total_price = order_total_price;
		/**this._address = address;
		this._contact_no = contact_no;
		this._customer_name = customer_name;**/
		this._payment_terms_name = payment_terms_name;
		this._payment_terms_id = payment_terms_id;
		this._status = status;
		this._order_items_discount = order_items_discount;
		
		this._payment_method = payment_method;
		this._amount = amount;
		this._check_date = check_date;
		this._check_no = check_no;
		this._bank_id = bank_id;
		this._bank_name = bank_name;
	}
	
	public long getOrderNo()
	{
		return _order_no;
	}
	
	public long getCustomerId()
	{
		return _customer_id;
	}
	
	public String getOrderDate()
	{
		return _order_date;
	}
	
	public String getOrderItems()
	{
		return _order_items;
	}
	
	public String getOrderItemsQty()
	{
		return _order_items_qty;
	}
	
	public double getOrderTotalPrice()
	{
		return _order_total_price;
	}
	
	/**public String getCustomerName()
	{
		return _customer_name;
	}
	
	public String getContactNo()
	{
		return _contact_no;
	}
	
	public String getAddress()
	{
		return _address;
	}**/
	
	public String getPaymentTermsName()
	{
		return _payment_terms_name;
	}
	
	public long getPaymentTermsId()
	{
		return _payment_terms_id;
	}
	
	public String getStatus()
	{
		return _status;
	}
	
	public String getOrderItemsDesc()
	{
		return _order_items_discount;
	}
	
	public String getPaymentMethod() // modified sept. 27, 2014
	{
		return _payment_method;
	}
	
	public double getAmount()
	{
		return _amount;
	}
	
	public String getCheckNo()
	{
		return _check_no;
	}
	
	public String getCheckDate()
	{
		return _check_date;
	}
	
	public long getBankId()
	{
		return _bank_id;
	}
	
	public String getBankName()
	{
		return _bank_name;
	}
}