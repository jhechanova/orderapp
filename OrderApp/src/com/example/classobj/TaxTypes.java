package com.example.classobj;

public class TaxTypes {
	private String _name;
	private double _rate;
	private long _id;
		
	public TaxTypes() {}
	
	public TaxTypes(String name, double rate, long id)
	{
		this._name = name;
		this._id = id;
		this._rate = rate;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public long getId()
	{
		return _id;
	}
	
	public double getRate()
	{
		return _rate;
	}
}

