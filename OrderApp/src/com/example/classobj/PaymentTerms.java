package com.example.classobj;

public class PaymentTerms {
	private String _name;
	private long _id;
	private int _days;
	private int _days_allowable;
	
	public PaymentTerms() {}
	
	public PaymentTerms(String name, long id, int days, int days_allowable)
	{
		this._name = name;
		this._id = id;
		this._days = days;
		this._days_allowable = days_allowable;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public long getId()
	{
		return _id;
	}
	
	public int getDays()
	{
		return _days;
	}
	
	public int getDaysAllowable()
	{
		return _days_allowable;
	}
}

