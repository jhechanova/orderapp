package com.example.classobj;

public class CustomerProfile {
	long _customer_id;
	String _customer_name;
	String _address;
	String _contact_no;
	// added
	String _prof_image_path;
	String _comp_image_path;
	double _geo_latitude;
	double _geo_longitude;
	int _route_days;
	String _tin;
	int _term;
	int _tax_type;
	// end
	
	//public CustomerProfile() {}
	
	public CustomerProfile(long customer_id, String customer_name, String address, String contact_no
			, String prof_image_path, double geo_latitude, double geo_longitude, int route_days, String _comp_image_path, String tin,
			int term, int tax_type) // modified
	{
		this._customer_id = customer_id;
		this._customer_name = customer_name;
		this._address = address;
		this._contact_no = contact_no;
		// added
		this._prof_image_path = prof_image_path;
		this._comp_image_path = _comp_image_path;
		this._geo_latitude = geo_latitude;
		this._geo_longitude = geo_longitude;
		this._route_days = route_days;
		this._tin = tin;
		this._term = term;
		this._tax_type = tax_type;
		// end
	}
	
	// setters
	
	public void setCustomerId(long customer_id)
	{
		this._customer_id = customer_id;
	}
	
	public void setCustomerName(String customer_name)
	{
		this._customer_name = customer_name;
	}
	
	public void setAddress(String address)
	{
		this._address = address;
	}
	
	public void setContactNo(String contact_no)
	{
		this._contact_no = contact_no;
	}
	
	// added
	public void setProfImagePath(String prof_image_path)
	{
		this._prof_image_path = prof_image_path;
	}
	
	public void setGeoLatitude(double geo_latitude)
	{
		this._geo_latitude = geo_latitude;
	}
	
	public void setGeoLongitude(double geo_longitude)
	{
		this._geo_longitude = geo_longitude;
	}
	
	public void setRouteDays(int routeDays)
	{
		this._route_days = routeDays;
	}
	
	public void setCompImagePath(String comp_image_path)
	{
		this._comp_image_path = comp_image_path;
	}
	
	public void setTin(String tin)
	{
		this._tin = tin;
	}
	
	public void setTerm(int term)
	{
		this._term = term;
	}
	
	public void setTaxType(int tax_type)
	{
		this._tax_type = tax_type;
	}
	
	// getters
	
	public long getCustomerId()
	{
		return _customer_id;
	}
	
	public String getCustomerName()
	{
		return _customer_name;
	}
	
	public String getAddress()
	{
		return _address;
	}
	
	public String getContactNo()
	{
		return _contact_no;
	}
	
	// added
	public String getProfImagePath()
	{
		return _prof_image_path;
	}
	
	public double getGeoLatitude()
	{
		return _geo_latitude;
	}
	
	public double getGeoLongitude()
	{
		return _geo_longitude;
	}
	
	public int getRouteDays()
	{
		return _route_days;
	}
	
	public String getCompImagePath()
	{
		return _comp_image_path;
	}
	
	public String getTin()
	{
		return _tin;
	}
	
	public int getTerm()
	{
		return _term;
	}
	
	public int getTaxType()
	{
		return _tax_type;
	}
	// end
}
