package com.example.classobj;

public class ItemDetails {

	private long itemId;
	private double itemQty;
	private double itemDesc;
	private boolean toBeDeleted;
	private boolean toBeAdded;
	
	public ItemDetails() 
	{
		this.toBeDeleted = false;
		this.toBeAdded = false;
	}
	
	public void setItemId(long _itemId)
	{
		this.itemId = _itemId;
	}
		
	public long getItemId()
	{
		return this.itemId;
	}
	
	public void setItemQty(double _itemQty)
	{
		this.itemQty = _itemQty;
	}
	
	public double getItemQty()
	{
		return this.itemQty;
	}
	
	public void setItemDesc(double _itemDesc)
	{
		this.itemDesc = _itemDesc;
	}
	
	public double getItemDesc()
	{
		return this.itemDesc;
	}
	
	public void setToBeDeleted(boolean _toBeDeleted)
	{
		this.toBeDeleted = _toBeDeleted;
	}
	
	public boolean getToBeDeleted()
	{
		return this.toBeDeleted;
	}
	
	public void setToBeAdded(boolean _toBeAdded)
	{
		this.toBeAdded = _toBeAdded;
	}
	
	public boolean getToBeAdded()
	{
		return this.toBeAdded;
	}
}
