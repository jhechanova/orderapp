package com.example.classobj;

public class Banks {
	private String _name;
	private long _id;
	private String _code;
	
	public Banks() {}
	
	public Banks(String name, long id, String code)
	{
		this._name = name;
		this._id = id;
		this._code = code;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public long getId()
	{
		return _id;
	}
	
	public String getCode()
	{
		return _code;
	}
}

