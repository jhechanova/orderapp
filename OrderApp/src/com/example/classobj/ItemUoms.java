package com.example.classobj;

public class ItemUoms {
	private String _name;
	private String _short;
	private long _id;
		
	public ItemUoms() {}
	
	public ItemUoms(String name, String nshort, long id)
	{
		this._name = name;
		this._id = id;
		this._short = nshort;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public long getId()
	{
		return _id;
	}
	
	public String getShort()
	{
		return _short;
	}
}

